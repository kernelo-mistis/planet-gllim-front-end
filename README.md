> 🔥<span style="color:rgba(101, 73, 195, 0.73)"> NEW : PlanetGLLiM v2 is available </span> 🔥
>
>   This new version may many issues and thus need to be tested. You can use it by following the basic instructions and precising "BRANCH=v2" at *docker compose* commands. 
>   ```
>   $ BRANCH=v2 docker compose pull
>   $ BRANCH=v2 docker compose up
>   


# Documentation
Online documentation is [here](https://kernelo-mistis.gitlabpages.inria.fr/planet-gllim-front-end/).

# Prerequisite
Docker Engine is available on a variety of Linux platforms, macOS and Windows 10. You can find the installation documentation on [Docker's documentation website](https://docs.docker.com/)
## Linux & MacOs X
* [Docker Engine](https://docs.docker.com/engine/) ([Installation instructions](https://docs.docker.com/engine/install/))
* [Docker Compose](https://docs.docker.com/compose/) ([Installation instructions](https://docs.docker.com/compose/install/))

## Windows
* Docker Desktop that contains everything to run a Docker Compose ([Installation instructions](https://docs.docker.com/desktop/windows/install/)). You will be asked to install a linux kernel on the first Docker Desktop run. Choose Ubuntu-20.04. [Instructions to install the linux kernel](https://docs.microsoft.com/en-us/windows/wsl/install)

# How to run PlanetGLLIM

## Login
* In a terminal, run:
```
$ docker login registry.gitlab.inria.fr
```

## Run on Linux (Ubuntu as example)

* clone repository
```
$ git clone https://gitlab.inria.fr/kernelo-mistis/planet-gllim-front-end.git
```

* move to the build dorectory
```
$ cd planet-gllim-front-end/build
```

* pull docker required images
```
$ docker compose pull
```

* run the software
```
$ docker compose up
```

Then wait for this message in the console:
```
web_1     | Starting development server at http://0.0.0.0:8080/
web_1     | Quit the server with CONTROL-C.
```
* Open a web browser and go to http://0.0.0.0:8080/simulation

* To shut down the application, run:
```
$ docker compose down
```

* To shut down the application and remove previously computed data, run:
```
$ docker compose down --volumes
```

## Run on MacOs X

Running instructions are the same as for Linux

## Run on Windows
* Open a command prompt window and go to the desired folder in which you want to clone the repository
* clone repository
```
> git clone https://gitlab.inria.fr/kernelo-mistis/planet-gllim-front-end.git
```
* move to the build dorectory
```
> cd planet-gllim-front-end/build
```
* pull docker required images
```
> docker compose pull
```

* run the software
```
> docker compose up
```
Then wait for this message in the console:

```
web_1     | Starting development server at http://0.0.0.0:8080/
web_1     | Quit the server with CONTROL-C.
```
* Open a web browser and go to http://localhost:8080/simulation

* To shut down the application and remove previously computed data, run:
```
> docker-compose down --volumes
```

# Test environment : How to run PlanetGLLIM on test branch (or any other branch)

You need to specify the specific branch you want to use when calling docker compose. Default value is master.
```
$ BRANCH=test docker compose pull
$ BRANCH=test docker compose up
```

# Troubleshooting

**There is no ouput in the console of the application**

This appears to ahappend sometimes. Just let the simulation run and watch the output in your terminal. One finished, just reload the application page in the browser and rerun the simulation. The saved results will be called.

**My simulation crashes, raising an EOFERROR**

Be sure your docker app has enough memory allocated. Set the amount of RAM available for docker to 8GB at least.

**docker-compose up crashes, raising an error like ERROR: for broker  Cannot start service broker**

You likely have a broker running on your system (like rabbitmq) and the broker port is busy. Be sure the port 5672 is free.

# Licence
This software is licensed under the GNU GPL-compatible [CeCILL licence](LICENCE.txt).
While the software is free, we would appreciate it if you send us an e-mail at ``planet.gliim at inria.fr`` to let us know how you use it.
Also, please contact us if the licence does not meet your needs.
