""" This file define the syntax of the configuration file, using an AST of parsers """

from parsers.parsers import UnionParser, StructParser, EnumParser, StructField
from kernelo_library import kernelo as ker
import os
import numpy as np
import json


# Parsers are defined bottom to top

# ---------------- Physical model ----------------

class FourParamsHapkeAdapter(StructParser):
    fields = [
        StructField("b0", 0., "Fixed value for $B_0$", 3),
        StructField("h", 0.1, "Fixed value for $H$", 3),
    ]

    def parse(self, context):
        return ker.FourParamsHapkeAdapterConfig(
            context.get('b0'),
            context.get('h')
        )


class ThreeParamsHapkeAdapter(StructParser):
    fields = [
        StructField("b0", 0., "Fixed value for $B_0$", 3),
        StructField("h", 0.1, "Fixed value for $H$", 3),
    ]

    def parse(self, context):
        return ker.ThreeParamsHapkeAdapterConfig(
            context.get('b0'),
            context.get('h')
        )


class SixParamsHapkeAdapter:
    def parse(self):
        return ker.SixParamsHapkeAdapterConfig()


class HapkeAdaptaterConfig(UnionParser):
    choices = [
        ("4 parameters model", FourParamsHapkeAdapter()),
        ("3 parameters model", ThreeParamsHapkeAdapter()),
        ("6 parameters model", None),
    ]


class HapkeAdaptater(StructParser):
    fields = [
        StructField("type", EnumParser(["four", "three", "six"]), "Kind of variant.", 3),
        StructField("config", HapkeAdaptaterConfig(), "Options for the variant.", 3)
    ]

    def parse(self, context):
        parsers = {'four': FourParamsHapkeAdapter(),
                   'three': ThreeParamsHapkeAdapter(),
                   'six': SixParamsHapkeAdapter()}
        return parsers[context.get('type')].parse(context.get('config'))


class HapkeModel(StructParser):
    fields = [
        StructField("geometries", "", "Path to geometries file", 1),
        StructField("version", EnumParser(["2002", "1993"]), "Hapke model variant.", 3),
        StructField("theta_bar_scaling", 30., "Maximum value of $\\bar{\\theta}$", 2),
        StructField("adaptater", HapkeAdaptater(), "Variant on model parameters $x$.", 3),
    ]

    def parse(self, context, geometries):
        adapter_config = HapkeAdaptater().parse(context.get('adapter'))
        version = context.get('version')
        theta_bar_scaling = context.get('theta_bar_scaling')
        return ker.HapkeModelConfig(version, adapter_config, geometries, theta_bar_scaling).create()


class ShkuratovModel(StructParser):
    fields = [
        StructField("geometries", "", "Path to geometries file", 1),
        StructField("mins", [0, 0, 0.2, 0, 0], "Minimum values for $x$ parameters", 2),
        StructField("maxs", [1, 1.5, 1, 1.5, 1.5], "Maximum values for $x$ parameters", 2),
    ]

    def parse(self, context, geometries):
        mins = context.get('mins')
        maxs = context.get('maxs')
        return ker.ShkuratovModelConfig(geometries, mins, maxs).create()


class ExternModel(StructParser):
    fields = [
        StructField("file_name", "", "Python file name", 2),
        StructField("file_path", "", "Absolute path of the python file name", 2),
        StructField("class_name", "", "Name of the Python class deriving the base functionnal model", 2),
    ]


class PhysicalModel(StructParser):
    fields = [
        StructField("model", EnumParser(["hapke", "shkuratov", "extern"]), "Functionnal model to invert.", 1),
        StructField("config", UnionParser(
            [
                ("Configuration for Hapke model", HapkeModel()),
                ("Configuration for Shkuratov model", ShkuratovModel()),
                ("Configuration for an external model", ExternModel())
            ]
        ), "Options for the model.", 1)
    ]

    def parse(self, context, geometries):
        parsers = {'hapke': HapkeModel(),
                   'shkuratov': ShkuratovModel(),
                   'extern': ExternModel()}
        return parsers[context.get('model')].parse(context.get('config'), geometries)


# ------------------ Data generation ------------------

_seed_entry = StructField("seed", UnionParser([
    ("Default to current time", None),
    ("Fixed by the user", 1111),
]), "Seed used in random generator.", 3)


class FixedNoiseStatModel(StructParser):
    fields = [
        StructField("generator", EnumParser(["sobol", "latin_cube", "random"]), "Random generator type.", 3),
        StructField("variances", UnionParser([
            ("Isometric covariance", 0.001),
            ("Diagonal covariance", [])
        ]
        ), "Variance of the gaussian noise.", 2),
        _seed_entry,
    ]

    def parse(self, context, physicalModel):
        generator = context.get('generator')
        variances = context.get('variances')
        if not isinstance(variances, list):
            variances = np.ones(physicalModel.get_D_dimension()) * variances
        seed = context.get('seed')
        if seed == '':
            seed = int.from_bytes(os.urandom(4), 'little')
        return ker.GaussianStatModelConfig(generator, physicalModel, variances, seed).create()


class DataNoiseStatModel(StructParser):
    fields = [
        StructField("generator", EnumParser(["sobol", "latin_cube", "random"]), "Random generator type.", 3),
        StructField("noise_effect", 20, "$r$, where std is $\\frac{F(x)}{r}$", 2),
        _seed_entry,
    ]

    def parse(self, context, physicalModel):
        generator = context.get('generator')
        noise_effect = context.get('noise_effect')
        seed = context.get('seed')
        if seed == '':
            seed = int.from_bytes(os.urandom(4), 'little')
        return ker.DependentGaussianStatModelConfig(generator, physicalModel, noise_effect, seed).create()


class DataGeneration(StructParser):
    fields = [
        StructField("model", EnumParser(["basic", "dependent"]), "Satistical model to use.", 2),
        StructField("config", UnionParser([
            ("Configuration for the fixed covariance model", FixedNoiseStatModel()),
            ("Configuration for the data dependant covariance model", DataNoiseStatModel()),
        ]), "Subsequent options.", 2)
    ]

    def parse(self, context, physicalModel):
        parsers = {'basic': FixedNoiseStatModel(),
                   'dependent': DataNoiseStatModel()}
        return parsers[context.get('model')].parse(context.get('config'), physicalModel)


# ------------------------ GLLiM learning ------------------------


class GMMTraining(StructParser):
    fields = [
        StructField('kmeans_iteration', 10, "Number of k-means iteration performed to initialise the clusters", 3),
        StructField('em_iteration', 100, "Number of EM iterations (performed after k-means initialisation)", 3),
        StructField('floor', 1e-8,
                    "Minimal value for covariances (smaller covariances are regularized to avoid numerical issues)", 3),
    ]

    def parse(self, context):
        return ker.GMMLearningConfig(
            context.get('kmeans_iteration'),
            context.get('em_iteration'),
            context.get('floor'))


class EMTraining(StructParser):
    fields = [
        StructField('max_iteration', 100, "Number of EM iterations", 2),
        StructField('ratio_ll', 1e-5,
                    "EM iterations stop when the ratio of the log-likelihood between two iterations is less than this ratio",
                    3),
        StructField('floor', 1e-8,
                    "Minimal value for covariances (smaller covariances are regularized to avoid numerical issues)", 3),
    ]

    def parse(self, context):
        return ker.EMLearningConfig(
            context.get('max_iteration'),
            context.get('ratio_ll'),
            context.get('floor'))


class FixedInit(StructParser):
    fields = [
        StructField("gmm_learning_config", GMMTraining(), "Configuration of the GMM learning", 3),
        _seed_entry
    ]

    def parse(self, context):
        seed = context.get('seed')
        if seed == '':
            seed = int.from_bytes(os.urandom(4), 'little')
        return ker.FixedInitConfig(seed, GMMTraining().parse(context.get('gmm_learning_config')))


class MultiInit(StructParser):
    fields = [
        StructField("nb_exp", 10, "Number of parallel startups", 3),
        StructField("nb_iter_em", 5, "Number of iterations to run for each try", 3),
        StructField("gmm_learning_config", GMMTraining(), "Configuration of the GMM learning", 3),
        _seed_entry
    ]

    def parse(self, context):
        seed = context.get('seed')
        if seed == '':
            seed = int.from_bytes(os.urandom(4), 'little')

        return ker.MultInitConfig(
            seed,
            context.get('nb_iter_em'),
            context.get('nb_exp'),
            GMMTraining().parse(context.get('gmm_learning_config'))
        )


class InitConfig(StructParser):
    fields = [
        StructField("type", EnumParser(["multi", "fixed"]), "Initialisation strategy.", 3),
        StructField("config", UnionParser([
            ("Configuration for multiple initialisation", MultiInit()),
            ("Configuration for single initialisation", FixedInit(),)
        ]), "Subsequent options.", 3),
    ]

    def parse(self, context):
        parsers = {'multi': MultiInit(),
                   'fixed': FixedInit()}

        return parsers[context.get('type')].parse(context.get('config'))


class TrainConfig(StructParser):
    fields = [
        StructField("type", EnumParser(["em", "gmm"]), "Learning implementation (EM vs GMM on joint model).", 3),
        StructField("config", UnionParser([
            ("Configuration for the EM algorithm", EMTraining()),
            ("Configuration for the joint GMM learning", GMMTraining())
        ]), "Corresponding configuration.", 3),
    ]

    def parse(self, context):
        parsers = {'em': EMTraining(),
                   'gmm': GMMTraining()}

        return parsers[context.get('type')].parse(context.get('config'))


_cov_type = EnumParser(["Full", "Diag", "Iso"])


class GLLiMTraining(StructParser):
    fields = [
        StructField("K", 50, "Number of affine components in the approximated model", 2),
        StructField("gamma_type", _cov_type, "Covariance constraint on $\\Gamma$ ($X$ part).", 3),
        StructField("sigma_type", _cov_type, "Covariance constraint on $\\Sigma$ ($Y$ part).", 3),
        StructField("init_config", InitConfig(), "Configuration of the initilisation step", 3),
        StructField("train_config", TrainConfig(), "Configuration of the training step", 3)
    ]

    def parse(self, context, physicalModel):
        return ker.GLLiM(physicalModel.get_D_dimension(),
                         physicalModel.get_L_dimension(),
                         context.get('K'),
                         context.get('gamma_type'),
                         context.get('sigma_type'),
                         InitConfig().parse(context.get('init_config')),
                         TrainConfig().parse(context.get('train_config')))


# ------------------- Prediction -------------------

class Prediction(StructParser):
    fields = [
        StructField("k_merged", 2,
                    "Maximum number of possible multi-solutions; also the number of components the GLLiM posterior is reduced to.",
                    2),
        StructField("k_pred_means", 2,
                    "The number of components to use in the mean prediction; mainly useful to alter the Importance Sampling step.",
                    3),
        StructField("threshold", 1e-10,
                    "Components with weight under this threshold will be merged in one step. Mainly useful to speed up the merging step when $K$ is very large.",
                    3)
    ]

    def parse(self, context, gllim):
        return ker.PredictionConfig(
            context.get('k_merged'),
            context.get('k_pred_means'),
            context.get('threshold'),
            gllim).create()


# ----------------- Importance Sampling -----------------

class ImportanceSampling(StructParser):
    fields = [
        StructField('nb_samples', 20_000, "Number of samples", 2),
        StructField('mean_prop_law', EnumParser(["gaussian"]),
                    "Type of proposition law for the mean. Currently, only gaussian mixture model is implemented.", 3),
        StructField('center_prop_law', EnumParser(["gaussian"]),
                    "Type of proposition law for the centroids. Currently, only gaussian model is implemented.", 3),
    ]


class MainParser(StructParser):
    fields = [
        StructField("physical_model", PhysicalModel(), "Configuration of the functionnal model to invert", 1),
        StructField("data_generation", DataGeneration(), "Configuration of the generation of training data", 2),
        StructField("gllim", GLLiMTraining(), "Configuration of the learning", 2),
        StructField("prediction", Prediction(), "Configuration of the prediction step", 2),
        StructField("importance_sampling", ImportanceSampling(), "Configuration of the Importance Sampling step", 2),
    ]

    def parse(self, context, geometries):
        physicalModel = PhysicalModel().parse(context.get('physical_model'), geometries)
        statModel = DataGeneration().parse(context.get('data_generation'), physicalModel)
        gllimModel = GLLiMTraining().parse(context.get('gllim'), physicalModel)
        predictor = Prediction().parse(context.get('prediction'), gllimModel)
        return physicalModel, statModel, gllimModel, predictor


if __name__ == "__main__":
    a = MainParser()
    context = {
        "physical_model": {
            "model": "hapke",
            "config": {
                "version": "2002",
                "theta_bar_scaling": 30,
                "adapter": {
                    "type": "four",
                    "config": {
                        "b0": 0.5,
                        "h": 0.5
                    }
                }
            }
        },
        "data_generation": {
            "model": "dependent",
            "config": {
                "generator": "sobol",
                "noise_effect": 10000,
                "seed": ""
            }
        },
        "gllim": {
            "K": 10,
            "gamma_type": "Diag",
            "sigma_type": "Diag",
            "init_config": {
                "type": "multi",
                "config": {
                    "seed": "",
                    "nb_iter_em": 5,
                    "nb_exp": 3,
                    "gmm_learning_config": {
                        "kmeans_iteration": 10,
                        "em_iteration": 5,
                        "floor": 1e-8
                    }
                }
            },
            "train_config": {
                "type": "em",
                "config": {
                    "max_iteration": 30,
                    "ratio_ll": 2.0,
                    "floor": 1e-8
                }
            }
        },
        "prediction": {
            "k_merged": 2,
            "k_pred_means": 5,
            "threshold": 0.01
        },
        "importance_sampling": {
            "nb_samples": 20000,
            "mean_prop_law": "gaussian",
            "center_prop_law": "gaussian"
        }
    }
    physicalModel, statModel, gllimModel, predictor = a.parse(context)

    x_gen, y_gen = statModel.gen_data(500)
    y_test = physicalModel.F(x_gen[0, :])
    print(y_test)

    gllimModel.initialize(x_gen, y_gen)
    print("done")
    gllimModel.train(x_gen, y_gen)

    result1 = predictor.predict(y_test, np.zeros(47))
    print(result1.centersPred.means)
    print(result1.meansPred.mean)

