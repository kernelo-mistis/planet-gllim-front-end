from typing import List, Union, Tuple, Optional

# types finaux : leur valeur donne la valeur par défaut
BasicType = Union[None, float, str, int, List]


class AbstractParser:
    """ Interface commune, qui documente la structure attendue d'un paramètre complexe"""
    pass


ValueType = Union[AbstractParser, BasicType]  # types complexes ou simples


class EnumParser(AbstractParser):
    """ Paramètre pouvant prendre un nombre fini de valeur de type 'string'"""

    def __init__(self, choices: List[str]) -> None:
        self.choices = choices


class StructField:
    """ Regroupe le nom d'un champ et son contenu """

    def __init__(self, identifier: str, parser: ValueType, description: str, niveau: int) -> None:
        self.identifier = identifier
        self.parser = parser
        self.description = description
        self.niveau = niveau


class StructParser(AbstractParser):
    """ Ensemble de clés->valeurs """

    fields: List[StructField] = []


class UnionParser(AbstractParser):
    """ Valeur de type variable (parmi un nombre fixé) """

    # [(label, parsing)]
    choices: List[Tuple[str, ValueType]] = []

    def __init__(self, choices: Optional[List[Tuple[str, ValueType]]] = None) -> None:
        super().__init__()
        if choices is not None:
            self.choices = choices
