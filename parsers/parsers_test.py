from parsers import StructField, EnumParser, UnionParser, StructParser

# configuration cible
config = {
    "functionnal": {
        "model": "Hapke",
        "config": {
            "variance": 0.5,
            "geometries": "/sdsdl.sj",
            "adaptater": {
                "type": "6params",
                "config": None
            }
        },
    },
    "data": {
        "model": "var",
        "config": {
            "variances": [4, 5, 6],
            "seed": 4578
        }
    }
}


# parsers nécessaires

class ConfigDataParser(StructParser):
    fields = [
        StructField("variances", [1, 2, 3, 5], "Variance (D)", 2),
        StructField("seed", 4, "seed", 3)
    ]


class DataParser(StructParser):
    fields = [
        StructField("model", EnumParser(
            ["var", "const"]), "Type de modèle", 2),
        StructField("config", ConfigDataParser(), "détails", 3)
    ]


class SkhuratovParser(float):
    # TODO
    pass


class Params4AdaptaterParser(StructParser):
    fields = [
        StructField("B0", 0.1, "Je ne sais plus", 3),
        StructField("H", 0.5, "le nom de ces paramètres", 3)
    ]


class AdaptaterConfig(UnionParser):
    choices = [("Avec 4 paramètres", Params4AdaptaterParser()), ("Avec 6 paramètres", None)]


class AdaptaterParser(StructParser):
    fields = [
        StructField("type", EnumParser(
            ["4params", "6params"]), "Catégorie", 3),
        StructField("config", AdaptaterConfig(), "Détails", 3)
    ]


class HapkeFonctionnalParser(StructParser):
    fields = [
        StructField("variance", 1.4, "Noise on data", 3),
        StructField("geometries", "def.json",
                    "Chemin vers les géométries", 1),
        StructField("adaptater", AdaptaterParser(), "Variantes", 3),
    ]


class FonctionnalConfig(UnionParser):
    choices = [
        ("Modèle de Hapke", HapkeFonctionnalParser()), ("Modèle de Skuratov", SkhuratovParser()),
    ]


class FonctionnalParser(StructParser):
    fields = [
        StructField("model", EnumParser(
            ["Hapke", "Skhu"]), "Type de modèle.", 1),
        StructField("config", FonctionnalConfig(), "Configuration.", 1)
    ]


class MainParser(StructParser):
    fields = [
        StructField("functionnal", FonctionnalParser(),
                    "Configuration du modèle Fonctionnal.", 1),
        StructField("data", DataParser(),
                    "Configuration de la génération.", 2),
    ]


a: StructParser = MainParser()
