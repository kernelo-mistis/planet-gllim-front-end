import random
import numpy as np
try:
    from kernelo_app.kernelo_library import kernelo as ker
except ImportError as e:
    try:
        import kernelo as ker
    except ImportError:
        raise ImportError("Unable to import kernelo")


class StrategyParser:
    def __init__(self, parsing_context, strategies):
        self.__strategies = strategies
        self.execute = self.__get_strategy(parsing_context)

    def get_choices(self):
        return self.__strategies.keys()

    def __get_strategy(self, parsing_context):
        return self.__strategies[parsing_context]


class PhysicalModelParser(StrategyParser):
    def __init__(self, parsing_context):
        super(PhysicalModelParser, self).__init__(parsing_context,
                                                  {'hapke': hapke_model_strategy,
                                                   'shkuratov': shkuratove_model_strategy})


class HapkeAdapterParser(StrategyParser):
    def __init__(self, parsing_context):
        super().__init__(parsing_context, {'four': four_params_adapter_strategy,
                                           'three': three_params_adapter_strategy,
                                           'six': six_params_adapter_strategy})


class DataGenerationParser(StrategyParser):
    def __init__(self, parsing_context):
        super().__init__(parsing_context, {'basic': basic_stat_model_strategy,
                                           'dependent': dependent_stat_model_strategy})


class GllimTrainingParser(StrategyParser):
    def __init__(self, parsing_context):
        super().__init__(parsing_context, {'em': em_training_strategy,
                                           'gmm': gmm_training_strategy})


class GllimInitializationParser(StrategyParser):
    def __init__(self, parsing_context):
        print(parsing_context)
        super().__init__(parsing_context, {'mult': mult_init_strategy,
                                           'fixed': fixed_init_strategy})


class GllimParser:
    def execute(self, context, physicalModel):
        return ker.GLLiM(physicalModel.get_D_dimensioon(),
                         physicalModel.get_L_dimensioon(),
                         context.get('K'),
                         context.get('gamma_type'),
                         context.get('sigma_type'),
                         GllimInitializationParser(
                             context.get('init_config').get('type')).execute(
                             context.get('init_config').get('config')),
                         GllimTrainingParser(
                             context.get('train_config').get('type')).execute(
                             context.get('train_config').get('config')))


class PredictionParser:
    def execute(self, context, gllim):
        return ker.PredictionConfig(
            context.get('k_merged'),
            context.get('k_pred_means'),
            context.get('threshold'),
            gllim).create()


def hapke_model_strategy(context):
    adapter_config = HapkeAdapterParser(context.get('adapter').get('type')) \
        .execute(context.get('adapter').get('config'))
    version = context.get('version')
    theta_bar_scaling = context.get('theta_bar_scaling')
    geometries = ''  # read_geometries(context.get('geometries'))
    return ker.HapkeModelConfig(version, adapter_config, geometries, theta_bar_scaling).create()


def shkuratove_model_strategy(context):
    geometries = ''  # read_geometries(context.get('geometries'))
    mins = context.get('mins')
    maxs = context.get('maxs')
    return ker.ShkuratovModelConfig(geometries, mins, maxs).create()


def three_params_adapter_strategy(context):
    return ker.ThreeParamsHapkeAdapterConfig(
        context.get('b0'),
        context.get('h')
    )


def four_params_adapter_strategy(context):
    return ker.FourParamsHapkeAdapterConfig(
        context.get('b0'),
        context.get('h')
    )


def six_params_adapter_strategy(context):
    return ker.SixParamsHapkeAdapterConfig()


def basic_stat_model_strategy(context, physicalModel):
    generator = context.get('generator')
    variances = context.get('variances')
    if not isinstance(variances, list):
        variances = np.ones(physicalModel.get_D_dimensioon()) * variances
    seed = context.get('seed')
    if seed == '':
        seed = random.seed()
    return ker.GaussianStatModelConfig(generator, physicalModel, variances, seed).create()


def dependent_stat_model_strategy(context, physicalModel):
    generator = context.get('generator')
    noise_effect = context.get('noise_effect')
    seed = context.get('seed')
    if seed == '':
        seed = random.seed()
    return ker.DependentGaussianStatModelConfig(generator, physicalModel, noise_effect, seed).create()


def em_training_strategy(context):
    return ker.EMLearningConfig(
        context.get('max_iteration'),
        context.get('ratio_ll'),
        context.get('floor'))


def gmm_training_strategy(context):
    return ker.GMMLearningConfig(
        context.get('kmeans_iteration'),
        context.get('em_iteration'),
        context.get('floor'))


def mult_init_strategy(context):
    seed = context.get('seed')
    if seed == '':
        seed = random.seed()

    return ker.MultInitConfig(
        seed,
        context.get('nb_iter_em'),
        context.get('nb_exp'),
        GllimTrainingParser('gmm').execute(context.get('gmm_learning_config'))
    )


def fixed_init_strategy(context):
    seed = context.get('seed')
    if seed == '':
        seed = random.seed()
    return ker.FixedInitConfig(seed, GllimTrainingParser('gmm').execute(context.get('gmm_learning_config')))


if __name__ == '__main__':
    context = {
        "physical_model": {
            "model": "hapke",
            "config": {
                "version": "2002",
                "theta_bar_scaling": 30,
                "adapter": {
                    "type": "four",
                    "config": {
                        "b0": "1",
                        "h": 0.1
                    }
                }
            }
        },
        "data_generation": {
            "model": "basic",
            "config": {
                "generator": "sobol",
                "variances": 0.001,
                "seed": ""
            }
        },
        "gllim": {
            "K": 50,
            "gamma_type": "Full",
            "sigma_type": "Diag",
            "init_config": {
                "type": "multi",
                "config": {
                    "seed": "",
                    "nb_iter_em": 5,
                    "nb_exp": 10,
                    "gmm_learning_config": {
                        "kmeans_iteration": 5,
                        "em_iteration": 10,
                        "floor": 1e-8
                    }
                }
            },
            "train_config": {
                "type": "em",
                "config": {
                    "max_iteration": 100,
                    "floor": 1e-8
                }
            }
        },
        "prediction": {
            "k_merged": 2,
            "k_pred_means": 2,
            "threshold": 1e-10
        },
        "importance_sampling": {
            "nb_samples": 20000,
            "mean_prop_law": "gaussian",
            "center_prop_law": "gaussian"
        }
    }

    physicalModel = PhysicalModelParser(context.get('physical_model').get('model')) \
        .execute(context.get('physical_model').get('config'))
    statModel = DataGenerationParser(context.get('data_generation').get('model')) \
        .execute(context.get('data_generation').get('config'), physicalModel)
    gllimModel = GllimParser().execute(context.get('gllim'), physicalModel)
    predictor = PredictionParser().execute(context.get('prediction'), gllimModel)