from parsers import StructParser, EnumParser, UnionParser, ValueType, BasicType, AbstractParser
from typing import List, Tuple, Dict
import json

def _latex_escape(s:str) -> str:
    return s.replace("_", "\\textunderscore ")

def _render_list(items: List[Tuple[str, str]]) -> str:
    if len(items) == 0:
        return ""
    lines = "\n".join(f"\\item[{_latex_escape(item[0])}]: {item[1]}" for item in items)
    return "\\begin{description}\n" + lines + "\n\\end{description}"


def _render_enum(choices: List[str]) -> Tuple[str, str]:
    return "One of \\lstinline[language=jsoninline]!" + json.dumps(choices)[1:-1] + "!.", choices[0]


def _render_union(parser: UnionParser, niveau: int) -> Tuple[str, object]:
    contenu = "One of the following :"
    exemples = []
    for (label, choice) in parser.choices:
        doc, exemple = _render_parser(choice, niveau)
        exemples.append(exemple)

        item = f"{{\\color{{unionColor}} {label}}} " + doc
        contenu += "\n" + "\\begin{mdframed}\n" + item + "\\end{mdframed}\n"
    return contenu, exemples[0]


def _render_struct(parser: StructParser, niveau: int) -> Tuple[str, Dict[str, object]]:
    items: List[Tuple[str, str]] = []
    exemples = {}
    for field in parser.fields:
        # on ignore les champs trop "évolués"
        if field.niveau > niveau:
            continue

        doc, exemple = _render_parser(field.parser, niveau)
        contenu = field.description + "\n" + doc
        items.append(
            ("{\\color{identifierColor} " + field.identifier + "}", contenu))

        exemples[field.identifier] = exemple
    return _render_list(items), exemples


def _render_basic(parser: BasicType) -> Tuple[str, object]:
    return f"(type : \\lstinline[language=Python]!{type(parser).__name__}! - default : \\lstinline[language=jsoninline]!{json.dumps(parser)}!)", parser


def _render_parser(parser: ValueType, niveau: int) -> Tuple[str, object]:
    """ Génère un bloc """
    if isinstance(parser, EnumParser):
        return _render_enum(parser.choices)
    elif isinstance(parser, UnionParser):
        return _render_union(parser, niveau)
    elif isinstance(parser, StructParser):
        return _render_struct(parser, niveau)
    elif parser is None:
        return " \\textit{empty}", None
    elif isinstance(parser, AbstractParser):
        raise TypeError(f"unexpected parsing : {parser}")
    else:  # basic value
        return _render_basic(parser)


def render_top_level(parser: StructParser, niveau: int) -> str:
    """ Génère le code latex correspondant à la structure définie par `parsing`.
    Seuls les champs avec un niveau inférieur ou égal à `niveau` sont gardés."""
    contenu = ""
    for field in parser.fields:
        # on ignore les champs trop "évolués"
        if field.niveau > niveau:
            continue

        doc, exemple = _render_parser(field.parser, niveau)
        contenu += "\\subsection{" + field.description + "}\n"
        contenu += doc

        exempleStr = json.dumps(
            dict([(field.identifier, exemple)]), indent=2)
        contenu += ("\\begin{lstlisting}[language=json, title=Exemple]\n" +
                    exempleStr +
                    "\\end{lstlisting}\n")

    return contenu
