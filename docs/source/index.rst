.. PlanetGLLiM documentation master file, created by
   sphinx-quickstart on Tue Jun 27 14:48:46 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. title:: PlanetGLLiM documentation

.. include:: rst/home.rst

.. toctree::
   :maxdepth: 3
   :hidden:
   :titlesonly:
   :caption: User guide

   rst/user_guide/installation
   rst/user_guide/getting_started/index

.. toctree::
   :maxdepth: 3
   :hidden:
   :titlesonly:
   :caption: Scientific documentation

   rst/scientific_doc/fundamentals/index
   rst/scientific_doc/photometric_models/index
   rst/scientific_doc/inputs_description/index
   rst/scientific_doc/outputs_description/index
   rst/scientific_doc/inversion_software/index

.. toctree::
   :maxdepth: 3
   :hidden:
   :titlesonly:
   :caption: Developers

   rst/developers/how_it_works
   rst/developers/developers_installation


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
