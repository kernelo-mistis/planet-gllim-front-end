1. use the provided [presentation template](template_presentation_markdown.md) in markdown
2. modify and enrich the template as needed
3. compile the source with pandoc (see below) to generate a latex file
4. modify the latex file to fine tune the presentation (inclusion of videos, animation, etc...)
5. compile the latex source with pdflatex.

Command to compile the markdown source :

```shell
/absolute_path_to_pandoc/pandoc --citeproc -o ${outputPath} -t tex --to=${class} --include-in-header=/absolute_path_to_latex_style_file/mystyle_beamer.tex --standalone ${currentFileFullName}
```

This is a copy of the mystyle_beamer.tex

```latex
% package for symbols
\usepackage{gensymb}
% allow the inclusion of svg graphics
\usepackage{svg}
\setsvg{inkscape
="/Applications/Inkscape.app/Contents/MacOS/inkscape"-z -D}
% appendix
\usepackage{appendixnumberbeamer}
% allow links to video
\usepackage{multimedia}
\usepackage{hyperref}
% add page number in the footnote
\setbeamertemplate{page in head/foot}[framenumber]
\setbeamertemplate{page number in head/foot}[framenumber]
% add a logo on every frames
\logo{\includegraphics[height=1.0cm]{/absolute_path_to_logos/logo_IPAG.png}\vspace{220pt}}
% allowd automatic frame break if content is too long
% \let\oldframe\frame
% \renewcommand\frame[1][allowframebreaks]{\oldframe[#1]}
% set style of blocks
\setbeamertemplate{blocks}[rounded][shadow=true]
%\definecolor{capri}{rgb}{0.1, 0.7, 1.0}
%\usecolortheme[named=capri]{structure}
```