---
class: beamer

author: Sylvain Douté, senior planetary physicist.

title: My title.

subtitle: My subtitle

institute: Institut de Planétologie et d’Astrophysique de Grenoble (IPAG)

theme: Metropolis
fontsize: 10pt
Aspectratio: 169
linkcolor: blue

date: My date

bibliography: xxxx_biblio.bib
csl: /Users/doutes/Communication/Bdoc_publi/Biblio_style/icarus.csl
link-citations: true
---

## Outline

[TOC]



# Introduction

## two-column slide with an image

:::::::::::::: {.columns}
::: {.column width="50%"}

**this is a two-column slide with an image**

![legend of the image](path_to_file){width=100%}

*text in slant face*

:::

::: {.column width="50%"}

**some texte**

:::

::::::::::::::

## Slide 2 with a list

This is a one column slide with a list:

* Item1,
* Item2, 
* Item3,
* Item4.

## Slide 3 with a video

This is a slide with a video. 

Note that the following latex code block will be only interpreted in the generated latex file if the user extract it from its current formatting.

```latex
<!---{\centering
\movie[externalviewer]{\includegraphics[width=0.6\textheight,keepaspectratio]{descente_Eagle_LM_Apollo11_image.png}}{descente_Eagle_LM_Apollo11.mp4}
}-->
```

The following html command will actually insert the video in the source markdown file but will not be transferred to the latex file.

<video src="descente_Eagle_LM_Apollo11.mp4" />

This is a sentence  with a reference to a note [^note1] ... 

[^note1]: some precision

# Methods

## Slide 1 of the second section

:::::::::::::: {.columns}
::: {.column width="50%"}

Put here the content of your first column.

:::

::: {.column width="50%"}

Put here the content of your second column

:::

::::::::::::::

## Slide 2 with some maths

It entails the following minimisation :
$$
\begin{array}{l}
{\bf{z}} = \mathop {\arg \min }\limits_{{\bf{p}},{\bf{q}}} {\bf{E_1}}\\
{\rm{with}}\ {\bf{E_1}} = \frac{1}{2}{\left( {\beta  \cdot {\bf{R}}\left( {{\bf{p}},{\bf{q}}} \right) - {\bf{I}_{corr}}} \right)^2} \\
{\rm{with}}\ {\bf{I}_{corr}=\frac{\bf{I}-\bf{L}_{atm}}{\bf{T}_{atm}}}
 \end{array}
$$

* ${\bf{R}}\left( {{\bf{p}},{\bf{q}}}\right)$ models the reflectance factor at the bottom of the atmosphere ${\bf{R}}=\pi L_{BOA}/F$,
* $\beta$ is a scale calibration factor,
*  $\bf{I}_{corr}$ is the atmospherically corrected image,
* $\bf{p}$ and $\bf{q}$ denote the surface orientation, respectively the northward and eastward slope magnitudes.

###

> Note: here in the final presentation, the slide will be animated with the first part appearing first (above the ###) and the second part appearing at the click. 
>

## Slide 3 with a link to another section and with references

Please put your content here :

###

Here with provide an active link to the  [(Supplementary materials)](#Supplementary materials) .

Here we provide 2 bibliographic references that should be in the bibtex file associated to the presentation (see yaml header)

[@Doute2020; @Jiang2017]

## Slide 4 with a table

| Title column 1 | Title column 2 | Title column 3 |
| -------------- | -------------- | :------------- |
| Content        |                |                |
|                | Content        |                |
|                |                | Content        |

# Results

## Slide 1 of the third section

:::::::::::::: {.columns}
::: {.column width="50%"}

Put here the content of your first column.

:::

::: {.column width="50%"}

Put here the content of your second column

:::

::::::::::::::

# Conclusion

## Conclusion

Markdown editing and pandoc processing is a great way to make presentation

* focus on the content,
* Preview of images, videos, latex equation and more,
* Possibility to have different output format (pdflatex, powerpoint etc.),
* The result is neat.

disadvantages:

* A little bit of latex editing may be necessary (videos, long slides, etc.).

# References

## References

::: {#refs}

:::

# Thank you

# Supplementary materials

## A glimpse on numerical methods

We end up with the following model:
$$
\begin{array}{l}
{\bf{z}} = \mathop {\arg \min }\limits_{{\bf{p}},{\bf{q}},{\bf{z}},\beta } {\bf{E}}\\
{\rm{with}}\ {\bf{E}} = \frac{1}{2}{\left( {\beta  \cdot {\bf{R}}\left( {{\bf{p}},{\bf{q}}} \right) - {\bf{I}_{corr}}} \right)^2} + \frac{{{\lambda _1}}}{2}\left[ {{{\left( {{\bf{p}} - {{\bf{z}}_x}} \right)}^2} + {{\left( {{\bf{q}} - {{\bf{z}}_y}} \right)}^2}} \right] \\ + \frac{{{\lambda _2}}}{2}{\left( {{\bf{W}} \cdot {\bf{z}} - {\bf{W}} \cdot {{\bf{z}}_0}} \right)^2}
\end{array}
$$
and 4 independent variables : $\left( {{\bf{p}},{\bf{q}}}\right)$,  $\bf{z}$, and $\beta$ . $\lambda_1$ and $\lambda_2$ are meta-parameters to tune using accurate control points. 

Updating $\bf{p}$ and $\bf{q}$ using classic gradient descent:
$$
\begin{array}{l}{{\bf{p}}^{n + 1}} = {{\bf{p}}^n} - \alpha _1^n\frac{{\partial E}}{{\partial {\bf{p}}}} \\ = {{\bf{p}}^n} - \alpha _1^n\left[ {\left( {{\beta ^n}{\bf{R}}\left( {{{\bf{p}}^n},{{\bf{q}}^n}} \right) - {\bf{I}}} \right){\beta ^n}\frac{{\partial {\bf{R}}\left( {{{\bf{p}}^n},{{\bf{q}}^n}} \right)}}{{\partial {\bf{p}}}} + {\lambda _1}\left( {{{\bf{p}}^n} - {\bf{z}}_x^n} \right)} \right] \\
{{\bf{q}}^{n + 1}} = {{\bf{q}}^n} - \alpha _2^n\frac{{\partial E}}{{\partial {\bf{q}}}} \\= {{\bf{q}}^n} - \alpha _2^n\left[ {\left( {{\beta ^n}{\bf{R}}\left( {{{\bf{p}}^{n + 1}},{{\bf{q}}^n}} \right) - {\bf{I}}} \right){\beta ^n}\frac{{\partial {\bf{R}}\left( {{{\bf{p}}^{n + 1}},{{\bf{q}}^n}} \right)}}{{\partial {\bf{q}}}} + {\lambda _1}\left( {{{\bf{q}}^n} - {\bf{z}}_y^n} \right)} \right]
\end{array}
$$


## Following

Updating $\bf{z}$ by solving the classical Euler-Lagrange equation (Gauss-Seidel method) :
$$
{\lambda _1}\left( {\Delta {\bf{z}} - \frac{{\partial {\bf{p}}}}{{\partial x}} - \frac{{\partial {\bf{q}}}}{{\partial y}}} \right) = {\lambda _2}{\bf{W}}\left( {{\bf{z}} - {{\bf{z}}_0}} \right)
$$
Updating β by setting the derivative of E with respect to β to 0:
$$
{\beta ^{n + 1}} = \frac{{{{\left( {{\bf{R}}\left( {{{\bf{p}}^{n + 1}},{{\bf{q}}^{n + 1}}} \right)} \right)}^T}{\bf{I}}}}{{{{\left( {{\bf{R}}\left( {{{\bf{p}}^{n + 1}},{{\bf{q}}^{n + 1}}} \right)} \right)}^T}\left( {{\bf{R}}\left( {{{\bf{p}}^{n + 1}},{{\bf{q}}^{n + 1}}} \right)} \right)}}
$$





