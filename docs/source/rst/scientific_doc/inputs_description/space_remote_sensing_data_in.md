Space remote sensing data
=========================

They are provided in the form of two cubes in ENVI® format. {ENVI}[https://www.nv5geospatialsoftware.com/Products/ENVI] is the industry standard image processing and analysis software. Each cube consists of two separate files:

1. a cube of reflectance values *prefix_rho_mod* and associated header *prefix_rho_mod.hdr* 
   The header *prefix_rho_mod.hdr* file of ascii type contains information on the structure of the cube and keywords=value items. We will thus find the keyword wavelength associated with the sequence of wavelengths.
   The *prefix_rho_mod* binary raster file contains a 3 dimensional object `[ nb_pts, D, nb_wave]` with ` nb_pts` the number of points in the scene, `D` the number of geometric configurations and `nb_wave` the number of wavelengths. Each line of the cube corresponds to a point in the scene. The suffix "_mod" emphases the fact that each column of the cube is expressed at the same geometry or, equivalently, that each line of the cube corresponding to a given point is seen according to the same geometrical sequence.  
2. a cube of uncertainty values *prefix_drho* and associated header *prefix_drho.hdr*
   It is the homologous cube of *prefix_rho* with the same dimensions `[ nb_pts, D, nb_wave]` that gives the uncertainty value on the reflectance measurement for a given point of the scene, at a given geometry and wavelength. 

In addition the geometries should also provided in the form of a separated json file, the root of which containing 3 arrays of floating reals of dimension `[D]`: `sza` (incidence angle) , `vza` (emergence angle), and `phi` (azimuth angle). 