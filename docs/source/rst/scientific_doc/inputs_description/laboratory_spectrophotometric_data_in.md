Laboratory spectrophotometric data
==================================

They are provided as a structured and hierarchical json file. The file includes a root level with the name of the experiment *name_experiment*. The root level includes:

1. an object *geometries*, array of floating reals of dimension `[D, 3]` with `D` the number of geometric configurations. The 1st column of the table includes the values of the angle of incidence, the 2nd the angle of emergence and the 3rd the angle of azimuth (see refxxx)
2. a *wavelengths* object, vector of floating reals of dimension `[nb_wave, 1]` with `nb_wave` the number of wavelengths.
3. one or more *reflectance_xxx objects*, array of floating reals of dimension `[nb_wave, 2, D]` in the case where an uncertainty value (index 2 of the 2nd dimension) exists for each measure of reflectance (index 1 of the 2nd dimension) or `[nb_wave, D]` if only the reflectance value is <!--to be verified-->. Each *reflectance_xxx* object corresponds to the measurement of a xxx sample of the experiment with geometric configurations and wavelengths given by the *geometries* and *wavelengths* object respectively. 

> Note that we use nested array structures to store N-dimensional arrays.
>
> Note that the geometries should also provided in the form of a separated json file the root of which containing 3 vectors of floating reals of dimension `[D]`: `sza` (incidence angle) , `vza` (emergence angle), and `phi` (azimuth angle). 
>
> See the example provided for the JSC1 experiment containing the measurements of 1 samples: reflectance_JSC1 in addition to geometries and wavelengths.

