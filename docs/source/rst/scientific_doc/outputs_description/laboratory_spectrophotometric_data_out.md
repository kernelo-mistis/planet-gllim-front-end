Laboratory spectrophotometric data
==================================

The results are provided as structured and hierarchical json files. There is one file for each type of estimation:

- *name_experiment_is_center_1*: importance sampling of the posterior probability distribution function (pdf) around the first centroid. 
- *name_experiment_is_center_2*: importance sampling of the posterior probability distribution function (pdf) around the second centroid. 
- *name_experiment_is_mean*: importance sampling of the mean of the posterior probability distribution function (pdf). 
- *name_experiment_pred_center_1*: prediction of the first centroid according to the Gllim model.
- *name_experiment_pred_center_2*: prediction of the second centroid according to the Gllim model.
- *name_experiment_pred_mean*: prediction of the mean of the Gllim Gaussian mixture model.

The latter stands in the case the user chooses a final mixture of 2-components for the Gllim model. If the mixture contains more than 2 components the number of files will change accordingly (two files *name_experiment_is_center_i* and *name_experiment_pred_center_i* for each center i). <!--To be verified-->. Each file includes a root level with the name of the experiment *name_experiment*. The root level includes:

1. a *wavelengths* object, vector of floating reals of dimension `[nb_wave, 1]` with `nb_wave` the number of wavelengths.

2. a series of sub-levels *reflectance_xxx* corresponding to the various xxx samples of the experiment. Each sub-level xxx contains:
   1. ESTIM_X object, array of floating reals of dimension `[nb_wave, L]` that contains the estimation of the L physical parameters normalised between [0,1] for each wavelength.
   2. ESTIM_X_PHYSICAL_UNITS object, array of floating reals of dimension `[nb_wave, L]` that contains the estimation of the L physical parameters for each wavelength.
   3. STD_X object, array of floating reals of dimension `[nb_wave, L]` that contains the uncertainty on the L physical parameters normalised between [0,1] for each wavelength.
   4. STD_X_PHYSICAL_UNITS object, array of floating reals of dimension `[nb_wave, L]` that contains the uncertainty on the L physical parameters for each wavelength.
   5. ERR_Y object, vector of floating reals of dimension `[nb_wave]` that contains the root mean square error between Y~obs~ and F(X) over the D geometrical configurations.


There are 3 additional files:

- name_experiment_MULT,
- name_experiment_REGU_CENTERS_IS,
- name_experiment_REGU_CENTERS.

<!--Find the description-->