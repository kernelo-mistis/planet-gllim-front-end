Geometries definitions
======================

There are different sets of angles that characterize the geometry of illumination and observation of a planetary surface or of a sample in the laboratory.  One of the sets includes the incident and emerging angles $i$ and $e$, and azimuth $\phi$ that is the angle between the planes of scattering and incidence.

For many applications another set can be used. It includes the so-called photometric (luminance) coordinates $\alpha$, $\beta$, and $\gamma$ that are the phase angle, and photometric latitude and longitude, respectively. 

<img src="../../../_static/angles.png" alt="Scheme describing the geometry of illumination and observation" width="50%" class="align-center">

These sets depend on each other as follows :

$$
\cos i=\cos \beta  \cos (\alpha-\gamma) \\ 
$$
$$
\cos e=\cos \beta \cos \gamma,\\ 
$$
$$
\cos \varphi=\frac{\cos \alpha-\cos i \cos e}{\sin i \sin e} \\ \\
$$

and

$$
\cos \alpha=\cos i \cos e+\sin i \sin e \cos \varphi \\
$$
$$
\cos \beta=\sqrt{ \frac{(\sin (i+e))^2-(\cos (\varphi / 2))^2 \sin 2 e \sin 2 i}{(\sin (i+e))^2-(\operatorname{cog}(\varphi / 2))^2 \sin 2 e \sin 2 i+(\sin e)^2(\sin i)^2(\sin \varphi)^2} } \\ \\
$$
$$
\cos \gamma=\frac{\cos e}{\cos \beta}
$$

The radiance factor also known as “I/F”, is $r_F=\pi r$

A photometric model for the surface provides an analytical expression for $r_F$.

The observations that are processed by planetgllim are multi-angular: for the same laboratory sample (or the same spatial point in a planetary scene), we have a reflectance measurement for each geometric configuration $G_d$ of a series of D geometries (d=1,...,D) where $G = (i,e,φ)$. We consider that G is known precisely, fixed by the measurement context. Thus, when performing the inversion, the functional to be considered is evaluated as a vector and is defined by a D-tuple of geometric configurations. In the context of real data, these configurations are controlled by the experiment: a satellite, subject to operational constraints, generally covers about ten configurations; in the laboratory, we can obtain on the order of tens of measurements.