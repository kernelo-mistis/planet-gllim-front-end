Hapke's model
=============


The Hapke model is a radiative transfer model introduced by B. Hapke in [Hapke, 1981, Hapke, 1984, Hapke, 1986] which models the interaction of light in a particulate medium and provides an analytical expression for reflectance. This model makes it possible to explain a photometric measurement with relatively few parameters like the absorptivity of the particles, their scattering cross sections, their phase function and the macroscopic roughness of the material. Several versions and refinements of the model have been proposed. The version proposed in [Hapke, 2002] links the physical $(w, b, c, \theta_H, B_0, h)$ to the bidirectional reflectance by the following formula:

$$
R(i,e,G)=\frac{w}{4π} \frac{μ_{0eG} (θ_H )}{μ_{0eG} (θ_H )+μ_{eG} (θ_H )} [P_G(\alpha,b,c)(1+ B_G(B_0, h)+M(μ_{0eG}(θ_H ),μ_{eG}(θ_H ) )]× S_G(i,e,\alpha,θ_H)
$$

G encodes the geometric measurement configuration as described in the previous section. The physical parameters and the various expressions that depends on $G$, i.e. $μ_{0eG}, μ_{eG}, S_G, B_G, P_G$ and $M$ are described below.

| **Symbol** | **Parameter**                                     | **Physical meaning**                                    | **Interval of variation** |
| ---------- | ------------------------------------------------- | ------------------------------------------------------- | ------------------------- |
| w          | Single scattering albedo                          | Scattering at the grain level                           | [0  , 1]                  |
| b          | Anisotropy parameter                              | b=0   isotropic scattering  b=1  directional scattering | [0  , 1]                  |
| c          | Backward or forward scattering coefficient        | c  > 0.5  backscattering   c  < 0.5  forward scattering | [0 , 1]                   |
| $θ_H$      | Photometric roughness                             | Mean slope angle averaged on all scales                 | [0°  , 90°]               |
| $B_0$      | Amplitude  of the shadow hiding opposition effect | Opacity of the grains                                   | [0  , 1]                  |
| h          | Width of the shadow hiding opposition effect      | Porosity of the granular medium                         | [0  , 1]                  |

> Single scattering albedo ω and multiple scattering function H

The albedo w describes the brightness of a particle and represents the proportion of the radiation scattered by a single particle compared to the incident radiation. It intervenes in the multiple scattering function M, which is defined (in the version of [Hapke, 2002]) by:

$$
M(μ_{0eG}(θ_H ),μ_{eG}(θ_H ) )=H(w,μ_{0eG}(θ_H ))H(w,μ_{eG}(θ_H ))-1 \\
$$
$$
H(w,x)=\frac{1+2x}{1+2\sqrt{(1-w)}}
$$

where $x$ is $μ_{0eG}$ or $μ_{eG}$. 

> Parameters b and c of the $P_G$ phase function 

The $P_G$ phase function characterizes the angular distribution of the scattered energy by a particle. It depends on the parameter b which is related to the anisotropy of the diffusing lobe and on the parameter c which defines the main direction of diffusion. It is defined by:

$$
P_G(b,c)=(1-c)\frac{1-b^2}{(1+2b\cos(\alpha)+b^2)^{3/2}}+c \frac{1-b^2}{(1-2b\cos(\alpha)+b^2)^{3/2}}
$$

Note that we choose here [0, 1] as the domain of variation of c, which corresponds to the convention of [Schmidt and Fernando, 2015].[Hapke, 2002], meanwhile, uses [−1, 1].

> Macroscopic roughness $\theta_H$

The macroscopic roughness factor $S_G$ is constructed from a description of the surface topography as a set of facets where the tangent of the slopes follows a Gaussian distribution, with mean slope angle $θ_H$. The macroscopic roughness parameter  $θ_H$ is also involved in the expression of $μ_{0eG}$ and $μ_{eG}$ which correspond to the cosine of the equivalent angle of incidence and emergence of a rough surface. We refer to [Hapke, 1993](https://www.sciencedirect.com/science/article/pii/S0032063311001954#bib76) for the exact expression of $S_G$, $μ_{0eG}$ and $μ_{eG}$.

> Parameters $B_0$ and $h$ of the opposition effect

The opposition effect corresponds to the increase in the light intensity diffused by a granular medium, at small phase angles ([Hapke, 1986, Hapke, 2002, Hapke, 2021] ) due to shadow hiding. In the model of [Hapke, 2002], it is parameterized by the amplitude $B_0$ and the width of the opposition peak h, linked by the equation:

$$
B_G(h,B_0)=\frac{B_0}{1+1/h\tan(\alpha)}
$$

This physical process is maximum for phase angles close to zero, but becomes negligible past 20◦. As it is difficult to obtain measurements for small phase angles, it is common not to try to constrain $B_0$ and $h$ and to set them to a default value. We then obtain a Hapke model with 4 parameters $(ω,θ_H,b,c)$.