Custom model
============

The PlanetGLLiM software allows you to edit your own physical model. To do so you must provide a Python file (.py) describing your physical model as a Python class.

#### How to add my own physical model ?

Go to the configuration of physical model section and select "Custom model" in the drop-down menu. There you can upload a Python file describing your physical model. The structure of the Python script must follows the Template. You can download the Template file and edit it. An example of the implementation of the Shkuratov photometric model is also available.

In order for the simulation to work, the implementation of the Python file must respects a few rules, described below.
The physical model must be declared as a Python class.
```python 
class PhysicalModel(object):
```
Since the mathematical functioning of the inversion relies on X components normalized between 0 and 1, the user must provide rules for variable change.

The Python class must be composed of 5 mandatory functions:

- `F`: the functional model F describing the physical model. F takes photometries as arguments and return reflectances,
- `get_D_dimension`: returns the dimension of Y (reflectances),
- `get_L_dimension`: return de dimension of X (photometries),
- `to_physic`: converts the X data from mathematical framework $(0<X<1)$ to physical framework,
- `from_physic`: converts the X data from physical framework to mathematical framework $(0<X<1)$.

If your physical model requires geometries as with Hapke and Shkuratov, the structure and the values of the geometries must be declared within this Python file.
```python 
geometries =    [   
                        [inc0, inc1, inc2, inc3, inc4, inc5, inc6, ...],
                        [eme0, eme1, eme2, eme3, eme4, eme5, eme6, ...],
                        [phi0, phi1, phi2, phi3, phi4, phi5, phi6, ...] 
                    ]
```

Note that some class constants, other functions and class constructors can also be declared.
