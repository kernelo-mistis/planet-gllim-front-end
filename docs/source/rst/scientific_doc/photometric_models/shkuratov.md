Shkuratov's model
=================

Here we summarise the theory presented in the seminal paper of ([Shkuratov et al., 2011](https://www.sciencedirect.com/science/article/pii/S0032063311001954)).

In this section we consider photometric models that can be separated in a phase function and a disk function : $r_F=A_{eq}(\alpha)D(\mu_0,\mu,\alpha)$ 

The equigonal albedo, or phase function, describes the phase dependence of the brightness: $A_{eq}=A_Nf(\alpha)$ 

where $A_N$ is the normal albedo, and $f(α)$ is the phase function normalized to unity at $α=0°$. The latter depends on the choice of disk function *D*, which describes how the reflectance varies over the planetary disk at constant phase angle.


**Disk functions D**

We consider two well-known disk functions, each normalized at $i=e=\alpha=0$ 

1. The parameter-free version of the Akimov function which was derived theoretically for an extremely rough surface that is slightly randomly undulated :

$$
D(\alpha,\beta,\gamma) = \cos\frac{\alpha}{2}\cos\left( {\frac{\pi}{\pi - \alpha}\left( {\gamma - \frac{\alpha}{2}} \right)} \right)\frac{{(\cos\beta)}^{\alpha(\pi - \alpha)}}{\cos\gamma}
$$

Analysis of light scattering by different complex structures leads to the notion about equifinality of a scattering law ([Shoshany, 1991](https://www.sciencedirect.com/science/article/pii/S0032063311001954#bib270)), when a significant number of different complex structures exhibit the same scattering law. For example, the Akimov function can be rigorously derived not only for the case of utterly rough surfaces ([Akimov, 1976](https://www.sciencedirect.com/science/article/pii/S0032063311001954#bib5), [Akimov, 1988a](https://www.sciencedirect.com/science/article/pii/S0032063311001954#bib8), [Shkuratov et al., 1994a](https://www.sciencedirect.com/science/article/pii/S0032063311001954#bib249)), but also for fractal-like surfaces with the same Gaussian statistics of local slopes at each hierarchical level of roughness ([Shkuratov et al., 2003a](https://www.sciencedirect.com/science/article/pii/S0032063311001954#bib258)). The previous expression corresponds to the limit when the number of hierarchical levels tends to [infinity](https://www.sciencedirect.com/topics/earth-and-planetary-sciences/infinity). Thus, the Akimov function seems to be as fundamental as the Lambert or Lommel–Seeliger laws.

2. A semi-empirical version of the Akimov disk function was developed for the moon:

$$
D(\alpha,\beta,\gamma) = \cos\frac{\alpha}{2}\cos\left( {\frac{\pi}{\pi - \alpha}\left( {\gamma - \frac{\alpha}{2}} \right)} \right)\frac{{(\cos\beta)}^{\eta\alpha(\pi - \alpha)}}{\cos\gamma}
$$

The parameter $\eta$  measures the deviation from the pure fractal-like surface with an infinite number of hierarchical levels.


**Phase function in a wide range of phase angles**

The equigonal albedo $A_{eq}$ depends on phase angle only, and is directly linked to the phase function $A_{eq}=A_Nf(\alpha)$. Here we consider two possible expressions for $f(\alpha)$.

1. The empirical formula suggested by [Akimov (1988b)](https://www.sciencedirect.com/science/article/pii/S0032063311001954#bib9) 

$$
f(\alpha)=\frac{e^{-\mu_1 \alpha}+m_0 e^{-\mu_2 \alpha}}{1+m_0}
$$

which contains three parameters: $μ_1$ is the parameter associated with [surface roughness](https://www.sciencedirect.com/topics/earth-and-planetary-sciences/surface-roughness), $m_0$ and $μ_2$ are, respectively, a characteristic of the amplitude and the width of the opposition peak. Constraining parameters $m_0$ and $μ_2$ requires measurements acquired for phase angles lower than 10° approximately.

2. Recently, [Velikodsky et al. (2011)](https://www.sciencedirect.com/science/article/pii/S0032063311001954#bib302) suggested a more accurate expression for approximating airless body phase function:

$$
A_{eq}(\alpha)=A_N(A_1 e^{-\mu_1 \alpha}+A_2 e^{-\mu_2 \alpha}+A_3 e^{-\mu_3 \alpha}), \ A_1+A_2+A_3=1
$$

- The first term $(A_1,μ_1)$, which has a maximal value of the exponent, approximately describes both the shadowing and non-shadowing opposition spike components. The first one may be caused by the effect of fractality (Shkuratov and Helfenstein, 2001), and the second one can be due to the coherent backscattering enhancement (Shkuratov, 1988, Hapke, 2002) and/or the lensing effect (Shkuratov, 1983, Trowbridge, 1984);
- The second term $(A_2,μ_2)$ corresponds to the single particle scattering and shadow-hiding effect in the regolith medium. It also depends on albedo due to incoherent multiple scattering between regolith particles ([Hapke, 1993](https://www.sciencedirect.com/science/article/pii/S0032063311001954#bib76));
- The third term $(A_3,μ_3)$ describes the shadow-hiding effect on the lunar surface topography ([Hapke, 1993](https://www.sciencedirect.com/science/article/pii/S0032063311001954#bib76)) and the exponent has a minimal value.


**Final expression for photometric model**

The photometric model we consider is expressed by the following empirical equation:

$$
A(\alpha,\beta,\gamma,\lambda)=A_n \frac{e^{-\mu_1 \alpha}+m_0 e^{-\mu_2 \alpha}}{1+m_0} \cos\frac{\alpha}{2}\cos\left( {\frac{\pi}{\pi - \alpha}\left( {\gamma - \frac{\alpha}{2}} \right)} \right)\frac{{(\cos\beta)}^{\eta\alpha(\pi - \alpha)}}{\cos\gamma}
$$

involving 5 parameters : $A_n=A_n(\lambda)=A_{eq}(0,\lambda)$ the normal albedo, $m_0=m_0(\lambda)$, $\mu_2=\mu_2(\lambda)$, the parameters of the opposition effect, $\mu_1=\mu_1(\lambda)$ the parameter linked to surface roughness, and $\eta=\eta(\lambda)$ which measures the deviation from the pure fractal-like surface. Note that constraining this model requires a large range of phase angle $0\lesssim \alpha \lesssim 120$ .

If very low phase angles are not accessible we may consider the simplified version :

$$
A(\alpha,\beta,\gamma,\lambda)=A_n e^{-\mu_1 \alpha} \cos\frac{\alpha}{2}\cos\left( {\frac{\pi}{\pi - \alpha}\left( {\gamma - \frac{\alpha}{2}} \right)} \right)\frac{{(\cos\beta)}^{\eta\alpha(\pi - \alpha)}}{\cos\gamma}
$$

reducing the number of parameter to 3: $A_n=A_n(\lambda)=A_{eq}(0,\lambda)$  the normal albedo, $\mu_1 =\mu_1(\lambda)$  the parameter linked to surface roughness, and $\eta=\eta(\lambda)$ which measures the deviation from the pure fractal-like surface.

Note that Shkuratov proposes an expression for the normal albedo if the optical and microphysical properties of the medium is known; i.e. Shkuratov's particulate model.

Note also that the reflectance factor we use in our modeling can be obtained by dividing the previous expression by $cos(i)$ :

$$
FR=A(\alpha,\beta,\gamma,\lambda)/\cos(i)=\frac{A(\alpha,\beta,\gamma,\lambda)}{\cos(\beta)\cos(\alpha-\gamma)}
$$

