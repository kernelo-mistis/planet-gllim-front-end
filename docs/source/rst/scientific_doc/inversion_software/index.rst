Description of the inversion workflow
=====================================

.. toctree::
    objectives_and_scope
    generation_of_a_learning_database
    initialisation_and_training_of_the_gllim_model
    prediction
    importance_sampling
    graphical_interface
    parameters
    summary_table_of_problem_dimensions