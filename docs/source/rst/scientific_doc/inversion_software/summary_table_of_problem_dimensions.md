Summary table of problem dimensions
===================================

The application works with a number of dimensions that will be used throughout this document. Here is the list and their details.

| Nom        | Description                                                  | Source                                                       |
| ---------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| nb_geom=D  | Number of available geometries                               | object *geometries* in the json file or cube prefix_geo (plan 3,4 et 5) |
| nb_param=L | Number of parameters of the physical model                   | Choice of the physical model (Hapke, Shkuratov, ...)         |
| nb_wave    | Number of wavelengths                                        | object *wavelengths* in the json file or label 'wavelengths' in  prefix_rho.hdr |
| nb_sample  | Number of sample in a lab experiment                         | Number of *reflectance_xxx* objects in the input json file   |
| nb_pts     | Number of spatial points in a planetary scene                | first dimension of the cubes *prefix_rho* or *prefix_drho*   |
| Nobs       | Number of Y vectors on which the physical model is inverted. | Experimental data: Nobs=nb_wave\*nb_sample  <br />Remote sensing data: Nobs=nb_wave\*nb_pts. |

