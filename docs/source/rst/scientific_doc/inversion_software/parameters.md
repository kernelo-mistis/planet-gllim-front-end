Parameters
==========

#### Configuration of the physical model

| Parameter      | Description                                                  | Type                  | Default value |
| -------------- | ------------------------------------------------------------ | --------------------- | ------------- |
| Physical model | Identifier of the physical model among "Hapke model" or "Shkuratov model" | Tag in drop down menu | "Hapke model" |
|                |                                                              |                       |               |

###### [Hapke model](<../photometric_models/hapke>)

| Parameter                                  | Description                                                  | Type               | Default value |
| ------------------------------------------ | ------------------------------------------------------------ | ------------------ | ------------- |
| Version of the Hapke model                 | Version of the Hapke model                                   | single choice list | 2002          |
| Variant of the Hapke model                 | Choice of the free parameters: "Full"-> (w,b,c,$θ_H$,B0,h); "Reduced"-> (w,b,c,$θ_H$); "Hockey stick"-> (w,$θ_H$) with (b,c) related by the Hockey stick empirical formula | single choice list | "Reduced"     |
| Maximal value of theta_bar                 | Value used to normalise the $θ_H$ parameter into the mathematical parameter space [0,1] | Float              | 30            |
| B0 : Magnitude of the opposition effect    | If variant of the Hapke model is "reduced" or "Hockey stick", a fixed value is to be provided for parameter B0 | Float              | 0             |
| H : Angular width of the opposition effect | If variant of the Hapke model is "reduced" or "Hockey stick", a fixed value is to be provided for parameter h | Float              | 0.1           |
| Geometries file                            | file containing 3 vectors of floating reals of dimension `[D]`: *sza* (incidence angle) , *vza* (emergence angle), and *phi* (azimuth angle). | json               | -             |
|                                            |                                                              |                    |               |

###### [Shkuratov model](../photometric_models/shkuratov)

| Parameter            | Description                                                  | Type  | Default value |
| -------------------- | ------------------------------------------------------------ | ----- | ------------- |
| Maximal value of An  | Value used to normalise the An parameter into the mathematical parameter space [0,1] | Float | 1.0           |
| Minimal value of An  | Value used to normalise the An parameter into the mathematical parameter space [0,1] | Float | 0.            |
| Maximal value of mu1 | Value used to normalise the mu1 parameter into the mathematical parameter space [0,1] | Float | 1.5           |
| Minimal value of mu1 | Value used to normalise the mu1 parameter into the mathematical parameter space [0,1] | Float | 0.            |
| Maximal value of nu  | Value used to normalise the mu1 parameter into the mathematical parameter space [0,1] | Float | 1.0           |
| Minimal value of nu  | Value used to normalise the mu1 parameter into the mathematical parameter space [0,1] | Float | 0.2           |
| Maximal value of m0  | Value used to normalise the m0 parameter into the mathematical parameter space [0,1] | Float | 1.5           |
| Minimal value of m0  | Value used to normalise the m0 parameter into the mathematical parameter space [0,1] | Float | 0.            |
| Maximal value of mu2 | Value used to normalise the mu2 parameter into the mathematical parameter space [0,1] | Float | 1.5           |
| Minimal value of mu2 | Value used to normalise the mu2 parameter into the mathematical parameter space [0,1] | Float | 0.            |
| Geometries file      | file containing 3 vectors of floating reals of dimension `[D]`: *sza* (incidence angle) , *vza* (emergence angle), and *phi* (azimuth angle). | json  | -             |
|                      |                                                              |       |               |

#### [Configuration of the data generation module](<generation_of_a_learning_database>)

| Parameter             | Description                                                  | Type                  | Default value               |
| --------------------- | ------------------------------------------------------------ | --------------------- | --------------------------- |
| Data Generation model | Type of Gaussian statistical model to generate the learning dataset  $F(X)+\epsilon$, $\epsilon$ being a centered Gaussian noise: "basic" or "dependent" | Tag in drop down menu | "Basic gaussian stat model" |
| Dataset size          | Number of (X,Y) pairs to be generated in the learning dictionary. | Integer               | 10000                       |
| Generator type        | The random generator produces X vectors in $[0,1]^L$. Its type can be "latin hypercube", "random", or "sobol". | single choice list    | "sobol"                     |
| Seed of the generator | The seed used by the random generator                        | Integer               | System time                 |
| Variances             | In case of a "Basic gaussian stat model": the isometric fixed variance of the Gaussian noise $\epsilon$ is given in relative unit (%) | Float                 | 0.01%                       |
| Noise effect          | In case of a "Dependent gaussian stat model": the standard deviation of the Gaussian noise $\epsilon$ depends on the data: $\sigma_{obs}=F(X)/r$, with r the signal to noise ratio to be specified. | Integer               | 20                          |
|                       |                                                              |                       |                             |

#### [Configuration of the GLLiM model](<initialisation_and_training_of_the_gllim_model>)

| Parameter                          | Description                                                  | Type                  | Default value               |
| ---------------------------------- | ------------------------------------------------------------ | --------------------- | --------------------------- |
| Number of affine functions         | Number of affine transformations in the GLLiM model          | Integer               | 50                          |
| Type of Gamma matrices             | Type of covariance matrix for the K GLLiM components: "Full matrices", "Diagonal matrices", or "isomorphic" matrices. | single choice list    | "Full matrices"             |
| Type of Sigma matrices             | Type of covariance matrix for the Gaussian noise applied to each affine transformation: "Full matrices", "Diagonal matrices", or "isomorphic" matrices | single choice list    | "Diagonal matrices"         |
| Floor                              | Minimum threshold for the covariance values (Low covariances are regularized to insure numerical stability) | Float                 | 1.e-8                       |
| Initialisation step                | Initialisation strategy applied to the GLLiM learning "Fixed" or "Multiple" | Tag in drop down menu | "Multiple "Initialisations" |
| Configuration of the learning step | Learning step strategy: "GLLiM-EM method" or "GMM-EM method" | Tag in drop down menu | "GLLiM-EM method"           |
|                                    |                                                              |                       |                             |

###### Multiple initialisation configuration

| Parameter                  | Description                                                  | Type    | Default value |
| -------------------------- | ------------------------------------------------------------ | ------- | ------------- |
| Number of initialisations  | Number of initialization experiments                         | Integer | 10            |
| Seed of the generator      | The seed used by random generators                           | Integer | System time   |
| K-means iterations         | Number of iterations of the k-means algorithm to initialize the clusters | Integer | 5             |
| GMM-EM iterations          | Number of iterations of the GMM-EM algorithm after initialisation by the K-means. | Integer | 10            |
| Iterations of the GLLiM-EM | Number of iterations for the GLLiM-EM algorithm              | Integer | 5             |
|                            |                                                              |         |               |

###### Fixed initialisation configuration

| Parameter             | Description                                                  | Type    | Default value |
| --------------------- | ------------------------------------------------------------ | ------- | ------------- |
| Seed of the generator | The seed used by random generator                            | Integer | System time   |
| K-means iterations    | Number of iterations of the k-means algorithm to initialize the clusters | Integer | 5             |
| GMM-EM iterations     | Number of iterations of the GMM-EM algorithm after the initialization of the K-means. | Integer | 10            |
|                       |                                                              |         |               |

###### Configuration of the GLLiM Expectation-Maximisation "GLLiM-EM method"

| Parameter           | Description                                                  | Type    | Default value |
| ------------------- | ------------------------------------------------------------ | ------- | ------------- |
| GLLiM-EM iterations | Number of iterations for the GLLiM-EM algorithm              | Integer | 100           |
| likelihood increase | The GLLiM-EM algorithm stops if the gain in log-likelihood between two iterations is lower than this parameter | Float   | 0.00001       |
|                     |                                                              |         |               |

###### Configuration of the training strategy by the joint GMM model "GMM-EM  method"

| Parameter          | Description                                                  | Type    | Default value |
| ------------------ | ------------------------------------------------------------ | ------- | ------------- |
| K-means iterations | Number of iterations of the k-means algorithm to initialize the clusters | Integer | 10            |
| GMM-EM iterations  | Number of iterations of the GMM-EM algorithm after initialisation by the K-means. | Integer | 100           |
|                    |                                                              |         |               |

#### [Configuration of the prediction module](<prediction>)

| Parameter                                         | Description                                                  | Type    | Default value |
| ------------------------------------------------- | ------------------------------------------------------------ | ------- | ------------- |
| Number of predictions : Prediction by the center  | Final number of components $M$ to retain after merging the initial K component Gaussian mixture. The prediction is performed for each components (centroids). | Integer | 2             |
| Size of the reduced GMM : Prediction by the means | Final number of components $M$ to retain after merging the initial K component Gaussian mixture. The prediction is the mean of the reduced mixtures. | Integer | 2             |
| Minimum weight a GMM component                    | Any initial component which weight is lower than this threshold is discarded before the merging. | Float   | 1e-10         |
|                                                   |                                                              |         |               |

#### [Configuration of the importance sampling module](<importance_sampling>)

| Parameter                                   | Description                                                  | Type                  | Default value |
| ------------------------------------------- | ------------------------------------------------------------ | --------------------- | ------------- |
| Number of samples                           | Number of samples ($N_{samples}$) generated for the importance sampling of the target PDF | Integer               | 1000          |
| Proposition law : Prediction by the mean    | Type of proposition law used to estimate the mean of the posterior PDF. Currently, only the Gaussian mixture model is offered "gaussian". | Tag in drop down menu | "gaussian"    |
| Proposition law : Prediction by the centers | Type of proposition law used to estimate the modes of the posterior PDF. Currently, only the Gaussian mixture model is offered "gaussian". | Tag in drop down menu | "gaussian"    |
| $N_0$     | (optional) Number of samples at initial stage, in the Incremental Mixture Important Sampling (IMIS) algorithm.                                | Integer               | $N_{samples}/10$          |
| $B$       | (optional) Number of new samples at each step, in the Incremental Mixture Important Sampling (IMIS) algorithm. It must be lower than $N_0$ ($B \leq N_0$).                    | Integer               | $N_{samples}/20$          |
| $J$       | (optional) Number of iterations, in the Incremental Mixture Important Sampling (IMIS) algorithm. At the end of the algorithm, there are $N_{samples} = N_0+J∗B$ samples.   | Integer               | $18$         |
|                                             |                                                              |                       |               |

#### Configuration of the observations

| Parameter           | Description                                                  | Type                  | Default value    |
| ------------------- | ------------------------------------------------------------ | --------------------- | ---------------- |
| Observations source | types of data to be analysed: from ["Laboratory"](<../inputs_description/laboratory_spectrophotometric_data_in>) or ["Remote sensing"](<../inputs_description/space_remote_sensing_data_in>) | Tag in drop down menu | "Remote sensing" |
| Observations file   | json file or spectral image cubes depending on the data type. | json or envi          | -                |
|                     |                                                              |                       |                  |