Objectives and scope
====================

The application implements the GLLiM statistical learning technique in its different variants for the inversion of a physical model of reflectance on spectro-(gonio)-photometric data. The latter are of two types: 

- laboratory measurements of reflectance spectra acquired according to different illumination and viewing geometries, 
- and 4D spectro-photometric remote sensing products from multi-angular CRISM or Pléiades acquisitions. 

In both cases the data can be decomposed into a series of vectors of observables $Y_{obs}$  of dimension `D`, that represent in our planetary science case reflectance values for a fixed set of `D` geometries. From a vector $Y_{obs}$, the objective is to estimate the mean or the most probable value for each of the L parameters (the components of vector X) of the inverted physical model and to provide a measurement of uncertainty about the estimations. The estimation is performed in four steps:

1. The generation of a learning database made of couples (X,Y) using the direct physical model,
2. The GLLiM learning phase to construct a direct then inverse parametric statistical model of the functional Y=F(X). The learned model is a Gaussian mixture model with a structured parameterization (see [Deleforge et al 2015](https://link.springer.com/article/10.1007/s11222-014-9461-5), [Kugler et al 2022](https://link.springer.com/article/10.1007/s11222-021-10019-5) for details).  It  depends on the physical direct model and on the fixed set of `D` geometries through the use of the previous database. 
3. This statistical model is then applied to a number of $Y_{obs}$ vectors (corresponding to the spectral and possibly spatial dimensions of the problem) in order to build a set of a posteriori Probability Distribution Functions (PDFs) $p_{gllim}(X \vert Y_{obs})$. 
4. The PDFs are then exploited each independently by different techniques to estimate the solution $\hat{X}$ corresponding to each vector : estimation by the mean, fusion of the components of the Gaussian mixture model into a small number of centroids (usually two or three) to identify possible multiple solutions, importance sampling of the target PDF $p(X \vert Y_{obs})$ with the proposal distribution set to $p_{gllim}(X \vert Y_{obs})$ around the mean or around the centroids, [Kugler et al 2022](https://link.springer.com/article/10.1007/s11222-021-10019-5). 