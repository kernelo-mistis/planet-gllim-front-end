Prediction
==========

The prediction phase aims at inverting the physical model, that is at  predicting parameter values corresponding to a given observation. It is performed for each observation $y_{obs}$ which is accompanied by its measurement error $\epsilon_{obs}$ (assimilated to a standard deviation). Accounting for the additional information $\epsilon_{obs}$  requires a slight adjustment of the model before inversion. Before inverting the GLLiM model, we adjust the covariance matrices $\Sigma_{k}$ by adding to their diagonal the variance of the measurement error of the observation $\left(\epsilon_{obs}^{2}\right)$. The formulas leading to the inverse GLLiM can then be used and lead to an inverse PDF. This PDF is by construction a mixture of Gaussian distributions whose parameters depend on $y_{obs}$. It is straightforward to compute its expectation, which can be used as an estimation of the parameters. This mixture can also be exploited differently depending on the prediction method chosen as detailed below.

##### Prediction by the mean

The means of the different components in the mixture of Gaussian distributions are all available in closed-form. The overall mean of the mixture is their barycenter and can be used as an estimate of $x \in F^{-1}\left(y_{o b s}\right)$. Denoting by $w _{k}$ <!--definition?--> the weight of the k-th component of the mixture and $m_{k}$ its mean and $\Sigma_{k}$ its covariance matrix, the expectation is given by:

$$
x_{\text {pred }}=\sum_{k=1}^{K} w_{k} * m_{k}
$$

The corresponding covariance matrix of the mixture is  also available in closed-form and is given by:

$$
\sum_{k=1}^{K} \Sigma_{k}+m_{k} * m_{k}^{T} w_{k}-x_{p r e d} x_{p r e d}^{T}.
$$


##### Prediction by the centers

In the case where $y_{o b s}$ may be equally well explained by different vectors of parameter values,  the barycenter in the prediction by the mean may be very far from $F^{-1}\left(y_{obs}\right)$. Typically the mean in that case could end up in a very low probability region of the parameter space. To handle this case, one may look at the mode of the distribution instead representing the highest probability value. Finding this  mode for a Gaussian mixture distribution is not easy in general but the means of each components are generally good candidate local modes. The latter are called centers in what follows. The centers are good candidates as possible predictions instead of their average. However the size of the mixture can be very large and certainly it contains components carrying little information. The latter are filtered out when their weight is lower than a threshold. Then the method consists in further reducing the size of the mixture by merging the centers using a Kullback-Leibler distance criterion (see [Kugler et al 2022](https://link.springer.com/article/10.1007/s11222-021-10019-5) for the specific procedure used) <!--to be verified--> and keeping $M<K$ the most relevant centers (in our remote sensing science case we take $M=2$).