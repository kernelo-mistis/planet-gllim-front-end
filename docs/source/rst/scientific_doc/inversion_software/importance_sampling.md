Importance sampling
===================

In the Bayesian inversion framework the a posteriori conditional probability density function is $\phi=p(\mathbf{x} \mid \mathbf{y}) \propto p(\mathbf{y} \mid X=\mathbf{x}) p(\mathbf {x})$. The Importance Sampling technique is based on the following observation: if $\psi$ is another density (nonzero on the support of $\phi$ ), then:

$$
\mathbb{E}[f(X)]=\int f(\mathbf{x}) \phi(\mathbf{x}) d \mathbf{x}=\int \frac{\phi(\mathbf{x})}{\psi(\mathbf{x})} f(\mathbf{x}) \psi(\mathbf{x}) d \mathbf{x}=\mathbb{E}\left[\frac{\phi\left(X^{\prime}\right)}{\psi\left(X^{\prime}\right)} f\left(X^{\prime}\right)\right]
$$

$X^{\prime} \sim \psi$. If we know how to sample $\psi$, the law of large numbers therefore gives a simple estimator of $\mathbb{E}[f(X)]:$ if $\mathbf{x}_{1}, \ldots, \mathbf{x}_{N}$ is a sample of the distribution $\psi$:

$$
\sum_{n=1}^{N} \frac{\phi\left(\mathbf{x}_{n}\right)}{\psi\left(\mathbf{x}_{n}\right)} f\left(\mathbf{x}_{n}\right) \underset{N \rightarrow \infty}{\longrightarrow} \mathbb{E}[f(X)]
$$

We can generalize this result even when $\phi$ is only known up to a multiplicative constant: $\phi(\mathbf{x})=c \tilde{\phi}(\mathbf{x})$

If $\mathbf{x}_{1}, \ldots, \mathbf{x}_{N}$ is a sample of the law $\psi$, noting $w_{i}:=\frac{\tilde {\phi}\left(\mathbf{x}_{i}\right)}{\psi\left(\mathbf{x}_{i}\right)}$, we have:

$$
\frac{\sum_{n=1}^{N} w_{i} f\left(\mathbf{x}_{n}\right)}{\sum_{n=1}^{N} w_{i}} \underset{N \rightarrow \infty}{\longrightarrow} \mathbb{E}[f(X)]
$$

We thus take as a target law $\tilde{\phi}(\mathbf{x})=p(\mathbf{y} \mid X=\mathbf{x}) p(\mathbf {x})$ and the a-posteriori GLLiM law, which is a good approximation of the target law, provides a natural candidate for the proposition law $\psi(\mathbf{x})=p_{G}(\mathbf{x} \mid Y=\mathbf{y})$. As $\psi$ is a Gaussian mixture law, it is easy to generate a sample $\left(\mathbf{x}_{1}, \ldots \mathbf{x}_{I}\right)$ of size $I$ following $\psi$, and to calculate the estimator of the mean by Importance Sampling (IS):

$$
\overline{\mathbf{x}}_{I S-G}(\mathbf{y}):=\frac{1}{\sum_{i=1}^{I} w_{i}} \sum_{i=1}^{I} w_{i} \mathbf{x}_{i} \quad, \quad \operatorname{with} w_{i}:=\frac{p\left(\mathbf{y} \mid X=\mathbf{x}_{i}\right) p\left(\mathbf{x}_{i}\right)}{p_{G}\left(\mathbf{x}_{i} \mid Y=\mathbf{y}\right)}
$$

The associated variance is given by the following expression:

$$
\operatorname{var}(\mathbf{y}):=\frac{1}{\sum_{i=1}^{I} w_{i}} \sum_{i=1}^{I} w_{i}\left(\mathbf{x}_{i}-\mu\right)^{\mathrm{t}}\left(\mathbf{x}_{i}-\mu\right)
$$

The parameter $I$ is therefore the number of samples generated for the importance sampling of the target PDF according to the proposition law that is, in the present version of the software, a "Gaussian" mixture model. In practice the choice of $I$ shall ensure a compromise between the mathematical precision and the computation time. The duration of this importance sampling step indeed dominates the rest of the procedure, because, for each observation $y_{obs}$, the calculation of the weights $w_{i}$ requires evaluating $I$ times the functional F and the mixing law GLLiM. In comparison, a simple prediction by the GLLiM mean requires only averaging and no evaluation of F.

The importance Sampling (IS) technique is operated in a very similar manner to refine the prediction of the GLLiM centers by calculating an estimator of the modes of the a posteriori conditional probability density function $\phi$. The distance between the mode and the center $\boldsymbol{c}_{k}^{m}$ of the prior must be relatively small.

The estimator of the centroid $k$ by importance sampling $\overline{\mathbf{x}}_{I S \text {-centroid }, k}$ is therefore obtained by generating $I$ samples $\left(\mathbf{x} _{1}^{k}, \ldots, \mathbf{x}_{I}^{k}\right)$ according to the law $\mathcal{N}\left(\boldsymbol{c}_{k} ^{m}, \boldsymbol{\Gamma}_{k}^{m}\right)$ and calculating:

$$
\overline{\mathbf{x}}_{I S-\text { centroid }, k}(\mathbf{y}):=\frac{1}{\sum_{i=1}^{I} w_{i}^{(k)}} \sum_{i=1}^{I} w_{i}^{(k)} \mathbf{x}_{i}^{k} \quad, \quad \operatorname{avec} w_{i}^{(k)}:=p\left(\mathbf{y} \mid X=\mathbf{x}_{i}^{k}\right) p\left(\mathbf{x}_{i}^{k}\right)
$$

The associated variance is:

$$
\operatorname{var}(\mathbf{y}):=\frac{1}{\sum_{i=1}^{I} w_{i}^{(k)}} \sum_{i=1}^{I} w_{i}^{(k)}\left(\mathbf{x}_{i}^{(k)}-\mu\right)^{\mathrm{t}}\left(\mathbf{x}_{i}^{(k)}-\mu\right)
$$

where $\mu:=\overline{\mathbf{x}}_{I S-\text { centroid, } k}(\mathbf{y})$.