# Fundamentals of Planet-gllim, 

# a tool for the physical analysis of multi-dimensional data in planetary remote sensing.

2022 Edition

Sylvain Douté <sylvain.doute@univ-grenoble-alpes.fr> IPAG

**Planetgllim Team**:

Florence Forbes <florence.forbes@inria.fr>

Stan Borkowski <stan.borkowski@inria.fr>

Samuel Heidmann <samuel.heidmann@inria.fr>

Benoit Kugler <benoit.kugler@inria.fr>

## Introduction

Sensors aboard observation platforms in orbit around the Earth and other planets of the Solar System return huge volume of data. These data are high-dimensional covering space, wavelengths, time, angles, etc. in different spectral domains and different regimes (reflection, emission, active sensing). There are different approaches to analyse the data and retrieve the information of interest. One of them is inversion that refers to a situation where one aims at determining the causes of a phenomenon from experimental observations of its effects. The resolution of such a problem generally starts by the so-called *direct or forward* modelling of the phenomenon. It theoretically describes how input parameters x ∈ X are translated into effects y ∈ Y. Then, from experimental observations of these effects, the goal is to find the parameter values that best explain the observed measures. In remote sensing, the observations y are high-dimensional (of dimension D) because they represent signals in time, angles or wavelength. Besides many such high-dimensional observations are available and the application requires a very large number of inversions (denoted by N~obs~ in what follows). The parameters x to be predicted (of dimension L) is itself multi-dimensional with correlated dimensions. 
Kugler et al. 2020 [refxxx] presented a statistical learning technique with a Bayesian framework capable of solving the problem of inverting physical models on multi-angular data in order to estimate the value of their parameters. The method is able to address: 1) the large number of observations to be analysed, 2) their large size, 3) the need to provide predictions for several correlated parameters 4) the possibility of managing a possible multiplicity of solutions and 5) the requirement to accompany the latter with a quality measure (e.g. uncertainty). With the planetgllim software we implement the method that is optimised in the case where D is greater than L, with L typically smaller than ten and D about few tens, while the number of observations to be inverted N~obs~ can be of the order of a few millions. 

## An illustrative science case

In planetary science, information on the microtexture of surface materials such as grain size, shape, roughness and internal structure can be used as tracers of geological processes. This information is accessible by remote sensing under certain conditions thanks to multi-angular optical observations. Around Mars, the CRISM instrument [refxxx] acquires sequences of hyperspectral images in the visible and infrared from eleven different angles when the Mars Reconnaissance Orbiter flies over a site. Such observations are available for hundreds of Martian sites of interest. In the terrestrial domain, the Pléiades satellites [refxxx] have great agility allowing off-nadir acquisition up to around 50° with their onboard cameras operating in a panchromatic band (480–820 nm) and in four multispectral bands (450–915 nm). In the seminal work of [refxxx], the characterisation of Martian materials by orbital spectrophotometry is conducted thanks to the MARS-ReCO tool [refxxx]. MARS-ReCO extracts a parametric model of surface reflectance (BRDF RTLS) by atmospheric correction for 544 visible and near-infrared wavelengths and for an network of several thousand points distributed over the scene. The interpretation of the BRDF extracted by "MARS-ReCO" in terms of composition and microtexture is based on the inversion of physical models of radiative transfer (Hapke  [refxxx] and Shkuratov  [refxxx]) linking in a nonlinear way physical and observable parameters (functional Y=F (X)). In this case Y is a vector of D reflectance values estimated for D acquisition geometries, X is vector of photometric parameters. The great number of observations results from the combination of spectral and spatial sampling of the scene, of the order of 4.10^5^. The interpretation of the remote sensing data is also based on laboratory goniometric optical measurements on meteoritic, minerals and other planetary analogues [refxxx]. Such an experiment leads to a series of reflectance spectra acquired at different different illumination and viewing angles constituting a spectro-gonio-photometric dataset on which the Hapke and Shkuratov models can be inverted [refxxx].

## Detail of the photometric models

### Definition of geometry

There are different sets of angles that characterize the geometry of illumination and observation of a [planetary surface](https://www-sciencedirect-com.gaelnomade-1.grenet.fr/topics/earth-and-planetary-sciences/planetary-surface) or of a sample in the laboratory.  One such a set includes the incident and emerging angles *i* and *e*, and [azimuth](https://www-sciencedirect-com.gaelnomade-1.grenet.fr/topics/earth-and-planetary-sciences/azimuth) *φ* that is the angle between the planes of scattering and incidence ([Fig. 1](https://www-sciencedirect-com.gaelnomade-1.grenet.fr/science/article/pii/S0032063311001954#f0005)). 

For many applications an another set can be used. It includes the so-called photometric (luminance) coordinates *α*, *β*, and *γ* that are the phase angle, and photometric latitude and [longitude](https://www-sciencedirect-com.gaelnomade-1.grenet.fr/topics/earth-and-planetary-sciences/longitude), respectively. These sets depend on each other as follows :

![](_resources/7c97e63d2e81cfa191e252527be3490b.png)
and
![Geometry of a typical passive remote sensing acquisition](_resources/68dee73d2e86f2f75f17231149e32bf2.png)
<img src="_resources/69976d008fb057e5273ca8e7e5d44c7e.jpg" alt="" width="237" height="433" class="jop-noMdConv">
The radiance factor also known as “I/F”, is
![](_resources/6e863c5c5e266c6af125df1df13cd9ef.png)

A photometric model for the surface provides an analytical expression for ![](/Users/doutes/.config/joplin-desktop/resources/1a4aa127bd3e88ed75bfb862b082d8a5-5717853.png).

The observations that are processed by planetgllim are multi-angular: for the same laboratory sample (or the same spatial point in a planetary scene), we have a reflectance measurement for each geometric configuration $`G_d`$ of a series of D geometries (d=1,...,D) where $`G = (i,e,φ)`$. We consider that G is known precisely, fixed by the measurement context. Thus, when performing the inversion, the functional to be considered is evaluated as a vector and is defined by a D-tuple of geometric configurations. In the context of real data, these configurations are controlled by the experiment: a satellite, subject to operational constraints, generally covers about ten configurations; in the laboratory, we can obtain on the order of tens of measurements.

### The Hapke photometric model

The Hapke model is a radiative transfer model introduced by B. Hapke in [Hapke, 1981, Hapke, 1984, Hapke, 1986] which models the interaction of light in a particulate medium and provides an analytical expression for reflectance. This model makes it possible to explain a photometric measurement with relatively few parameters like the absorptivity of the particles, their scattering cross sections, their phase function and the macroscopic roughness of the material. Several versions and refinements of the model have been proposed. The version proposed in [Hapke, 2002] links the physical parameters $`(w, b, c, \theta_H, B_0, h)`$ to the bidirectional reflectance by the following formula:

```{math}
R(i,e,g)=\frac{w}{4π} \frac{μ_{0eG} (θ_H )}{μ_{0eG} (θ_H )+μ_{eG} (θ_H )} [P_G(\alpha,b,c)(1+ B_G(B_0, h)+M(μ_{0eG}(θ_H ),{μ_eG}(θ_H ) )]× S_G(i,e,\alpha,(θ_H ))
```

G encodes the geometric measurement configuration as described in the previous section. The physical parameters and the related quantities $`G, μ_{0e}, μ_e, S_G, B_G, P_G`$ and $`M`$ are described below.

| **Symbol** | **Parameter**                                     | **Physical meaning**                                    | **Interval of variation** |
| ---------- | ------------------------------------------------- | ------------------------------------------------------- | ------------------------- |
| w          | Single scattering albedo                          | Scattering at the grain level                           | [0  ; 1]                  |
| b          | Anisotropy parameter                              | b=0   isotropic scattering  b=1  directional scattering | [0  ; 1]                  |
| c          | Backward or forward scattering coefficient        | c  > 0.5  backscattering   c  < 0.5  forward scattering | [0  1;]                   |
| $`θ_H`$    | Photometric roughness                             | Mean slope angle averaged on all scales                 | [0°  ; 90°]               |
| B0         | Amplitude  of the shadow hiding opposition effect | Opacity of the grains                                   | [0  ; 1]                  |
| h          | Width of the shadow hiding opposition effect      | Porosity of the granular medium                         | [0  ; 1]                  |

> Single scattering albedo ω and multiple scattering function H

The albedo w describes the brightness of a particle and represents the proportion of the radiation scattered by a single particle compared to the incident radiation. It intervenes in the multiple scattering function M, which is defined (in the version of [Hapke, 2002]) by:

```{math}
M(μ_{0eG}(θ_H ),{μ_eG}(θ_H ) )=H(w,μ_{0eG}(θ_H ))H(w,μ_{eG}(θ_H ))-1 \\
H(w,x)=\frac{1+2x}{1+2\sqrt{(1-w)}}
```

where $`x`$ is $`μ_{0eG}`$ or $`μ_{eG}`$. 

> Parameters b and c of the $`P_G`$ phase function 

The $`P_G`$ phase function characterizes the angular distribution of the scattered energy by a particle. It depends on the parameter b which is related to the anisotropy of the diffusing lobe and on the parameter c which defines the main direction of diffusion. It is defined by:

```{math}
P_G(b,c)=(1-c)\frac{1-b^2}{(1+2b\cos(\alpha)+b^2)^{3/2}}+c \frac{1-b^2}{(1-2b\cos(\alpha)+b^2)^{3/2}}
```

Note that we choose here [0, 1] as the domain of variation of c, which corresponds to the convention of [Schmidt and Fernando, 2015].[Hapke, 2002], meanwhile, uses [−1, 1].

> Macroscopic roughness $`\theta_H`$

The macroscopic roughness factor $`S_G`$ is constructed from a description of the surface topography as a set of facets where the tangent of the slopes follows a Gaussian distribution, with mean slope angle $`θ_H`$. The macroscopic roughness parameter  $`θ_H`$ is also involved in the expression of $`μ_{0eG}`$ and $`μ_{eG}`$ which correspond to the cosine of the equivalent angle of incidence and emergence of a rough surface. We refer to [Hapke, 1993] for the exact expression of $`S_G`$, $`μ_{0eG}`$ and $`μ_{eG}`$.

> Parameters $`B_0`$ and $`h`$ of the opposition effect

The opposition effect corresponds to the increase in the light intensity diffused by a granular medium, at small phase angles ([Hapke, 1986, Hapke, 2002, Hapke, 2021] ) due to shadow hiding. In the model of [Hapke, 2002], it is parameterized by the amplitude $`B_0`$ and the width of the opposition peak h, linked by the equation:

```{math}
B_G(h,B_0)=\frac{B_0}{1+1/h\tan(\alpha)}
```

This physical process is maximum for phase angles close to zero, but becomes negligible past 20◦. As it is difficult to obtain measurements for small phase angles, it is common not to try to constrain $`B_0`$ and $`h`$ and to set them to a default value. We then obtain a Hapke model with 4 parameters $`(ω,θ_H,b,c)`$.

### The Shkuratov photometric model

Here we summarise the theory presented in the seminal papers of refxxx.

In this section we consider photometric models that can be separated in a phase function and a disk function : $`r_F=A_{eq}(\alpha)D(\mu_0,\mu,\alpha)`$ 

The equigonal albedo, or phase function, describes the phase dependence of the brightness: $`A_{eq}=A_Nf(\alpha)`$ 

where $`A_N`$ is the normal albedo, and $`f(α)`$ is the phase function normalized to unity at $`α=0°`$. The latter depends on the choice of disk function *D*, which describes how the reflectance varies over the planetary disk at constant phase angle.

**Disk functions D**

We consider two well-known disk functions, each normalized at $`i=e=\alpha=0`$ 

1/ the parameter-free version of the Akimov function which was derived theoretically for an extremely rough surface that is slightly randomly undulated :

```{math}
D(\alpha,\beta,\gamma) = \cos\frac{\alpha}{2}\cos\left( {\frac{\pi}{\pi - \alpha}\left( {\gamma - \frac{\alpha}{2}} \right)} \right)\frac{{(\cos\beta)}^{\alpha/(\pi - \alpha)}}{\cos\gamma}
```

Analysis of light scattering by different complex structures leads to the notion about equifinality of a scattering law ([Shoshany, 1991](https://www-sciencedirect-com.gaelnomade-1.grenet.fr/science/article/pii/S0032063311001954#bib270)), when a significant number of different complex structures exhibit the same scattering law. For example, the Akimov function can be rigorously derived not only for the case of utterly rough surfaces ([Akimov, 1976](https://www-sciencedirect-com.gaelnomade-1.grenet.fr/science/article/pii/S0032063311001954#bib5), [Akimov, 1988a](https://www-sciencedirect-com.gaelnomade-1.grenet.fr/science/article/pii/S0032063311001954#bib8), [Shkuratov et al., 1994a](https://www-sciencedirect-com.gaelnomade-1.grenet.fr/science/article/pii/S0032063311001954#bib249)), but also for fractal-like surfaces with the same Gaussian statistics of local slopes at each hierarchical level of roughness ([Shkuratov et al., 2003a](https://www-sciencedirect-com.gaelnomade-1.grenet.fr/science/article/pii/S0032063311001954#bib258)). The previous expression corresponds to the limit when the number of hierarchical levels tends to [infinity](https://www-sciencedirect-com.gaelnomade-1.grenet.fr/topics/earth-and-planetary-sciences/infinity). Thus, the Akimov function seems to be as fundamental as the Lambert or Lommel–Seeliger laws.

2/ A semi-empirical version of the Akimov disk function was developed for the moon:

```{math}
D(\alpha,\beta,\gamma) = \cos\frac{\alpha}{2}\cos\left( {\frac{\pi}{\pi - \alpha}\left( {\gamma - \frac{\alpha}{2}} \right)} \right)\frac{{(\cos\beta)}^{\eta\alpha/(\pi - \alpha)}}{\cos\gamma}
```

The parameter $`\eta`$  measures the deviation from the pure fractal-like surface with an infinite number of hierarchical levels.

**Phase function in a wide range of phase angles**

The equigonal albedo $`A_{eq}`$ depends on phase angle only, and is directly linked to the phase function $`A_{eq}=A_Nf(\alpha)`$. Here we consider two possible expressions for $`f(\alpha)`$.

1/ The empirical formula suggested by [Akimov (1988b)](https://www-sciencedirect-com.gaelnomade-1.grenet.fr/science/article/pii/S0032063311001954#bib9) 

```{math}
f(\alpha)=\frac{e^{-\mu_1 \alpha}+m_0 e^{-\mu_2 \alpha}}{1+m_0}
```

which contains three parameters: $`μ_1`$ is the parameter associated with [surface roughness](https://www-sciencedirect-com.gaelnomade-1.grenet.fr/topics/earth-and-planetary-sciences/surface-roughness), $`m_0`$ and $`μ_2`$ are, respectively, a characteristic of the amplitude and the width of the opposition peak. Constraining parameters $`m_0`$ and $`μ_2`$ requires measurements acquired for phase angles lower than 10° approximately.

2/ Recently, [Velikodsky et al. (2011)](https://www-sciencedirect-com.gaelnomade-1.grenet.fr/science/article/pii/S0032063311001954#bib302) suggested a more accurate expression for approximating airless body phase function:

```{math}
A_{eq}(\alpha)=A_N(A_1 e^{-\mu_1 \alpha}+A_2 e^{-\mu_2 \alpha}+A_3 e^{-\mu_3 \alpha}), \ A_1+A_2+A_3=1
```

- The first term $`(A_1,μ_1)`$, which has a maximal value of the exponent, approximately describes both the shadowing and non-shadowing opposition spike components. The first one may be caused by the effect of fractality (Shkuratov and Helfenstein, 2001), and the second one can be due to the coherent backscattering enhancement (Shkuratov, 1988, Hapke, 2002) and/or the lensing effect (Shkuratov, 1983, Trowbridge, 1984);
- The second term $`(A_2,μ_2)`$ corresponds to the single particle scattering and shadow-hiding effect in the regolith medium. It also depends on albedo due to incoherent multiple scattering between regolith particles ([Hapke, 1993](https://www-sciencedirect-com.gaelnomade-1.grenet.fr/science/article/pii/S0032063311001954#bib76));
- The third term $`(A_3,μ_3)`$ describes the shadow-hiding effect on the lunar surface topography ([Hapke, 1993](https://www-sciencedirect-com.gaelnomade-1.grenet.fr/science/article/pii/S0032063311001954#bib76)) and the exponent has a minimal value.

**Final expression for photometric model**

The photometric model we consider is expressed by the following empirical equation:

```{math}
A(\alpha,\beta,\gamma,\lambda)=A_n \frac{e^{-\mu_1 \alpha}+m_0 e^{-\mu_2 \alpha}}{1+m_0} \cos\frac{\alpha}{2}\cos\left( {\frac{\pi}{\pi - \alpha}\left( {\gamma - \frac{\alpha}{2}} \right)} \right)\frac{{(\cos\beta)}^{\eta\alpha/(\pi - \alpha)}}{\cos\gamma}
```

involving 5 parameters : $`A_n=A_n(\lambda)=A_{eq}(0,\lambda)`$ the normal albedo, $`m_0=m_0(\lambda)`$, $`\mu_2=\mu_2(\lambda)`$, the parameters of the opposition effect, $`\mu_1=\mu_1(\lambda)`$ the parameter linked to surface roughness, and $`\eta=\eta(\lambda)`$ which measures the deviation from the pure fractal-like surface. Note that constraining this model requires a large range of phase angle $`0\lesssim \alpha \lesssim 120`$ .

If very low phase angles are not accessible we may consider the simplified version :

```{math}
A(\alpha,\beta,\gamma,\lambda)=A_n e^{-\mu_1 \alpha} \cos\frac{\alpha}{2}\cos\left( {\frac{\pi}{\pi - \alpha}\left( {\gamma - \frac{\alpha}{2}} \right)} \right)\frac{{(\cos\beta)}^{\eta\alpha/(\pi - \alpha)}}{\cos\gamma}
```

reducing the number of parameter to 3: $`A_n=A_n(\lambda)=A_{eq}(0,\lambda)`$  the normal albedo, $`\mu_1 =\mu_1(\lambda)`$  the parameter linked to surface roughness, and $`\eta=\eta(\lambda)`$ which measures the deviation from the pure fractal-like surface.

Note that Shkuratov proposes an expression for the normal albedo if the optical and microphysical properties of the medium is known; i.e. [Shkuratov's particulate model](:/0559731beb16462a8b91418d273b6590).

Note also that the reflectance factor we use in our modeling can be obtained by dividing the previous expression by $cos(i)$ :

```{math}
FR=A(\alpha,\beta,\gamma,\lambda)/\cos(i)=\frac{A(\alpha,\beta,\gamma,\lambda)}{\cos(\beta)\cos(\alpha-\gamma)}
```

## Description of the application input data files

The inversion of the Hapke and Shkuratov photometric models can be done on two types of data:

### Laboratory spectrophotometric data

They are provided as a structured and hierarchical json file. The file includes a root level with the name of the experiment *name_experiment*. The root level includes:

1. an object *geometries*, array of floating reals of dimension `[D, 3]` with `D` the number of geometric configurations. The 1st column of the table includes the values of the angle of incidence, the 2nd the angle of emergence and the 3rd the angle of azimuth (see refxxx)
2. a *wavelengths* object, vector of floating reals of dimension `[nb_wave, 1]` with `nb_wave` the number of wavelengths.
3. one or more *reflectance_xxx objects*, array of floating reals of dimension `[nb_wave, 2, D]` in the case where an uncertainty value (index 2 of the 2nd dimension) exists for each measure of reflectance (index 1 of the 2nd dimension) or `[nb_wave, D]` if only the reflectance value is <!--to be verified-->. Each *reflectance_xxx* object corresponds to the measurement of a xxx sample of the experiment with geometric configurations and wavelengths given by the *geometries* and *wavelengths* object respectively. 

> Note that we use nested array structures to store N-dimensional arrays.
>
> Note that the geometries should also provided in the form of a separated json file the root of which containing 3 vectors of floating reals of dimension `[D]`: *sza* (incidence angle) , *vza* (emergence angle), and *phi* (azimuth angle). 
>
> See the example provided for the Mukundpura experiment containing the measurements of 2 samples: *reflectance powder_block* and *reflectance_block* in addition to geometries and wavelengths.

### Space remote sensing data

They are provided in the form of two cubes in envi format, each consisting of two separate files:

1. a cube of reflectance values *prefix_rho* and associated header *prefix_rho.hdr* 
   The header *prefix_rho.hdr* file of ascii type contains information on the structure of the cube and keywords=value items. We will thus find the keyword wavelength associated with the sequence of wavelengths.
   The *prefix_rho* binary raster file contains a 3 dimensional object `[ nb_pts, D, nb_wave]` with ` nb_pts` the number of points in the scene, `D` the number of geometric configurations and `nb_wave` the number of wavelengths. Each line of the cube corresponds to a point in the scene.
2. a cube of uncertainty values *prefix_drho* and associated header *prefix_drho.hdr*
   It is the homologous cube of *prefix_rho* with the same dimensions `[ nb_pts, D, nb_wave]` that gives the uncertainty value on the reflectance measurement for a given point of the scene, at a given geometry and wavelength. 

In addition the geometries should also provided in the form of a separated json file, the root of which containing 3 arrays of floating reals of dimension [D]: *sza* (incidence angle) , *vza* (emergence angle), and *phi* (azimuth angle). 

## Description of the inversion software

We present a high-performance, documented and open-source software, accessible to the planetary remote sensing community. The software benefits from state-of-the-art development methods and quality standards like systematic testing upon building, continuous integration, distribution across multiple platforms as docker images. 

### Objectives and scope 

The application implements the GLLiM statistical learning technique in its different variants for the inversion of a physical model of reflectance on spectro-(gonio)-photometric data. The latter are of two types: 

- laboratory measurements of reflectance spectra acquired according to different illumination and viewing geometries, 
- and 4D spectro-photometric remote sensing products from multi-angular CRISM or Pléiades acquisitions. 

In both cases the data can be decomposed into a series of vectors of observables Y~obs~  of rank D, that represent in our science case reflectance values for a fixed set of D geometries. From a vector Y~obs~, the objective is the estimation of the mean or the most probable value for each parameter of the inverted physical model (among the L components of the vector X of the parameters) accompanied by a measurement of uncertainty about the estimate. The estimation is performed in four steps:

1. The generation of a learning database (X,Y) using the direct physical model,
2. The gllim learning phase makes it possible to construct a direct then inverse statistical model GLLiM of the functional Y=F(X) which depends on the physical model and on the fixed set of D geometries through the use of the previous database. 
3. This statistical model is then applied to a sequence of Y~obs~ vectors (corresponding to the spectral and possibly spatial dimension of the problem) in order to build a set of a posteriori Probability Distribution Functions (PDFs) $`p_{gllim}(X \vert Y_{obs})`$. 
4. The PDFs are then exploited each independently by different techniques to make estimate the solution $`\hat{X}`$ corresponding to each vector : estimation by the mean, fusion of the components of the Gaussian mixture model into a small number of centroids (usually two or three) to tackles possible multiplicity, importance sampling of the target PDF $`p(X \vert Y_{obs})`$ with the proposition law $`p_{gllim}(X \vert Y_{obs})`$ aroung the mean or around the centroids. 

### Features

#### Generation of a learning database (X,Y) 

The objective of this step is to generate the dictionary $`(X_n, F(X_n)),\ n=1...N`$  of arbitrary size N. We first start by generating randomly a set of parameter vectors X trying to sample as uniformly as possible the entire mathematical parameter space $`[0,1]^L`$. In practice, to avoid  under-sampled area when the dimension of the data is large, we use one of the three following sampling methods: "latin hypercubes", "random usual", and "Sobol". 

1) The "usual" method (Matsumoto and Nishimura, 1998), or also called "random", is based on a pseudo-random generator called Mersenne-Twister. The generator constructs a deterministic sequence from a starting state called "seed" chosen arbitrarily.
2) The sampling method by "Latin hypercubes", proposed by (Mckay et al., 1979), requires an explicit condition on the distribution of the sample. Let N be the number of points to be generated, the idea is to divide the intervals of the components one by one into N sections of width $`\frac{1}{N}`$ . For each component $`k`$, we choose a random number $`x_{k,i}`$ per section (i = 1...N). Once the sample lists are ready, we randomly associate the components $`x_{k,i}`$.
3) "Sobol" (Sobol, 1967), is a method based on Sobol sequences. These sequences belong to the family of quasi-random sequences which are designed to generate samples of multiple parameters in the most uniform way possible over a multidimensional parameter space. The advantage over pseudo-random methods is that the choice of sample values is made by taking into consideration the points of the previous samples, and thus avoiding the appearance of clusters or empty areas.

Secondly we apply the physical model and we add  a centered Gaussian noise $`\mathcal{N}(0,\epsilon)`$: $`Y_n=F(X_n)+\epsilon`$ . The Gaussian noise can be dependent or independent on the data.  In the former case the standard deviation of the Gaussian noise $`\epsilon`$ depends on the data: $`\sigma_{obs}=F(X)/r`$, with r the signal to noise ratio to be specified, while in the latter case the standard deviation is fixed. 

Note that the actual realisation of the learning database $`\left\{x_{n},y_{n}\right\}_{n=1}^{n=N} \in R^{L}`$ can be retrieved by the user for later use (e.g. in machine learning experiments).

#### Initialisation and training of the GLLiM model

The Gaussian Locally Linear Mapping (GLLiM) model is a parametric statistical model closely related to Gaussian mixtures. It can describe the direct and inverse interaction between X and Y by a combination of local affine transformations and is adapted to solving inversion regression problems in a Bayesian framework. We refer the reader to [Kugler et al. 2020 and Deleforge et al., 2014] for a definition and detailed description of the model. 

The local transformation function $`\tau_k`$ from X to Y is given by:

```{math}
Y=\sum_{k=1}^{K} I(Z=k)\left(A_{k} X+b_{k}+E_{k}\right)
```

where $`A_{k} \in R^{D \times L}`$ and $`b_{k} \in R^{D}`$ define the transformation $`\tau_{k}`$, $`Z`$ is a latent variable ($`Z=k`$ if and only if Y is the image of X by the transformation $`\tau_{k}`$), and $`I`$ is the indicator function. $`E_{k} \in R^{D}`$ represents both the noise errors in the observations and the transformation errors.

Under the assumption that $`E_{k}`$ is a centered Gaussian variable with a covariance matrix $`\Sigma_{k} \in R^{D \times D}`$ which does not depend on $`X`$ and $`Y`$, we obtain :

```{math}
p(Y=y \mid X=x, Z=k ; \theta)=\mathcal{N}\left(y ; A_{k} x+b_{k}, \Sigma_{k}\right)
```

In addition the variable $`X`$ is supposed to follow a mixture of $`K`$ Gaussians components which gives:

```{math}
\begin{gathered}
p(X=x \mid Z=k ; \theta)=\mathcal{N}\left(x ; c_{k}, \Gamma_{k}\right) \\
p(Z=k ; \theta)=\pi_{k}
\end{gathered}
```

where $`c_{k} \in R^{L}`$ the centers, $`\Gamma_{k} \in R^{L \times L}`$ the covariance matrices et $`\sum_{k=1}^{K} \pi_{k}=1`$ the component weights.

The model splits the space $`R^{L}`$ into $`k`$ partitions $`R_{k}`$, where $`R_{k}`$ is the region where the transformation $`\tau_{k}`$ is most probable. The GLLiM parameter vector is thus:

```{math}
\theta=\left\{c_{k}, \Gamma_{k}, \pi_{k}, A_{k}, b_{k}, \Sigma_{k}\right\}_{k=1}^{K}
```

##### Learning step

The expectation-maximization algorithm (often abbreviated as EM), proposed by (Dempster et al., 1977), is an iterative algorithm which makes it possible to find the maximum likelihood parameters of a probabilistic model. We will see in this section an adaptation of the algorithm to estimate the parameter vector $`\theta`$ of the GLLiM model.

The EM algorithm is defined as follows:

- Initialisation de $`\theta^{0}`$ (see next section)
- $`i=0`$
- As long as the algorithm has not converged to a local maximum likelihood :
  - Step E: Calculate the expectation of the likelihood taking into account the last observed variables.
  - Step M: Estimate the maximum likelihood of the parameters by maximizing the likelihood found in step E.
- $`i=i+1`$
- end

In the case of the GLLiM model, the EM algorithm is described as follows:

The E step is determined by:

```{math}
r_{n k}:=P\left(Z=k \mid X, Y=x_{n}, y_{n}\right)=\frac{\pi_{k} \mathcal{N}\left(y_{n} ; A_{k} x_{n}+b_{k} \Sigma_{k}\right) \mathcal{N}\left(x_{n} ; c_{k}, \Gamma_{k}\right)}{\sum_{j=1}^{K} \pi_{j} \mathcal{N}\left(y_{n} ; A_{k} x_{n}+b_{k} \Sigma_{j}\right) \mathcal{N}\left(x_{n} ; c_{j}, \Gamma_{j}\right)}
```

the learning database being $`\left\{x_{n},y_{n}\right\}_{n=1}^{n=N} \in R^{L}`$. 

Noting $`r_{k}=\sum_{n=1}^{N} r_{n k}`$, the step M is given by:

```{math}
\begin{aligned}
\pi_{k} &=\frac{r_{k}}{N} \\
c_{k} &=\sum_{n=1}^{N} \frac{r_{n k}}{r_{k}} x_{n} \\
\Gamma_{k} &=\sum_{n=1}^{N} \frac{r_{n k}}{r_{k}}\left(x_{n}-c_{k}\right)\left(x_{n}-c_{k}\right)^{T} \\
A_{k} &=Y_{k} X_{k}\left(X_{k} X_{k}^{T}\right)^{-1} \\
b_{k} &=\sum_{n=1}^{N} \frac{r_{n k}}{r_{k}}\left(y_{n}-A_{k} x_{n}\right) \\
\Sigma_{k} &=\sum_{n=1}^{N} \frac{r_{n k}}{r_{k}}\left(y_{n}-A_{k} x_{n}-b_{k}\right)\left(y_{n}-A_{k} x_{n}-b_{k}\right)^{T}
\end{aligned}
```

where

```{math}
\begin{aligned}
X_{k} &=\frac{1}{\sqrt{r_{k}}}\left[\sqrt{r_{1 k}}\left(x_{1}-\overline{x_{k}}\right) \ldots \sqrt{r_{N k}}\left(x_{N}-\overline{x_{k}}\right)\right], \\
Y_{k} &=\frac{1}{\sqrt{r_{k}}}\left[\sqrt{r_{1 k}}\left(y_{1}-\overline{y_{k}}\right) \ldots \sqrt{r_{N k}}\left(y_{N}-\overline{y_{k}}\right)\right], \\
\overline{x_{k}} &=\sum_{n=1}^{N} \frac{r_{n k}}{r_{k}} x_{n k}, \\
\overline{y_{k}} &=\sum_{n=1}^{N} \frac{r_{n k}}{r_{k}} y_{n k}
\end{aligned}
```

This training algorithm ("GLLiM-EM method") is recommended if the covariance matrices $`\Gamma_{k}\left(\operatorname{resp} \Sigma_{k}\right)`$ are diagonal or isotropic matrices (same variance imposed for all the variables). It is very sensitive to the initialisation of the parameter vector $`\theta`$. So, in the case where no constraint is available, it is advised to use the joint Gaussian Mixtures Model (GMM) model equivalent to the GLLiM model (Deleforge et al., 2014) to carry out the training phase ("GMM-EM method"). This equivalence is very interesting in implementation because it makes it possible to use existing libraries of the EM algorithm on the GMMs. The idea is to transform the GLLiM model into GMM, train on the GMM with the EM algorithm and then return to the GLLiM model. Note that the GMM-EM method is itself initialised by an iterative kmeans algorithm. 

##### Initialisation step

There are two ways to generate an initial proposition for vector $`\theta`$:

> "Fixed" Initialisation strategy : we take uniform $`Z`$ (i.e. $`\left.\pi_{k}=\frac{1}{K}\right), c_{k}`$ uniformly distributed in the parameter space, and $`\Gamma_{k }=\sqrt{\frac{1}{k^{1 / L}}} * I_{L}`$. Then we build a mixture model only on $`X`$ parameterized by the means $`c_{k}`$, the covariance matrices $`\Gamma_{k}`$ and the weights $`\pi_{k}`$. The posterior probability law of the mixture makes it possible to construct the initial vector $`\theta^0`$ of the GLLiM using step M of the GLLiM-EM algorithm.

> "Multiple" Initialisation strategy : In this case, we use the idea of fixed initialization but this time we train the mixture model only on $`X`$ before using its posterior probability law. Once the initial vector $`\theta`$ of the GLLiM has been constructed, a few iterations of the GLLiM-EM algorithm are applied. At the end, we save the vector $`\theta^0`$ obtained and the value of the likelihood reached. This initialization is repeated several times, so we obtain a set of possible initial values of the vector $`\theta^0`$, we choose the one that maximizes the likelihood.

#### Prediction

The prediction phase aims at reversing the physical model on the observations. It is performed for each observation $`y_{o b s}`$ which is accompanied by its measurement error $`\epsilon_{obs}`$ (assimilated to a standard deviation). Before inverting the GLLiM model, we adjust the covariance matrices $`\Sigma_{k}`$ by adding to their diagonal the variance of the measurement error of the observation $`\left(\epsilon_{o b s}^{2 }\right)`$. Once the GLLiM has been reversed, we calculate the expectation which is in the format of a mixture of Gaussians. This mixture is exploited differently depending on the prediction method chosen.

##### Prediction by the mean

The means of the different components in the mixture of Gaussians can all be an estimate of $`x \in F^{-1}\left(y_{o b s}\right)`$. We then consider their barycenter given by:

```{math}
x_{\text {pred }}=\sum_{k=1}^{K} w_{k} * m_{k}
```

The corresponding covariance matrix is given by:

```{math}
\sum_{k=1}^{K} c_{k}+m_{k} * m_{k}^{T} w_{k}-x_{p r e d} x_{p r e d}^{T}
```

where $`w _{k}`$ <!--definition?--> the weight of the k-th component of the mixture and $`m_{k}`$ its mean and $`c_{k}`$ its covariance matrix.

##### Prediction by the centers

In the case where $`y_{o b s}`$ has several antecedents, it is possible that the barycenter in the prediction by the mean is very far from $`F^{-1}\left(y_{o b s}\right)`$. Prediction by centers corrects this problem by returning a set of possible predictions instead of their average. However the size of the mixture can be very large and certainly it contains components carrying little information. The latter are filtered out when their weight is lower than a threshold. Then the method consists in further reducing the size of the mixture by merging the centers following the Kullback-Leibler method <!--to be verified--> and keeping $`M<K`$ the most relevant centers (in our remote sensing science case we take $`M=2`$).

#### Importance sampling

In the Bayesian inversion framework the a posteriori conditional probability density function is $`\phi=p(\mathbf{x} \mid \mathbf{y}) \propto p(\mathbf{y} \mid X=\mathbf{x}) p(\mathbf {x})`$. The Importance Sampling technique is based on the following observation: if $`\psi`$ is another density (nonzero on the support of $`\phi`$ ), then:

```{math}
\mathbb{E}[f(X)]=\int f(\mathbf{x}) \phi(\mathbf{x}) d \mathbf{x}=\int \frac{\phi(\mathbf{x})}{\psi(\mathbf{x})} f(\mathbf{x}) \psi(\mathbf{x}) d \mathbf{x}=\mathbb{E}\left[\frac{\phi\left(X^{\prime}\right)}{\psi\left(X^{\prime}\right)} f\left(X^{\prime}\right)\right]
```

$`X^{\prime} \sim \psi`$. If we know how to sample $`\psi`$, the law of large numbers therefore gives a simple estimator of $`\mathbb{E}[f(X)]:`$ if $`\mathbf{x}_{1}, \ldots, \mathbf{x}_{N}`$ is a sample of the distribution $`\psi`$:

```{math}
\sum_{n=1}^{N} \frac{\phi\left(\mathbf{x}_{n}\right)}{\psi\left(\mathbf{x}_{n}\right)} f\left(\mathbf{x}_{n}\right) \underset{N \rightarrow \infty}{\longrightarrow} \mathbb{E}[f(X)]
```

We can generalize this result even when $`\phi`$ is only known up to a multiplicative constant: $`\phi(\mathbf{x})=c \tilde{\phi}(\mathbf{x})`$

If $`\mathbf{x}_{1}, \ldots, \mathbf{x}_{N}`$ is a sample of the law $`\psi`$, noting $`w_{i}:=\frac{\tilde {\phi}\left(\mathbf{x}_{i}\right)}{\psi\left(\mathbf{x}_{i}\right)}`$, we have:

```{math}
\frac{\sum_{n=1}^{N} w_{i} f\left(\mathbf{x}_{n}\right)}{\sum_{n=1}^{N} w_{i}} \underset{N \rightarrow \infty}{\longrightarrow} \mathbb{E}[f(X)]
```

We thus take as a target law $`\tilde{\phi}(\mathbf{x})=p(\mathbf{y} \mid X=\mathbf{x}) p(\mathbf {x})`$ and the a-posteriori GLLiM law, which is a good approximation of the target law, provides a natural candidate for the proposition law $`\psi(\mathbf{x})=p_{G}(\mathbf{x} \mid Y=\mathbf{y})`$. As $`\psi`$ is a Gaussian mixture law, it is easy to generate a sample $`\left(\mathbf{x}_{1}, \ldots \mathbf{x}_{I}\right)`$ of size $`I`$ following $`\psi`$, and to calculate the estimator of the mean by Importance Sampling (IS), using the law $\operatorname{GLLiM}(\mathrm{G}))$:

```{math}
\overline{\mathbf{x}}_{I S-G}(\mathbf{y}):=\frac{1}{\sum_{i=1}^{I} w_{i}} \sum_{i=1}^{I} w_{i} \mathbf{x}_{i} \quad, \quad \operatorname{with} w_{i}:=\frac{p\left(\mathbf{y} \mid X=\mathbf{x}_{i}\right) p\left(\mathbf{x}_{i}\right)}{p_{G}\left(\mathbf{x}_{i} \mid Y=\mathbf{y}\right)}
```

The associated variance is given by the following expression:

```{math}
\operatorname{var}(\mathbf{y}):=\frac{1}{\sum_{i=1}^{I} w_{i}} \sum_{i=1}^{I} w_{i}\left(\mathbf{x}_{i}-\mu\right)^{\mathrm{t}}\left(\mathbf{x}_{i}-\mu\right)
```

The parameter $`I`$ is therefore the number of samples generated for the importance sampling of the target PDF according to the proposition law that is, in the present version of the software, a "Gaussian" mixture model. In practice the choice of $`I`$ shall ensure a compromise between the mathematical precision and the computation time. The duration of this importance sampling step indeed dominates the rest of the procedure, because, for each observation $`y_{obs}`$, the calculation of the weights $`w_{i}`$ requires evaluating $`I`$ times the functional F and the mixing law GLLiM. In comparison, a simple prediction by the GLLiM mean requires only averaging and no evaluation of F.

The importance Sampling (IS) technique is operated in a very similar manner to refine the prediction of the GLLiM centers by calculating an estimator of the modes of the a posteriori conditional probability density function $`\phi`$. The distance between the mode and the center $`\boldsymbol{c}_{k}^{m}`$ of the prior must be relatively small.

The estimator of the centroid $`k`$ by importance sampling $`\overline{\mathbf{x}}_{I S \text {-centroid }, k}`$ is therefore obtained by generating $I$ samples $`\left(\mathbf{x} _{1}^{k}, \ldots, \mathbf{x}_{I}^{k}\right)`$ according to the law $`\mathcal{N}\left(\boldsymbol{c}_{k} ^{m}, \boldsymbol{\Gamma}_{k}^{m}\right)`$ and calculating:

```{math}
\overline{\mathbf{x}}_{I S-\text { centroid }, k}(\mathbf{y}):=\frac{1}{\sum_{i=1}^{I} w_{i}^{(k)}} \sum_{i=1}^{I} w_{i}^{(k)} \mathbf{x}_{i}^{k} \quad, \quad \operatorname{avec} w_{i}^{(k)}:=p\left(\mathbf{y} \mid X=\mathbf{x}_{i}^{k}\right) p\left(\mathbf{x}_{i}^{k}\right)
```

The associated variance is:

```{math}
\operatorname{var}(\mathbf{y}):=\frac{1}{\sum_{i=1}^{I} w_{i}^{(k)}} \sum_{i=1}^{I} w_{i}^{(k)}\left(\mathbf{x}_{i}^{(k)}-\mu\right)^{\mathrm{t}}\left(\mathbf{x}_{i}^{(k)}-\mu\right)
```

where $`\mu:=\overline{\mathbf{x}}_{I S-\text { centroid, } k}(\mathbf{y})`$.

#### Graphical interface

The graphical interface allows the user to configure, save, load, and run an experiment. In addition it provides a quick look on the results that can be saved in their complete form locally in a zip archive. The graphical interface is organized by sections according to the sequence followed by the GLLiM-IS method (see section [Parameters](#Parameters)). In the different sections, several choices are available to the user and the panel adapts accordingly. A default configuration is provided for the different parameters of the experiment. Once the configuration is completed, the user validates the latter and the associated input data files by pressing the save configuration button. Saving the configuration also allows the user to run the simulation. A follow-up of the execution through the different steps is shown while a detailed log is also displayed and updated every 5 seconds. During execution, the user has the possibility to interrupt the simulation. Once the simulation is finished, the user can download the result files, the execution log, and the learning database (zip archive).

#### Description of logging

errors

performance

memory usage

execution time

metric

### Parameters

<u>Configuration of the [physical model](#Detail-of-the-photometric-models)</u>

| Parameter      | Description                                                  | Type                  | Default value |
| -------------- | ------------------------------------------------------------ | --------------------- | ------------- |
| Physical model | Identifier of the physical model among "Hapke model" or "Shkuratov model" | Tag in drop down menu | "Hapke model" |
|                |                                                              |                       |               |

[Hapke model](#The-Hapke-photometric-model)

| Parameter                                  | Description                                                  | Type               | Default value |
| ------------------------------------------ | ------------------------------------------------------------ | ------------------ | ------------- |
| Version of the Hapke model                 | Version of the Hapke model                                   | single choice list | 2002          |
| Variant of the Hapke model                 | Choice of the free parameters: "Full"-> (w,b,c,$`θ_H`$,B0,h); "Reduced"-> (w,b,c,$`θ_H`$); "Hockey stick"-> (w,$`θ_H`$) with (b,c) related by the Hockey stick empirical formula | single choice list | "Reduced"     |
| Maximal value of theta_bar                 | Value used to normalise the $`θ_H`$ parameter into the mathematical parameter space [0,1] | Float              | 30            |
| B0 : Magnitude of the opposition effect    | If variant of the Hapke model is "reduced" or "Hockey stick", a fixed value is to be provided for parameter B0 | Float              | 0             |
| H : Angular width of the opposition effect | If variant of the Hapke model is "reduced" or "Hockey stick", a fixed value is to be provided for parameter h | Float              | 0.1           |
| Geometries file                            | file containing 3 vectors of floating reals of dimension `[D]`: *sza* (incidence angle) , *vza* (emergence angle), and *phi* (azimuth angle). | json               | -             |
|                                            |                                                              |                    |               |

[Shkuratov model](#The-Shkuratov-photometric-model)

| Parameter            | Description                                                  | Type  | Default value |
| -------------------- | ------------------------------------------------------------ | ----- | ------------- |
| Maximal value of An  | Value used to normalise the An parameter into the mathematical parameter space [0,1] | Float | 1.0           |
| Minimal value of An  | Value used to normalise the An parameter into the mathematical parameter space [0,1] | Float | 0.            |
| Maximal value of mu1 | Value used to normalise the mu1 parameter into the mathematical parameter space [0,1] | Float | 1.5           |
| Minimal value of mu1 | Value used to normalise the mu1 parameter into the mathematical parameter space [0,1] | Float | 0.            |
| Maximal value of nu  | Value used to normalise the mu1 parameter into the mathematical parameter space [0,1] | Float | 1.0           |
| Minimal value of nu  | Value used to normalise the mu1 parameter into the mathematical parameter space [0,1] | Float | 0.2           |
| Maximal value of m0  | Value used to normalise the m0 parameter into the mathematical parameter space [0,1] | Float | 1.5           |
| Minimal value of m0  | Value used to normalise the m0 parameter into the mathematical parameter space [0,1] | Float | 0.            |
| Maximal value of mu2 | Value used to normalise the mu2 parameter into the mathematical parameter space [0,1] | Float | 1.5           |
| Minimal value of mu2 | Value used to normalise the mu2 parameter into the mathematical parameter space [0,1] | Float | 0.            |
| Geometries file      | file containing 3 vectors of floating reals of dimension `[D]`: *sza* (incidence angle) , *vza* (emergence angle), and *phi* (azimuth angle). | json  | -             |
|                      |                                                              |       |               |

<u>Configuration of the data generation module</u>

| Parameter             | Description                                                  | Type                  | Default value               |
| --------------------- | ------------------------------------------------------------ | --------------------- | --------------------------- |
| Data Generation model | Type of Gaussian statistical model to generate the learning dataset  $`F(X)+\epsilon`$, $`\epsilon`$ being a centered Gaussian noise: "basic" or "dependent" | Tag in drop down menu | "Basic gaussian stat model" |
| Dataset size          | Number of (X,Y) pairs to be generated in the learning dictionary. | Integer               | 10000                       |
| Generator type        | The random generator produces X vectors in $`[0,1]^L`$. Its type can be "latin hypercube", "random", or "sobol". | single choice list    | "sobol"                     |
| Seed of the generator | The seed used by the random generator                        | Integer               | System time                 |
| Variances             | In case of a "Basic gaussian stat model": the isometric fixed variance of the Gaussian noise $`\epsilon`$ is given in relative unit (%) | Float                 | 0.01%                       |
| Noise effect          | In case of a "Dependent gaussian stat model": the standard deviation of the Gaussian noise $`\epsilon`$ depends on the data: $`\sigma_{obs}=F(X)/r`$, with r the signal to noise ratio to be specified. | Integer               | 20                          |
|                       |                                                              |                       |                             |

<u>Configuration of the GLLiM model</u> 

| Parameter                          | Description                                                  | Type                  | Default value               |
| ---------------------------------- | ------------------------------------------------------------ | --------------------- | --------------------------- |
| Number of affine functions         | Number of affine transformations in the GLLiM model          | Integer               | 50                          |
| Type of Gamma matrices             | Type of covariance matrix for the K GLLiM components: "Full matrices", "Diagonal matrices", or "isomorphic" matrices. | single choice list    | "Full matrices"             |
| Type of Sigma matrices             | Type of covariance matrix for the Gaussian noise applied to each affine transformation: "Full matrices", "Diagonal matrices", or "isomorphic" matrices | single choice list    | "Diagonal matrices"         |
| Floor                              | Minimum threshold for the covariance values (Low covariances are regularized to insure numerical stability) | Float                 | 1.e-8                       |
| Initialisation step                | Initialisation strategy applied to the GLLiM learning "Fixed" or "Multiple" | Tag in drop down menu | "Multiple "Initialisations" |
| Configuration of the learning step | Learning step strategy: "GLLiM-EM method" or "GMM-EM method" | Tag in drop down menu | "GLLiM-EM method"           |
|                                    |                                                              |                       |                             |

Multiple initialisation configuration

| Parameter                  | Description                                                  | Type    | Default value |
| -------------------------- | ------------------------------------------------------------ | ------- | ------------- |
| Number of initialisations  | Number of initialization experiments                         | Integer | 10            |
| Seed of the generator      | The seed used by random generators                           | Integer | System time   |
| K-means iterations         | Number of iterations of the k-means algorithm to initialize the clusters | Integer | 5             |
| GMM-EM iterations          | Number of iterations of the GMM-EM algorithm after initialisation by the K-means. | Integer | 10            |
| Iterations of the GLLiM-EM | Number of iterations for the GLLiM-EM algorithm              | Integer | 5             |
|                            |                                                              |         |               |

Fixed initialisation configuration

| Parameter             | Description                                                  | Type    | Default value |
| --------------------- | ------------------------------------------------------------ | ------- | ------------- |
| Seed of the generator | The seed used by random generator                            | Integer | System time   |
| K-means iterations    | Number of iterations of the k-means algorithm to initialize the clusters | Integer | 5             |
| GMM-EM iterations     | Number of iterations of the GMM-EM algorithm after the initialization of the K-means. | Integer | 10            |
|                       |                                                              |         |               |

Configuration of the GLLiM Expectation-Maximisation "GLLiM-EM method"

| Parameter           | Description                                                  | Type    | Default value |
| ------------------- | ------------------------------------------------------------ | ------- | ------------- |
| GLLiM-EM iterations | Number of iterations for the GLLiM-EM algorithm              | Integer | 100           |
| likelihood increase | The GLLiM-EM algorithm stops if the gain in log-likelihood between two iterations is lower than this parameter | Float   | 0.00001       |
|                     |                                                              |         |               |

Configuration of the training strategy by the joint GMM model "GMM-EM  method"

| Parameter          | Description                                                  | Type    | Default value |
| ------------------ | ------------------------------------------------------------ | ------- | ------------- |
| K-means iterations | Number of iterations of the k-means algorithm to initialize the clusters | Integer | 10            |
| GMM-EM iterations  | Number of iterations of the GMM-EM algorithm after initialisation by the K-means. | Integer | 100           |
|                    |                                                              |         |               |

<u>Configuration of the prediction module</u> 

| Parameter                                         | Description                                                  | Type    | Default value |
| ------------------------------------------------- | ------------------------------------------------------------ | ------- | ------------- |
| Number of predictions : Prediction by the center  | Final number of components $M$ to retain after merging the initial K component Gaussian mixture. The prediction is performed for each components (centroids). | Integer | 2             |
| Size of the reduced GMM : Prediction by the means | Final number of components $M$ to retain after merging the initial K component Gaussian mixture. The prediction is the mean of the reduced mixtures. | Integer | 2             |
| Minimum weight a GMM component                    | Any initial component which weight is lower than this threshold is discarded before the merging. | Float   | 1e-10         |
|                                                   |                                                              |         |               |

<u>Configuration of the importance sampling module</u>

| Parameter                                   | Description                                                  | Type                  | Default value |
| ------------------------------------------- | ------------------------------------------------------------ | --------------------- | ------------- |
| Number of samples                           | Number of samples generated for the importance sampling of the target PDF | Integer               | 1000          |
| Proposition law : Prediction by the mean    | Type of proposition law used to estimate the mean of the posterior PDF. Currently, only the Gaussian mixture model is offered "gaussian". | Tag in drop down menu | "gaussian"    |
| Proposition law : Prediction by the centers | Type of proposition law used to estimate the modes of the posterior PDF. Currently, only the Gaussian mixture model is offered "gaussian". | Tag in drop down menu | "gaussian"    |
|                                             |                                                              |                       |               |

<u>Configuration of the observations</u> 

| Parameter           | Description                                                  | Type                  | Default value    |
| ------------------- | ------------------------------------------------------------ | --------------------- | ---------------- |
| Observations source | types of data to be analysed: from ["Laboratory"](#Laboratory-spectrophotometric-data) or ["Remote sensing"](#Space-remote-sensing-data) | Tag in drop down menu | "Remote sensing" |
| Observations file   | json file or spectral image cubes depending on the data type. | json or envi          | -                |
|                     |                                                              |                       |                  |





### Summary table of problem dimensions

The application works with a number of dimensions that will be used throughout this document. Here is the list and their details.

| Nom        | Description                                                  | Source                                                       |
| ---------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| nb_geom=D  | Number of available geometries                               | object *geometries* in the json file or cube prefix_geo (plan 3,4 et 5) |
| nb_param=L | Number of parameters of the physical model                   | Choice of the physical model (Hapke, Shkuratov, ...)         |
| nb_wave    | Number of wavelengths                                        | object *wavelengths* in the json file or label 'wavelengths' in  prefix_rho.hdr |
| nb_sample  | Number of sample in a lab experiment                         | Number of *reflectance_xxx* objects in the input json file   |
| nb_pts     | Number of spatial points in a planetary scene                | first dimension of the cubes *prefix_rho* or *prefix_drho*   |
| Nobs       | Number of Y vectors on which the physical model is inverted. | Experimental data: Nobs=nb_wave\*nb_sample  <br />Remote sensing data: Nobs=nb_wave\*nb_pts. |

## Description of application output data files

The content and format of the results of the inversion of the physical model on the input data depends on the nature of the latter.

#### Laboratory spectrophotometric data

The results are provided as structured and hierarchical json files. There is one file for each type of estimation:

- *name_experiment_is_center_1*: importance sampling of the posterior probability distribution function (pdf) around the first centroid. 
- *name_experiment_is_center_2*: importance sampling of the posterior probability distribution function (pdf) around the second centroid. 
- *name_experiment_is_mean*: importance sampling of the mean of the posterior probability distribution function (pdf). 
- *name_experiment_pred_center_1*: prediction of the first centroid according to the Gllim model.
- *name_experiment_pred_center_2*: prediction of the second centroid according to the Gllim model.
- *name_experiment_pred_mean*: prediction of the mean of the Gllim Gaussian mixture model.

The latter stands in the case the user chooses a final mixture of 2-components for the Gllim model. If the mixture contains more than 2 components the number of files will change accordingly (two files *name_experiment_is_center_i* and *name_experiment_pred_center_i* for each center i). <!--To be verified-->. Each file includes a root level with the name of the experiment *name_experiment*. The root level includes:

1. a *wavelengths* object, vector of floating reals of dimension `[nb_wave, 1]` with `nb_wave` the number of wavelengths.

2. a series of sub-levels *reflectance_xxx* corresponding to the various xxx samples of the experiment. Each sub-level xxx contains:
   1. ESTIM_X object, array of floating reals of dimension `[nb_wave, L]` that contains the estimation of the L physical parameters normalised between [0,1] for each wavelength.
   2. ESTIM_X_PHYSICAL_UNITS object, array of floating reals of dimension `[nb_wave, L]` that contains the estimation of the L physical parameters for each wavelength.
   3. STD_X object, array of floating reals of dimension `[nb_wave, L]` that contains the uncertainty on the L physical parameters normalised between [0,1] for each wavelength.
   4. STD_X_PHYSICAL_UNITS object, array of floating reals of dimension `[nb_wave, L]` that contains the uncertainty on the L physical parameters for each wavelength.
   5. ERR_Y object, vector of floating reals of dimension `[nb_wave]` that contains the root mean square error between Y~obs~ and F(X) over the D geometrical configurations.


There are 3 additional files:

- name_experiment_MULT,
- name_experiment_REGU_CENTERS_IS,
- name_experiment_REGU_CENTERS.

<!--Find the description-->

#### Space remote sensing data

The results are provided as envi cubes. There are tree cubes for each type of estimation:

1. *prefix_ESTIM_X* cube, array of floating reals of dimension `[nb_pts,L,nb_wave]` that contains the estimation of the L physical parameters (sample dimension) normalised between [0,1] for `nb_pts` spatial points (line dimension) and for each wavelength (band dimension).
2. *prefix_STD_X* cube, array of floating reals of dimension `[nb_pts,L,nb_wave]` that contains the uncertainty on the L physical parameters normalised between [0,1] for `nb_pts` spatial points and for each wavelength.
3. *prefix_ERR_Y* cube, vector of floating reals of dimension `[1,nb_pts,nb_wave]` that contains the root mean square error between Y~obs~ and F(X) over the D geometrical configurations for `nb_pts` spatial points as a function of wavelength.

<!--We should rather output the unnormalised physical parameters-->

There are six types of estimations identified by a *prefix* :

- *is_center_1*: importance sampling of the posterior probability distribution function (pdf) around the first centroid. 
- *is_center_2*: importance sampling of the posterior probability distribution function (pdf) around the second centroid. 
- *is_mean*: importance sampling of the mean of the posterior probability distribution function (pdf). 
- *pred_center_1*: prediction of the first centroid according to the Gllim model.
- *pred_center_2*: prediction of the second centroid according to the Gllim model.
- *pred_mean*: prediction of the mean of the Gllim Gaussian mixture model.

In the case the user chooses a final mixture of 2-components for the Gllim model, 18=6\*3 cubes are generated by the software plus one MULT cube. If the mixture contains more than 2 components the number of files will change accordingly 

