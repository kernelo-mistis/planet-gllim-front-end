Installation
============

PlanetGLLiM is 


.. contents::
   :depth: 2

Development on your machine
---------------------------

Prerequisite
~~~~~~~~~~~~

- list of all the dependencies

ToDO
~~~~~~~~~~~~

This is the content of section 1.


Development within Docker container with Dev Containers VS code extension
-------------------------------------------------------------------------

If your IDE is Visual Studio Code, you might be interested in the Dev Containers extension. Indeed 

Prerequisite
~~~~~~~~~~~~

- `Docker Engine <https://docs.docker.com/engine/install/>`__


Build Planetgllim Dockers
=========================

To build Planetgllim Dockers, perform the following steps:

1. Change directory to the Planet-Gllim Front-end project:

   .. code-block:: shell

      cd $planet-gllim-front-end

2. Build the `kernelo_python_runner` Docker image, providing the `kernelo_runner` image as a build argument:

   .. code-block:: shell

      docker build -f docker/runner.Dockerfile --build-arg image=kernelo_runner -t kernelo_python_runner .

Development in Container with Dev Container VS Code Extension (planetgllim_devcontainer)
---------------------------------------------------------------------------------------

To develop inside a Docker container with minimal tools and configuration using the VS Code Dev Container extension (planetgllim_devcontainer), follow these steps:

1. Install the Dev Container extension in VS Code.

2. Refer to the documentation at: [link to documentation](....com)

3. All configuration files are located in the `.devcontainer/` folder.

4. Click "Dev Container" and select "Reopen in container."

5. A new container is built, allowing you to develop with VS Code inside a Docker container.

6. To switch back to developing locally, click "Dev Container" and select "Reopen folder locally."

Note: If you rebuild the Docker image, you will need to reinstall any necessary dependencies.

Run App with Docker Compose
===========================

You can also run the under development app using Docker Compose. To run the app using Docker Compose, execute the following commands:

1. Build the `planetgllimapp` Docker image, providing the `kernelo_python_runner` image as a build argument:

   .. code-block:: shell

      docker build -f docker/app.Dockerfile --build-arg image=kernelo_python_runner -t planetgllimapp .

2. Use Docker Compose to start the application:

   .. code-block:: shell

      docker compose -f build/docker-compose-dev.yml -p dev up

3. To stop the application, press `Ctrl+C`.

