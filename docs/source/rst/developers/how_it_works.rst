How PlanetGLLiM works ?
=======================

Global scheme
-------------

.. image:: ../../_static/global.png
   :width: 400
   :height: 300
   :alt: Global scheme


Docker containers
-----------------

.. image:: ../../_static/containers_scheme.png
   :width: 400
   :height: 300
   :alt: Docker containers


Algorithmic workflow
--------------------

.. image:: ../../_static/workflow.png
   :width: 400
   :height: 300
   :alt: Algorithmic workflow

