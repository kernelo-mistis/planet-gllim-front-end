Getting Started
===============

Tutorial
--------

.. raw:: html

   <video width="640" height="480" controls>
        <source src="../../../_static/planetgllim_tuto_v1.mp4" type="video/mp4">
        Your browser does not support the video tag.
   </video>

Admin interface
---------------

.. image:: ../../../_static/admin_interface.png

PlanetGLLiM take advantage of the Django automatic admin interface. You can visualize and manage content of your simulations (Simulations, Laboratory data, Remote sensing data, Custom models).
The page is accessible at http://0.0.0.0:8080/admin/ where you should enter defautl login credentials:
 - username: *admin*
 - password: *admin*


Try it yourself !
-----------------

In order to get started with the application, input data are proposed in order to run your own simulation. You can download the input data, run a simulation as described in the tutorial video and check the results.
There is data for both laboratory observations and space remote sensing observations.

.. grid:: 1 2 auto auto
    :gutter: 2

    .. grid-item-card:: :fas:`microscope` Laboratory data
        :link: laboratory_example
        :link-type: ref

    .. grid-item-card:: :fas:`satellite-dish` Space remote sensing data
        :link: remote_sensing_example
        :link-type: ref

.. toctree::
    :hidden:

    laboratory_example
    remote_sensing_example
