.. _laboratory_example:

Test data from the Laboratory
=============================

Here is proposed opensource dataset corresponding to laboratory observations.


Description
~~~~~~~~~~~

The zip file includes all the lab measurements gathered in one file named *JRSC1_BRDF_planetgllim.json*. 
It also includes the file named geometries_JSC1.json containing the description of the geometries corresponding to the observations. 
This geometries file is required for the simulation.

*Schmitt, Bernard; Bollard, Philippe; Albert, Damien; Garenne, Alexandre; Gorbacheva, Maria; Bonal, Lydie; Volcke, Pierre, and the SSHADE partner's consortium (2018).
SSHADE: "Solid Spectroscopy Hosting Architecture of Databases and Expertise" and its databases. OSUG Data Center. Service/Database Infrastructure.* 
`doi:10.26302/SSHADE <https://doi.org/10.26302/SSHADE>`__


Get the observations data
~~~~~~~~~~~~~~~~~~~~~~~~~

.. raw:: html

    <a href="../../../_static/JSC1_data.zip" class="btn btn-secondary btn-md btn-block">
        <i class="fa fa-download"></i> Download
    </a>


Analysis
~~~~~~~~

Once the simulation is complete, you should see two "quick look" graphs :

    - **Relative reconstruction error reported for each wavelength** 
        for each estimation methods (Prediction by the mean, prediction by each centroid, IMIS by the mean and IMIS by each centroids). IMIS corresponds to a smart sampling method (more details in scientific documentation) of the predictions and should be better than the prediction estimation. 
        *x_best* corresponds to a combination of estimation methods minimizing error on each wavelengths.

    - **Estimation of parameter values reported for each wavelength** 
        for 2 methods : IMIS by the mean (*imis_mean*) and best on error (*x_best*).
        Usually *imis_mean* has a better continuity than *x_best*.

For further analysis, all the results data for each methods are gathered in an zip file and available for download by clicking the corresponding button.

.. image:: ../../../_static/JSC1_reconstruction_error.png
    :alt: Relative reconstruction error

.. image:: ../../../_static/JSC1_param_imis_mean.png
    :alt: Parameters estimation

Here the reconstruction error is relatively low - less than 10% for the *imis_mean* method.
This simulation has been run with the 4-parameters Hapke physical model. The second graph shows three of the four Hapke parameters estimated with the *imis_mean* method :
    - X_0 corresponding to the Single scattering albedo :math:`\boldsymbol{\omega}`,
    - X_2 corresponding to the Anisotropy parameter **b**,
    - X_3 corresponding to the Backward or forward scattering coefficient **c**.
