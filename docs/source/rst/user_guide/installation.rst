Installation
============

PlanetGLLiM is provided through a Docker image and runs on your local machine with the Docker Compose tool.

====

Prerequisite
------------

You need to install:

- `Python 3 <https://www.python.org/downloads/>`__
- `RabbitMQ <https://www.rabbitmq.com/download.html>`__
- `PostgreSQL <https://www.postgresql.org/download/>`__
- `Docker Engine <https://docs.docker.com/engine/install/>`__
- `Docker Compose <https://docs.docker.com/compose/install/>`__

====

Download the app
----------------

- Log into the repository:

.. code-block:: shell

    docker login registry.gitlab.inria.fr

- Clone the repository:

.. code-block:: shell

    git clone https://gitlab.inria.fr/kernelo-mistis/planet-gllim-front-end.git

- Move to the build directory:

.. code-block:: shell

    cd planet-gllim-front-end/build

- Pull Docker required images:

.. code-block:: shell

    docker compose pull


Run the app
------------

- Run the software:

.. code-block:: shell

    docker compose up

Then wait for this message in the console:

.. code-block:: shell

    web_1     | Starting development server at http://0.0.0.0:8080/
    web_1     | Quit the server with CONTROL-C.

- Open a web browser and go to http://0.0.0.0:8080

- To shut down the application, run:

.. code-block:: shell

    docker compose down

- To shut down the application and remove previously computed data, run:

.. code-block:: shell

    docker compose down --volumes


====

Troubleshooting
---------------

* **There is no output in the console of the application**

This appears to happen sometimes. Just let the simulation run and watch the output in your terminal. Once finished, just reload the application page in the browser and rerun the simulation. The saved results will be called.

* **My simulation crashes, raising an EOFERROR**

Be sure your Docker app has enough memory allocated. Set the amount of RAM available for Docker to 8GB at least.

* **docker-compose up crashes, raising an error like ERROR: for broker  Cannot start service broker**

You likely have a broker running on your system (like RabbitMQ) and the broker port is busy. Be sure the port 5672 is free.
