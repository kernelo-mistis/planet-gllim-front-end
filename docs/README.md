This file present how to contribute to the Planet-GLLiM's documentation.

## Prerequisite

* **Sphinx** : https://www.sphinx-doc.org
> Be familiar with Markup languages, espacially reStructuredText (.rst) wich is Sphinx default language.
[Here](https://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html) is a user guide for basics is rst language. There is also a complete example file is reStructuredText language is the project located at <span style="color:rgb(150,100,0)">docs/source/dev_example_templates/example_rst_file.rst</span>

## Installation

* Clone the *documentation* branch
```
git clone --single-branch --branch documentation git@gitlab.inria.fr:kernelo-mistis/planet-gllim-front-end.git
```
* Move into the project directory
```
cd planet-gllim-front-end
```
* Install required dependencies
```
pip install -r docs/requirements.txt
```

## Workflow

**Set up your own branch**

* Make sure your on *documentation* branch
```
git checkout documentation
```
* Pull the remote branch
```
git pull
```
* Create your own branch - i.e. *modif_title_size*
```
git checkout -b <my-documentation-branch> 
```
* Move to the *documentation* directory
```
cd docs
```

**Make your modification**

Add, modify, delete rst or md files. Do not edit other files !

* Compile with Sphinx
```
make html
```
* Checkout result in browser by opening <span style="color:rgb(150,100,0)">build/html/index.html</span> in your browser,
For instance, if you use Firefox, you can enter:
```
firefox build/html/index.html
```
* Do not forget to commit frequently
```
git commit -m “update file hapke_model.rst”
```
* Push your commits to the remote branch
```
git push
```

**Create Merge Resquest on Gitlab**

* Go on Gitlab repo (https://gitlab.inria.fr/kernelo-mistis/planet-gllim-front-end)
* In section "Merge requests" click button <span style="background-color:rgb(0,100,200)">**New merge request**</span>
* Select your branch as "Source branch" and *documentation* as "Target branch"
![MR: select branches screenshot](source/_static/merge_request.png)
* Click <span style="background-color:rgb(0,100,200)">**Compare branches and continue**</span>
* Double check your modification and click <span style="background-color:rgb(0,100,200)">**Create merge request**</span>



Once the merge request is approved and merged into master, your modification will take effect on the official documentation at 
> https://kernelo-mistis.gitlabpages.inria.fr/planet-gllim-front-end/.
