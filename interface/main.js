window.$ = window.jQuery = require('jquery');
require('bootstrap-sass');

import Vue from 'vue';
import App from "./App.vue";
import router from './router';
import VueResource from 'vue-resource';
Vue.use(VueResource);

import VueCookies from 'vue-cookies';
Vue.use(VueCookies);

import VueMathjax from 'vue-mathjax'
Vue.use(VueMathjax)


// Import Bootstrap Lumen theme
import './style/bootstrap.css';


Vue.prototype.dimension_D = '';
Vue.prototype.K = '';

import Chart from 'chart.js';

import VueCarousel from 'vue-carousel';
Vue.use(VueCarousel);

window.Vue = Vue;
const app = new Vue({
    el: '#app',
    router,
    components: {
        App,
    }
});