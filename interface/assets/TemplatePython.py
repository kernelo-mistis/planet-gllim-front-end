# IMPORTS
import numpy as np

class PhysicalModel(object):
    """ This is a python class defining the Shkuratov physical model. 
    
    This class is composed of 5 mandatory functions:
        - F: the functional model F describing the physical model. F takes photometries as arguments and return reflectances
        - get_D_dimension: returns the dimension of Y (reflectances)
        - get_L_dimension: return de dimension of X (photometries)
        - to_physic: converts the X data from mathematical framework (0<X<1) to physical framework
        - from_physic: converts the X data from physical framework to mathematical framework (0<X<1)

    Note that some class constants, other functions and class constructors can be declared.

    Geometries : if your physical model requires geometries as with Shkuratov, the structure and the values of the geometries
         must be declared within this Python file.

    See the Planet-Gllim documentation for more informations (https://gitlab.inria.fr/kernelo-mistis/planet-gllim-front-end/-/wikis/home)
    """
    
    #################################################################################################
    ##                          CLASS CONSTANTS (OPTIONAL)                                         ##
    #################################################################################################

    # Geometries of the physical model MUST be declared here - directly in the python file. 


    #################################################################################################
    ##                          CORE FUNCTIONS (MANDATORY)                                         ##
    #################################################################################################

    def F(self):
        pass

    def get_D_dimension(self):
        pass

    def get_L_dimension(self):
        pass

    def to_physic(self):
        pass

    def from_physic(self):
        pass


    #################################################################################################
    ##                          OTHER FUNCTIONS (OPTIONAL)                                         ##
    #################################################################################################

    def __init__(self, *args, **kwargs):
        pass

    def optional_but_useful_function():
        pass
