import numpy as np

class ShkuratovModel5p(object):
    """ This is a python class defining the Shkuratov physical model. 
    
    This class is composed of 5 mandatory functions:
        - F: the functional model F describing the physical model. F takes photometries as arguments and return reflectances
        - get_D_dimension: returns the dimension of Y (reflectances)
        - get_L_dimension: return de dimension of X (photometries)
        - to_physic: converts the X data from mathematical framework (0<X<1) to physical framework
        - from_physic: converts the X data from physical framework to mathematical framework (0<X<1)

    Note that some class constants, other functions and class constructors can be declared.
    """
    
    #################################################################################################
    ##                          CLASS CONSTANTS (OPTIONAL)                                         ##
    #################################################################################################

    DEGREE_180 = 180
    L_dimension = 5
    scalingCoeffs = [1.0,1.5,1.5,1.5,1.5]
    offset = [0,0,0.2,0,0]

    # geometry
    ALPHA = 0
    BETA = 1
    GAMMA = 2
    INC = 0 # sza
    EME = 1 # vza
    PHI = 2 # phi

    # photometry
    AN = 0
    MU_1 = 1
    NU = 2
    M = 3
    MU_2 = 4

    """ Geometries should be shaped as an array matrix with 3 raws (angles) and N columns (nb of geometrical configuration), like this:
    geometries =    [   
                        [inc0, inc1, inc2, inc3, inc4, inc5, inc6, ...],
                        [eme0, eme1, eme2, eme3, eme4, eme5, eme6, ...],
                        [phi0, phi1, phi2, phi3, phi4, phi5, phi6, ...] 
                    ]
    where 
        - 'inc' corresponds to the incident beam angle or solar zenith angle (sza)
        - 'eme' corresponds to the emergent beam angle or visual zenith angle (vza)
        - 'phi' corresponds to the azimuth angle
    """
    # geometries data
    geometries =  [] # [TO COMPLETE (copy-paste your geometries)]
    geometries = np.array(geometries)

 
    #################################################################################################
    ##                          CORE FUNCTIONS (MANDATORY)                                         ##
    #################################################################################################

    def F(self, photometry):
        photometry = self.to_physic(photometry)

        f = (np.exp(- photometry[self.MU_1] * self.configuredGeometries[self.ALPHA]) + photometry[self.M] * np.exp(- photometry[self.MU_2] * self.configuredGeometries[self.ALPHA])) / (1 + photometry[self.M])
        d = np.cos(self.configuredGeometries[self.ALPHA] / 2.0) * np.cos(np.pi * (self.configuredGeometries[self.GAMMA] - self.configuredGeometries[self.ALPHA] / 2.0) / (np.pi - self.configuredGeometries[self.ALPHA])) / np.cos(self.configuredGeometries[self.GAMMA])
        o=d*np.cos(self.configuredGeometries[self.BETA])**(photometry[self.NU] * self.configuredGeometries[self.ALPHA] * (np.pi - self.configuredGeometries[self.ALPHA]))
        reflectances = photometry[self.AN] * o.T * f.T / self.cos_i.T

        return reflectances


    def get_D_dimension(self):
        return self.D_dimension

    def get_L_dimension(self):
        return self.L_dimension

    def to_physic(self, x):
        for l in range(x.shape[0]):
            x[l] = x[l] * self.scalingCoeffs[l] + self.offset[l]
        return x

    def from_physic(self, x):
        for l in range(x.shape[0]):
            x[l] = (x[l] - self.offset[l]) / self.scalingCoeffs[l]
        return x


    #################################################################################################
    ##                          OTHER FUNCTIONS (OPTIONAL)                                         ##
    #################################################################################################

    def __init__(self):
        self.D_dimension = self.geometries.shape[1]
        self.configuredGeometries = self.setupGeometries(self.geometries)
        self.cos_i = np.cos(self.geometries[self.INC]*np.pi / self.DEGREE_180)

    def degToGrad(self, degree):
        return degree * np.pi / self.DEGREE_180


    def setupGeometries(self, geometries):
        configuredGeometries = np.zeros(geometries.shape)
        geomsGrad = np.array(geometries)
        geomsGrad = self.degToGrad(geomsGrad)

        #compute Alpha
        configuredGeometries[self.ALPHA] = np.arccos(np.cos(geomsGrad[self.INC]) * np.cos(geomsGrad[self.EME]) + np.sin(geomsGrad[self.INC]) * np.sin(geomsGrad[self.EME]) * np.cos(geomsGrad[self.PHI]))

        #compute Beta
        sin_i_e_2 = pow(np.sin(geomsGrad[self.INC] + geomsGrad[self.EME]),2)
        cos_phiDiv2_2 = pow(np.cos(geomsGrad[self.PHI]/2.0),2)
        sin_2_i = np.sin(geomsGrad[self.INC] * 2)
        sin_2_e = np.sin(geomsGrad[self.EME] * 2)
        cos_beta = np.sqrt(
                (sin_i_e_2 - cos_phiDiv2_2 * sin_2_i * sin_2_e) /
                (sin_i_e_2 - cos_phiDiv2_2 * sin_2_i * sin_2_e + pow(np.sin(geomsGrad[self.EME]),2) * pow(np.sin(geomsGrad[self.INC]),2) * pow(np.sin(geomsGrad[self.PHI]),2)))
        configuredGeometries[self.BETA] = np.arccos(cos_beta)

        #compute Gamma
        #tmp_angle=np.arccos(np.cos(geomsGrad[self.EME]) / cos_beta)
        #configuredGeometries[self.GAMMA] = np.where(geomsGrad[self.PHI]<ma.pi/2.,-tmp_angle,tmp_angle)
        configuredGeometries[self.GAMMA] = np.arctan((np.cos(geomsGrad[self.INC])/np.cos(geomsGrad[self.EME])-np.cos(configuredGeometries[self.ALPHA]))/np.sin(configuredGeometries[self.ALPHA]))

        return configuredGeometries
