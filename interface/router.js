import Vue from 'vue';
import VueRouter from 'vue-router';

import Home from './views/Home.vue';
import Simulation from './views/Simulation.vue';
import MassDataAnalysis from './views/MassDataAnalysis.vue';
import Documentation from './views/Documentation.vue';
import NotFound from './views/NotFound.vue';

Vue.use(VueRouter);

const routes = [
    {
        path: '/',
        redirect: '/home'
    },
    {
        path: '/home',
        name: 'Home',
        component: Home
    },
    {
        path: '/simulation',
        name: 'Simulation',
        component: Simulation
    },
    {
        path: '/massdata',
        name: 'MassDataAnalysis',
        component: MassDataAnalysis
    },
    {
        path: '/documentation',
        name: 'Documentation',
        component: Documentation
    },
    {
        path :'*',
        name: 'NotFound',
        component: NotFound
    }
];

const router = new VueRouter({
    mode: 'history',
    routes
});

export default router;