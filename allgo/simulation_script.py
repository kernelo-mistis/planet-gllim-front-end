try:
    from kernelo_app.general_functions.all_functions import *
except ImportError:
    from all_functions import *
try:
    from kernelo_app.parsing.config_parsers import MainParser
except ImportError:
    from config_parsers import MainParser

import os.path
import argparse
import pickle
import logging
logging.getLogger().setLevel(logging.INFO)

        
def pre_process_inputs():

    parser = argparse.ArgumentParser(description="Just an example", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("--config", help="Simulation configuration file")
    parser.add_argument("--geometries", help="Multi-angular geometries file")
    parser.add_argument("--model", help="Python file describing a custom photometric model")
    parser.add_argument("--observations", help="Observation file composed of observed reflectances and incertitudes")
    args = parser.parse_args()
    job_config = vars(args)

    config_file = job_config.get('config')
    geometries_file = job_config.get('geometries')
    external_model_file = job_config.get('model')
    observations_file = job_config.get('observations')

    logging.info(
        '''Processing files:
            configuration file : {}
            geometries file : {}
            external model file : {}
            observations file : {}
        '''.format(config_file, geometries_file, external_model_file, observations_file)
    )

    cwd = os.getcwd()
    config_file_path = None if config_file == None else os.path.join(cwd, config_file)
    geometries_file_path = None if geometries_file == None else os.path.join(cwd, geometries_file)
    external_model_file_path = None if external_model_file == None else os.path.join(cwd, external_model_file)
    observations_file_path = None if observations_file == None else os.path.join(cwd, observations_file)

    return read_inputs(config_file_path, geometries_file_path, external_model_file_path, observations_file_path)

    
def execution_pipeline(config, geometries, observations):

    # Parse config file
    parser = MainParser()
    physical_model, stat_model, data_size, gllim, predictor, importance_sampler, imis_sampler, mean_prop_law, center_prop_law = \
        parser.parse(config, geometries)
    logging.info("  Simulation configuration is set up")

    # Check geometries dimensions before execution
    if len(observations[0,0]['reflectances']) != physical_model.get_D_dimension() or len(observations[0, 0]['incertitudes']) != physical_model.get_D_dimension() :
        logging.error("Geometries dimensions does not correspond to the observations")
        logging.info("---------- End of simulation ----------")
        # raise ValueError("Geometries dimensions does not correspond to the observations")
        sys.exit()

    # Useful for Prediction step and IS step (for now)
    null_pred_dim = (
                physical_model.get_L_dimension(),
                config["prediction"]["k_pred_means"],
                config["prediction"]["k_merged"]
                )
    
    if not os.path.isfile('gllim'): #[TODO] faire qqch de plus robuste

        x_gen, y_gen = stat_model.gen_data(data_size)
        write_synthetic_dataset(x_gen, y_gen)
        logging.info("The generation of the training dataset is done")

        gllim.initialize(x_gen, y_gen)
        logging.info("The initialisation of the GLLiM model is done")

        gllim.train(x_gen, y_gen)
        logging.info("The training of the GLLiM model is done")

        gllim_parameters = gllim.exportModel()
        with open('gllim', 'wb') as f:
            pickle.dump(gllim_parameters, f, pickle.HIGHEST_PROTOCOL)
        f.close()
        logging.info("GlliM model is saved for future executions")

    else:

        with open('gllim', 'rb') as f:
            gllim_parameters = pickle.load(f)
        f.close()
        gllim.importModel(gllim_parameters)
        logging.info("GLLiM model is already trained, previous instance is loaded")

    predictions = predict(observations, predictor, null_pred_dim)
    logging.info("  Predictions step is done")

    is_results = importance_sampling(predictions, observations, imis_sampler, mean_prop_law, center_prop_law ,null_pred_dim)
    logging.info("  Important sampling step is done")

    results = write_results(predictions, is_results, observations, physical_model, predictor)
    logging.info("  Results are saved in files")

    return results


def post_process_outputs(results, wavelengths, keys, title, config):
    data_source_type = config.get('entry_data').get('data_source')
    cwd = os.getcwd()
    if data_source_type == 'remoteSensing':
        write_remote_sensing_file(results, cwd, wavelengths)
    elif data_source_type == 'laboratory':
        write_json_file(results, cwd, wavelengths, keys, title)



if __name__ == '__main__':
    config, geometries, observations, wavelengths, keys, observations_file_name = pre_process_inputs()     # Specific to allgo inputs
    results = execution_pipeline(config, geometries, observations)                      # Planet-GLLiM simulation routine and
    post_process_outputs(results, wavelengths, keys, observations_file_name, config)                 # Specific to allgo outputs