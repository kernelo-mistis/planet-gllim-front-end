ARG image
FROM $image

RUN pip install zipfile36 billiard==3.6.3.0
COPY ./allgo .
COPY ./kernelo_app/general_functions/all_functions.py .
COPY ./kernelo_app/parsing/config_parsers.py .
COPY ./kernelo_app/parsing/parsers.py .
RUN chmod +x entrypoint
ENTRYPOINT [ "/entrypoint" ]