ARG image
FROM $image

USER root
RUN apt-get update
RUN apt-get install -y --no-install-recommends apt-utils software-properties-common python3-pip vim
RUN DEBIAN_FRONTEND=noninteractive TZ=Etc/UTC apt-get -y install tzdata
RUN add-apt-repository ppa:ubuntugis/ppa && \
    apt-get update && \
    apt-get install -y gdal-bin libgdal-dev && \
    export CPLUS_INCLUDE_PATH=/usr/include/gdal && \
    export C_INCLUDE_PATH=/usr/include/gdal && \
    pip install --global-option=build_ext --global-option="-I/usr/include/gdal" GDAL==3.3.2
COPY *.so .