ARG image
FROM $image

# Setup env
# PYTHONUNBUFFERED 1 : Allows for log messages to be immediately dumped to the stream instead of being buffered.
# PYTHONFAULTHANDLER 1 : This module contains functions to dump Python tracebacks explicitly, on a fault, after a timeout, or on a user signal.
ENV PYTHONUNBUFFERED 1
ENV PYTHONFAULTHANDLER 1

# Install general app dependencies.
# Note: Nodejs version 14.x is installed (10.x and 20.x lead to node-gyp error when npm install)
# RUN apt-get update
RUN apt-get install -y curl python-celery-common libpq-dev python-dev-is-python3
RUN curl -fsSL https://deb.nodesource.com/setup_14.x | bash - &&\
    apt-get install -y nodejs

# Create kernelo user
RUN useradd -ms /bin/bash kernelo
USER kernelo
WORKDIR /home/kernelo

# Copy project
RUN mkdir app
COPY . app
WORKDIR /home/kernelo/app

# Install Django dependencies (backend)
RUN pip install -r ./requirements.txt --no-warn-script-location

# Clean up and prepare files for build
USER root
RUN rm -rf kernelo_app/kernelo_library
RUN mkdir kernelo_app/kernelo_library
RUN mv /*.so /home/kernelo/app/kernelo_app/kernelo_library/kernelo.so
RUN chmod +x build/start.sh
RUN rm -rf interface/bundles
RUN rm -rf node_modules
RUN mkdir interface/bundles
RUN rm -rf media
RUN mkdir media
RUN chown -R kernelo /home/kernelo/app

# Install Vuejs dependencies (frontend)
USER kernelo
RUN npm install

# Compile frontend vue into static files with webpack and collect static files with Django
RUN npx webpack --config webpack.config.js
RUN python3 manage.py collectstatic --noinput