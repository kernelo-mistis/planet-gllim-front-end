from rest_framework import routers
from kernelo_app.viewsets import SimulationViewSet
from kernelo_app.viewsets import LaboratoryDataViewSet
from kernelo_app.viewsets import RemoteSensingDataViewSet
from kernelo_app.viewsets import CustomModelViewSet

router = routers.DefaultRouter()

router.register(r'simulation', SimulationViewSet)
router.register(r'laboratoryData', LaboratoryDataViewSet)
router.register(r'remoteSensingData', RemoteSensingDataViewSet)
router.register(r'customModel', CustomModelViewSet)
