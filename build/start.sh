#!/bin/bash
python3 /home/kernelo/app/manage.py collectstatic --noinput
python3 /home/kernelo/app/manage.py makemigrations
python3 /home/kernelo/app/manage.py migrate
python3 manage.py createsuperuser --noinput --username $DJANGO_SUPERUSER_USERNAME
python3 /home/kernelo/app/manage.py runserver 0.0.0.0:8080
