FROM kernelo_coverage
# In development mode we need all the tools from kernelo_builder and kernelo_coverage

# python runner
RUN apt-get update
RUN apt-get install -y --no-install-recommends apt-utils software-properties-common python3-pip vim
RUN DEBIAN_FRONTEND=noninteractive TZ=Etc/UTC apt-get -y install tzdata
RUN add-apt-repository ppa:ubuntugis/ppa && \
    apt-get update && \
    apt-get install -y gdal-bin libgdal-dev && \
    export CPLUS_INCLUDE_PATH=/usr/include/gdal && \
    export C_INCLUDE_PATH=/usr/include/gdal && \
    pip install --global-option=build_ext --global-option="-I/usr/include/gdal" GDAL==3.3.2

# Setup env
# PYTHONUNBUFFERED 1 : Allows for log messages to be immediately dumped to the stream instead of being buffered.
# PYTHONFAULTHANDLER 1 : This module contains functions to dump Python tracebacks explicitly, on a fault, after a timeout, or on a user signal.
ENV PYTHONUNBUFFERED 1
ENV PYTHONFAULTHANDLER 1

# Install general app dependencies.
# Note: Nodejs version 14.x is installed (10.x and 20.x lead to node-gyp error when npm install)
RUN apt-get install -y curl python-celery-common libpq-dev python-dev-is-python3
RUN curl -fsSL https://deb.nodesource.com/setup_14.x | bash - &&\
    apt-get install -y nodejs

# Specific to the dev container
RUN apt-get install -y git rabbitmq-server
RUN service rabbitmq-server start

# Documentation dependencies
RUN pip install -r docs/requirements.txt --no-warn-script-location