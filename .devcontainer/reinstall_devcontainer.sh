#!/bin/bash

# Install Django dependencies (backend)
pip install -r ./requirements.txt --no-warn-script-location

# Clean up and prepare files for build
mkdir kernelo_app/kernelo_library
mv /*.so /home/kernelo/app/kernelo_app/kernelo_library/kernelo.so
rm -rf interface/bundles
rm -rf node_modules
mkdir interface/bundles
mkdir media

# Install Vuejs dependencies (frontend)
npm install

# Compile frontend vue into static files with webpack and collect static files with Django
npx webpack --config webpack.config.js
python3 manage.py collectstatic --noinput

# Migrate DB models
python3 manage.py makemigrations
python3 manage.py migrate
