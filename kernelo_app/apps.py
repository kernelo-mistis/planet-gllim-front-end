from django.apps import AppConfig


class KerneloConfig(AppConfig):
    name = 'kernelo_app'
