from rest_framework import viewsets, status
from rest_framework.response import Response
from .models import Simulation, LaboratoryData, RemoteSensingData, CustomModel
from .serializers import SimulationSerializer, LaboratoryDataSerializer, RemoteSeningDataSerializer, CustomModelForm
from kernelo_app.io.data_mangers import GeometriesManager, IntermediatesResultsManager, ConfigsManager


from kernelo_app.tasks import execute_simulation
from celery import uuid

class SimulationViewSet(viewsets.ModelViewSet):
    queryset = Simulation.objects.all()
    serializer_class = SimulationSerializer

    def create(self, request, *args, **kwargs):
        serializer = SimulationSerializer(data=request.data)
        try:
            Simulation.objects.get(simulation_id=serializer.initial_data.get('simulation_id'))
            task_id = uuid()
            execute_simulation.apply_async(args=(serializer.initial_data,), task_id=task_id)

            return Response({'simulation_id': serializer.initial_data.get('simulation_id'),
                             'task_id': task_id})
            # with open(results_filepath, "rb") as file:
            #     response = HttpResponse(file.read(), content_type="application/zip")
            #     response['content-Disposition'] = "attachment, filename={}".format(
            #         serializer.initial_data.get('simulation_title')+'_'+date.today().strftime("%d/%m/%Y")+'_results.zip')
            #     return response
        except Simulation.DoesNotExist:
            if serializer.is_valid():
                serializer.save()
                #------------------------------- function : create config file ---------------------------#
                ConfigsManager.write_config(serializer.data.get('simulation_id'), serializer.data.get('simulation_config'))

                #------------------------------- function : create temporary folders and file ------------#
                _ = IntermediatesResultsManager(serializer.data.get('physical_model'),
                                                                    serializer.data.get('data_generation'),
                                                                    serializer.data.get('gllim'),
                                                                    serializer.data.get('prediction'),
                                                                    serializer.data.get('importance_sampling'))
                                                                    # serializer.data.get('imis_sampling'))
                if GeometriesManager.get_geometries(serializer.data.get('physical_model')) is None and  request.data['geometries_content'] != "undefined":
                    GeometriesManager.write_geometries(serializer.data.get('physical_model'), request.data['geometries_content'])

                task_id = uuid()
                execute_simulation.apply_async(args=(serializer.data,), task_id=task_id)

                return Response({'simulation_id': serializer.data.get('simulation_id'),
                                 'task_id': task_id})

            else:
                return Response(serializer.errors,
                                status=status.HTTP_400_BAD_REQUEST)


class LaboratoryDataViewSet(viewsets.ModelViewSet):
    queryset = LaboratoryData.objects.all()
    serializer_class = LaboratoryDataSerializer

    def create(self, request, *args, **kwargs):
        serializer = LaboratoryDataSerializer(data=request.data)

        try:
            LaboratoryData.objects.get(entry_id=serializer.initial_data.get('entry_id'))
            return Response({'success': False, 'error': 'Data already exists'},
                            status=status.HTTP_208_ALREADY_REPORTED)
        except LaboratoryData.DoesNotExist:
            if serializer.is_valid():
                serializer.save()
                return Response(status=status.HTTP_200_OK)
            else:
                return Response(serializer.errors,
                                status=status.HTTP_400_BAD_REQUEST)


class RemoteSensingDataViewSet(viewsets.ModelViewSet):
    queryset = RemoteSensingData.objects.all()
    serializer_class = RemoteSeningDataSerializer

    def create(self, request, *args, **kwargs):
        serializer = RemoteSeningDataSerializer(data=request.data)

        try:
            RemoteSensingData.objects.get(entry_id=serializer.initial_data.get('entry_id'))
            return Response({'success': False, 'error': 'Data already exists'},
                            status=status.HTTP_208_ALREADY_REPORTED)
        except RemoteSensingData.DoesNotExist:
            if serializer.is_valid():
                serializer.save()
                return Response(status=status.HTTP_200_OK)
            else:
                return Response(serializer.errors,
                                status=status.HTTP_400_BAD_REQUEST)


class CustomModelViewSet(viewsets.ModelViewSet):
    queryset = CustomModel.objects.all()

    def create(self, request, *args, **kwargs):
        form = CustomModelForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return Response(status=status.HTTP_200_OK)
        else:
           return Response(status=status.HTTP_208_ALREADY_REPORTED)
