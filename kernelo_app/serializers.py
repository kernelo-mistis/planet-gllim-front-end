from rest_framework import serializers
from .models import Simulation
from .models import LaboratoryData
from .models import RemoteSensingData
from .models import CustomModel
from django import forms


class SimulationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Simulation
        fields = '__all__'


class LaboratoryDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = LaboratoryData
        fields = '__all__'


class RemoteSeningDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = RemoteSensingData
        fields = '__all__'


class CustomModelForm(forms.ModelForm):
    class Meta:
        model = CustomModel
        fields = '__all__'
