from generedoc import _render_list, _render_enum, render_top_level
from parsers_test import a
from config import MainParser
import sys

def test_list() -> None:
    print(_render_list([("functionnal", "ldmdml"), ("data", "dsmùldsd")]))


def test_enum() -> None:
    print(_render_enum(["value1", "value2"]))


def test_struct() -> None:
    print(render_top_level(a, 3))

def test_config_doc() -> None:
    level = 3
    if len(sys.argv) >= 2:
        level = int(sys.argv[1])
    print(render_top_level(MainParser(), level))

if __name__ == "__main__":
    # test_list()
    # test_enum()
    # test_struct()
    test_config_doc()
