from kernelo_app.models import Simulation
from kernelo_app.parsing.config_parsers import MainParser
from kernelo_app.io.data_mangers import *
from kernelo_app.general_functions.all_functions import *

import numpy as np
from abc import ABC, abstractmethod
import logging
from time import time
import sys


class Executor(ABC):
    def __init__(self, interm_result_manager):
        self._physical_model = None
        self._stat_model = None
        self._gllim_model = None
        self._predictor = None
        self._importance_sampler = None
        self._imis_sampler = None
        self._mean_prop_law = None
        self._center_prop_law = None
        self._data_manager = None
        self._data_global_relative_uncertainty = None
        self._simulation_id = None
        self._interm_result_manager = interm_result_manager

    @abstractmethod
    def read_obsevations(self, data_id):
        pass

    def execute_simulation(self, task_model):

        logging.info("---------- Start of simulation ----------")

        #---------------------- Pre process inputs ----------------------
        
        task_model.stopeable = False
        task_model.save()

        simulation = Simulation.objects.get(pk=task_model.simulation_id)
        self._simulation_id = task_model.simulation_id

        config = ConfigsManager.get_config(task_model.simulation_id)

        geometries = GeometriesManager.get_geometries(simulation.physical_model)
        logging.info("Geometries are loaded")

        if config['physical_model']['model'] == 'extern':
            config['physical_model']['config'] = ExternalPhysicalModelManager.get_external_model_config(config['physical_model']['config']['hash'])
            logging.info("External model module is loaded")

        observations = self.read_obsevations(simulation.data_id)
        logging.info("Observation data are loaded")

        task_model.stopeable = True
        task_model.save()

        #---------------------- Execution pipeline ----------------------
        
        # Parse config file
        parser = MainParser()
        self._physical_model, self._stat_model, dataset_size, self._gllim_model, self._predictor, self._importance_sampler, self._imis_sampler, self._mean_prop_law, self._center_prop_law = \
            parser.parse(config, geometries)
        self._data_global_relative_uncertainty = config['entry_data'].get('data_global_relative_uncertainty', 0.1)
        logging.info("Simulation modules are loaded")

        # Check geometries dimensions before execution
        if len(observations[0,0]['reflectances']) != self._physical_model.get_D_dimension() or len(observations[0, 0]['incertitudes']) != self._physical_model.get_D_dimension() :
            logging.error("Geometries dimensions does not correspond to the observations")
            task_model.step = 8
            task_model.save()
            logging.info("---------- End of simulation ----------")
            sys.exit()
        
        # Useful for Prediction step and IS step (for now)
        null_pred_dim = (
                    self._physical_model.get_L_dimension(),
                    config["prediction"]["k_pred_means"],
                    config["prediction"]["k_merged"]
                    )

        # Generate training dataset
        x_train, y_train = self._stat_model.gen_data(dataset_size)
        synthetic_dataset = {}
        synthetic_dataset['X'] = x_train
        synthetic_dataset['Y'] = y_train
        SyntheticDatasetManager.write_dataset(simulation.simulation_id, synthetic_dataset, simulation.physical_model)
        task_model.step = 1
        task_model.save()
        logging.info("The generation of the training dataset is done")

        gllim_model_params = self._interm_result_manager.read_gllim()
        if gllim_model_params is None:
            # initialize the GLLiM model
            self._gllim_model.initialize(x_train, y_train)
            task_model.step = 2
            task_model.save()
            logging.info("The initialisation of the GLLiM model is done")

            # train the GLLiM model
            self._gllim_model.train(x_train, y_train)
            logging.info("The training of the GLLiM model is done")

            # export and save model
            gllim_model_params = self._gllim_model.exportModel()
            task_model.stopeable = False
            task_model.save()
            self._interm_result_manager.write_gllim(gllim_model_params)
            logging.info("GlliM model is saved for future executions")
            task_model.stopeable = True
            task_model.save()
        else:
            self._gllim_model.importModel(gllim_model_params)
            logging.info("GLLiM model is already trained, previous instance is loaded")
        inverse_gllim_model_params = self._gllim_model.getInverse()
        GLLiMParametersManager.write_gllim(simulation.simulation_id, gllim_model_params, inverse_gllim_model_params) # write GLLiM parameters in file
        task_model.step = 3
        task_model.save()
 
        # Make predictions
        predictions = self._interm_result_manager.read_predictions()
        if predictions is None:
            ts = time()
            predictions = predict(observations, self._predictor, null_pred_dim)
            logging.info('Prediction took %s sec', time() - ts)
            task_model.stopeable = False
            task_model.save()
            self._interm_result_manager.write_predictions(predictions)
            logging.info("Predictions are saved for future executions")
            task_model.stopeable = True
            task_model.save()
        else:
            logging.info("Observations already processed, previous predictions are loaded")
        task_model.step = 4
        task_model.save()
        logging.info("Prediction step is done")

        # Apply Importance Sampling on predictions (IMIS algo)
        is_results = self._interm_result_manager.read_is_results()
        if is_results is None:
            ts = time()
            is_results = importance_sampling(predictions, observations, self._imis_sampler, self._mean_prop_law, self._center_prop_law ,null_pred_dim)
            logging.info('IS took %s sec', time() - ts)
            task_model.stopeable = False
            task_model.save()
            self._interm_result_manager.write_is_results(is_results)
            task_model.stopeable = True
            task_model.save()
            logging.info("Importance sampling results are saved for future executions")
        else:
            logging.info("Predictions already processed, previous importance sampling results are loaded")
        task_model.step = 5
        task_model.save()
        logging.info("Importance sampling step is done")

        # Formatting results and indicators in a json
        results = write_results(predictions, is_results, observations, self._physical_model, self._predictor)

        #---------------------- Post process outputs --------------------

        self._data_manager.write(results, self._simulation_id)
        logging.info("Simulation results are saved")
        task_model.step = 6
        task_model.save()

        #----------------------------------------------------------------
        
        logging.info("---------- End of simulation ----------")


class LaboratorySimulationExecutor(Executor):
    def read_obsevations(self, data_id):
        self._data_manager = LaboratoryDataManager(data_id, self._data_global_relative_uncertainty)
        observations, keys = self._data_manager.get_reflectances()
        return np.array(observations)


class RemoteSensingSimulationExecutor(Executor):
    def __init__(self, interm_result_manager):
        super(RemoteSensingSimulationExecutor, self).__init__(interm_result_manager)
        self._mask = []

    def read_obsevations(self, data_id):
        self._data_manager = RemoteSensingDataManager(data_id)
        observations = np.array(self._data_manager.get_reflectances())
        self._mask = np.ones(shape=(observations.shape[0], observations.shape[1]), dtype=bool)

        for scene in range(observations.shape[0]):
            for wavelength in range(observations.shape[1]):
                if np.isnan(observations[scene, wavelength]['reflectances']).any() or\
                        np.isnan(observations[scene, wavelength]['incertitudes']).any():
                    self._mask[scene, wavelength] = False

        return observations[::, :]

