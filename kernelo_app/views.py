from django.http import JsonResponse, FileResponse, HttpResponse
from django.middleware import csrf
from kernelo_app import tasks
import json
from datetime import date
from kernelo_app.io.data_mangers import LogManager, ResultsManager, SyntheticDatasetManager, GLLiMParametersManager
from kernelo_app.models import TaskModel, Simulation

def get_csrf_token(request):
    token = csrf.get_token(request)
    return JsonResponse({'token': token})


def stop_simulation(request):
    task_id = json.loads(request.body)['task_id']
    stopped = tasks.stop_simulation(task_id)
    return JsonResponse({'stopped': stopped})


def get_status(request):
    task_id = json.loads(request.body)['task_id']
    step = tasks.get_status(task_id)
    return JsonResponse({'step' : step})


def get_results(request):
    task_id = json.loads(request.body)['task_id']
    simulation_id = TaskModel.objects.get(pk=task_id).simulation_id
    results_filepath = ResultsManager.get_results_file(simulation_id)
    simulation_title = Simulation.objects.get(simulation_id=simulation_id).simulation_title

    with open(results_filepath, "rb") as file:
        response = HttpResponse(file.read(), content_type="application/zip")
        response['content-disposition'] = 'attachment; filename="{}"'.format(
            simulation_title+'_'+date.today().strftime("%d/%m/%Y")+'_results.zip')
        return response


def get_log(request):
    task_id = json.loads(request.body)['task_id']
    simulation_id = TaskModel.objects.get(pk=task_id).simulation_id
    log_filepath = LogManager.get_log_file(simulation_id)
    simulation_title = Simulation.objects.get(simulation_id=simulation_id).simulation_title
    with open(log_filepath, "rb") as file:
        response = HttpResponse(file.read(), content_type="text/plain")
        response['content-disposition'] = 'attachment; filename="{}"'.format(
            simulation_title+'_'+date.today().strftime("%d/%m/%Y")+'.log')
        return response

def get_dataset(request):
    task_id = json.loads(request.body)['task_id']
    simulation_id = TaskModel.objects.get(pk=task_id).simulation_id
    dataset_filepath = SyntheticDatasetManager.get_dataset_file(simulation_id)
    simulation_title = Simulation.objects.get(simulation_id=simulation_id).simulation_title
    with open(dataset_filepath, "rb") as file:
        response = HttpResponse(file.read(), content_type="application/zip")
        response['content-disposition'] = 'attachment; filename="{}"'.format(
            simulation_title+'_dataset_'+date.today().strftime("%d/%m/%Y")+'.zip')
        return response

def get_gllim(request):
    task_id = json.loads(request.body)['task_id']
    simulation_id = TaskModel.objects.get(pk=task_id).simulation_id
    gllim_filepath = GLLiMParametersManager.get_gllim_file(simulation_id)
    simulation_title = Simulation.objects.get(simulation_id=simulation_id).simulation_title
    with open(gllim_filepath, "rb") as file:
        response = HttpResponse(file.read(), content_type="application/zip")
        response['content-disposition'] = 'attachment; filename="{}"'.format(
            simulation_title+'_gllim_'+date.today().strftime("%d/%m/%Y")+'.zip')
        return response

def get_post_treat_results(request):
    simulation_id = json.loads(request.body)['simulation_id']
    simulation = Simulation.objects.get(simulation_id=simulation_id)
    data_source = simulation.data_source
    return JsonResponse({"results" : ResultsManager.get_post_treat_results(simulation_id, data_source)})
