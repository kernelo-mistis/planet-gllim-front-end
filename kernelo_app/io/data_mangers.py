import os
import sys
from os import path
import fnmatch
import json, simplejson
import numpy as np
from zipfile import ZipFile
from netCDF4 import Dataset
import logging
from inspect import isclass
import importlib.util
try:
    from kernelo_app.kernelo_library import kernelo as ker
except ImportError as e:
    try:
        import kernelo as ker
    except ImportError:
        raise ImportError("Unable to import kernelo")

gdal_dir = path.abspath("/home/reverse-proxy/.local/lib/python3.7/site-packages")
sys.path.insert(0, gdal_dir)
try:
    from osgeo import gdal
except ImportError:
    import gdal

from kernelo_app.general_functions.all_functions import *

class LaboratoryDataManager:
    def __init__(self, data_id, relative_uncertainty):
        self.__data, self.__title = self.__read(data_id)
        self.__relative_uncertainty = relative_uncertainty
        self.__keys = None

    def get_reflectances(self):
        observations, self.__keys = get_reflectances_laboratory(self.__data, self.__relative_uncertainty)
        return observations, self.__keys

    def get_geometries(self):
        sza, vza, phi = np.array(self.__data['geometries']).T
        return sza, vza, phi

    def __read(self, data_id):
        cwd = os.getcwd()
        filepath = os.path.join(cwd, 'media', 'data', data_id)
        filename = fnmatch.filter(os.listdir(filepath), '*.json')[0]
        filepath = os.path.join(filepath, filename)

        with open(filepath) as observations_file:
            data = json.load(observations_file)
        return data[list(data.keys())[0]], list(data.keys())[0]

    def write(self, results, simulation_id):
        cwd = os.getcwd()
        folder_path = os.path.join(cwd, 'media', 'simulation', simulation_id)
        wavelengths = get_wavelengths_laboratory(self.__data)
        return write_json_file(results, folder_path, wavelengths, self.__keys, self.__title)


class ExternalPhysicalModelManager:

    @staticmethod
    def get_external_model_config(physical_model_id):
        cwd = os.getcwd()
        models_folder_path = os.path.join(cwd, 'media', 'external_model')
        external_model_folder_path = os.path.join(models_folder_path, physical_model_id)
        filename = fnmatch.filter(os.listdir(external_model_folder_path), '*.py')[0]
        filepath = os.path.join(external_model_folder_path, filename)
        filedir = os.path.dirname(filepath)
        if filename.endswith('.py'):
            filename = os.path.splitext(filename)[0]
        spec = importlib.util.spec_from_file_location(filename, filepath)
        foo = importlib.util.module_from_spec(spec)
        sys.modules[filename] = foo
        spec.loader.exec_module(foo)
        class_name = [x for x in dir(foo) if isclass(getattr(foo, x))][0]
        return {"file_name": filename, "file_path": filedir, "class_name": class_name, "hash": physical_model_id}


class RemoteSensingDataManager:
    def __init__(self, data_id):
        self.__data_rho = self.__read(data_id, 'rho_mod')
        self.__data_drho = self.__read(data_id, 'drho')

    def get_reflectances(self):
        return get_reflectances_remote_sensing(self.__data_rho, self.__data_drho)

    def __read(self, data_id, extension):
        filepath = os.path.join(os.path.realpath('.'), 'media', 'data', data_id)
        try:
            filename = fnmatch.filter(os.listdir(filepath), '*_' + extension)[0]
        except:
            logging.error("Unable to load ENVI file " + extension)
            return None
        dataset = gdal.Open(os.path.join(filepath, filename), gdal.GA_ReadOnly)
        return dataset

    def write(self, results, simulation_id):
        cwd = os.getcwd()
        folder_path = os.path.join(cwd, 'media', 'simulation', simulation_id)
        wavelengths = get_wavelengths_remote_sensing(self.__data_rho)
        return write_remote_sensing_file(results, folder_path, wavelengths)


class GeometriesManager:
    @staticmethod
    def get_geometries(physical_model_id):
        cwd = os.getcwd()

        temporary_folder_path = os.path.join(cwd, 'media', 'temp', 'intermediate_results.nc')
        rootgrp = Dataset(temporary_folder_path, "r", format="NETCDF4")
        try :
            geometries = rootgrp["/" + physical_model_id + "/geometries"][:]
            rootgrp.close()
            return geometries.transpose()

        except Exception:
            rootgrp.close()
            return None

    @staticmethod
    def write_geometries(physical_model_id, geometries):
        cwd = os.getcwd()
        temporary_folder_path = os.path.join(cwd, 'media', 'temp', 'intermediate_results.nc')
        rootgrp = Dataset(temporary_folder_path, "a", format="NETCDF4")

        geometries = json.loads(geometries)
        geometries = np.array([geometries['sza'], geometries['vza'], geometries['phi']], dtype=np.double)


        rootgrp["/" + physical_model_id].createDimension("D", geometries.shape[1])
        rootgrp["/" + physical_model_id].createDimension("dimension_geom", 3)
        geometries_var = rootgrp.createVariable("/" + physical_model_id + "/geometries",
                                                "f8", ("dimension_geom", "D"))
        geometries_var[:] = geometries

        rootgrp.close()


class SyntheticDatasetManager:
    @staticmethod
    def write_dataset(simulation_id, dataset, physical_model_id):
        cwd = os.getcwd()
        simulation_folder_path = os.path.join(cwd, 'media', 'simulation', simulation_id)
        geometries = GeometriesManager.get_geometries(physical_model_id)
        config = ConfigsManager.get_config(simulation_id)
        if not os.path.exists(simulation_folder_path):
            os.makedirs(simulation_folder_path)
        dataset_file_path = os.path.join(simulation_folder_path, 'dataset.json')
        dataset_to_write = {'synthetic_dataset': {
                                'X': dataset['X'][:].tolist(),
                                'Y': dataset['Y'][:].tolist()
                                },
                            'config': config
                            }
        if geometries is not None:
            dataset_to_write['geometries'] = geometries.tolist()
        with open(dataset_file_path, 'w') as dataset_file:
            json.dump(dataset_to_write, dataset_file, indent=4)
        zipObj = ZipFile(os.path.join(simulation_folder_path, 'synthetic_dataset.zip'), 'w')
        zipObj.write(dataset_file_path, 'dataset.json')
        os.remove(dataset_file_path)
        zipObj.close()

    @staticmethod
    def get_dataset_file(simulation_id):
        cwd = os.getcwd()
        folder_path = os.path.join(cwd, 'media', 'simulation', simulation_id)
        return os.path.join(folder_path, 'synthetic_dataset.zip')

class GLLiMParametersManager:
    @staticmethod
    def write_gllim(simulation_id, gllim_parameters, gllim_parameters_star):
        cwd = os.getcwd()
        simulation_folder_path = os.path.join(cwd, 'media', 'simulation', simulation_id)
        if not os.path.exists(simulation_folder_path):
            os.makedirs(simulation_folder_path)
        gllim_file_path = os.path.join(simulation_folder_path, 'GLLiM_parameters.json')
        gllim_to_write = {
            "direct_model": {
                "Pi": gllim_parameters.Pi[:].tolist(),
                "A": gllim_parameters.A[:].tolist(),
                "B": gllim_parameters.B[:].tolist(),
                "C": gllim_parameters.C[:].tolist(),
                "Gamma": gllim_parameters.Gamma[:].tolist(),
                "Sigma": gllim_parameters.Sigma[:].tolist(),
            },
            "inverse_model": {
                "Pi_star": gllim_parameters_star.Pi[:].tolist(),
                "A_star": gllim_parameters_star.A[:].tolist(),
                "B_star": gllim_parameters_star.B[:].tolist(),
                "C_star": gllim_parameters_star.C[:].tolist(),
                "Gamma_star": gllim_parameters_star.Gamma[:].tolist(),
                "Sigma_star": gllim_parameters_star.Sigma[:].tolist(),
            },
        }

        with open(gllim_file_path, 'w') as gllim_file:
            json.dump(gllim_to_write, gllim_file, indent=4)
        zipObj = ZipFile(os.path.join(simulation_folder_path, 'GLLiM_parameters.zip'), 'w')
        zipObj.write(gllim_file_path, 'GLLiM_parameters.json')
        os.remove(gllim_file_path)
        zipObj.close()

    @staticmethod
    def get_gllim_file(simulation_id):
        cwd = os.getcwd()
        folder_path = os.path.join(cwd, 'media', 'simulation', simulation_id)
        return os.path.join(folder_path, 'GLLiM_parameters.zip')

class ConfigsManager:
    @staticmethod
    def get_config(simulation_id):
        cwd = os.getcwd()
        config_file_path = os.path.join(cwd, 'media', 'simulation', simulation_id, 'config.json')
        with open(config_file_path) as config_file:
            data = json.load(config_file)
        return data

    @staticmethod
    def write_config(simulation_id, simulation_config):
        cwd = os.getcwd()
        simulation_folder_path = os.path.join(cwd, 'media', 'simulation', simulation_id)
        if not os.path.exists(simulation_folder_path):
            os.makedirs(simulation_folder_path)
        config_file_path = os.path.join(simulation_folder_path, 'config.json')
        with open(config_file_path, 'w', encoding='utf-8') as f:
            f.write(simulation_config)


class LogManager:
    @staticmethod
    def set_logger(simulation_id):
        cwd = os.getcwd()
        folder_path = os.path.join(cwd, 'media', 'simulation', simulation_id)
        formatter = logging.Formatter('%(asctime)s :: %(levelname)s :: %(message)s')

        if os.path.exists(os.path.join(folder_path, 'simulation.log')):
            with open(os.path.join(folder_path, 'simulation.log'), "r") as file:
                old_log = file.read()
            os.remove(os.path.join(folder_path, 'simulation.log'))

            with open(os.path.join(folder_path, 'simulation.log'), "w") as file:
                file.write(old_log)

        handler = logging.FileHandler(filename=os.path.join(folder_path, 'simulation.log'), mode='a')

        handler.setFormatter(formatter)
        logging.getLogger().addHandler(handler)
        logging.getLogger().setLevel(logging.INFO)

    @staticmethod
    def get_log_file(simulation_id):
        cwd = os.getcwd()
        folder_path = os.path.join(cwd, 'media', 'simulation', simulation_id)
        return os.path.join(folder_path, 'simulation.log')


class ResultsManager:
    @staticmethod
    def get_results_file(simulation_id):
        cwd = os.getcwd()
        folder_path = os.path.join(cwd, 'media', 'simulation', simulation_id)
        return os.path.join(folder_path, 'results.zip')

    @staticmethod
    def get_post_treat_results(simulation_id, data_source):
        cwd = os.getcwd()
        file_path = os.path.join(cwd, 'media', 'simulation', simulation_id, 'results.zip')
        zipObj = ZipFile(file_path, 'r')

        if(data_source == 'laboratory'):
            files = {name : json.loads(zipObj.read(name)) for name in zipObj.namelist() if not name.endswith(("MULT.json","REGU_CENTERS.json","REGU_CENTERS_IS.json","REGU_CENTERS_IMIS.json"))}

            first_file = next(iter(files.values())) # select first element value in dict. E.g. files={name1: val1, name22: val2, name3: val3}; next(iter(files.values())) returns val1
            experience_name = list(first_file.keys())[0]

            output = {'errorY': {},
                      'spectra': {}}
            for sample in first_file[experience_name].keys():
                if sample.startswith('reflectance'):
                    output['errorY'][sample] = {}
                    output['spectra'][sample] = {}
                    for key in files.keys():
                        pred_type = key.split(experience_name + '_', 1)[1].split('.json')[0]
                        pred_error = files[key][experience_name][sample]['ERR_Y']
                        output['errorY'][sample][pred_type] = pred_error
                        output['spectra'][sample][pred_type] = {}
                        for param_nb in range(len(files[key][experience_name][sample]['ESTIM_X_PHYSICAL_UNITS'][0])):
                            output['spectra'][sample][pred_type][param_nb] = [param_spectrum[param_nb] for param_spectrum in files[key][experience_name][sample]['ESTIM_X_PHYSICAL_UNITS']]

            output["wavelengths"] = np.concatenate(first_file[experience_name]['wavelengths']).ravel().tolist()
            return simplejson.dumps(output, indent=4, ignore_nan=True)

        elif(data_source == 'remoteSensing'):
            error_Y_files = {name: gdal.Open('/vsizip/'+file_path+'/'+name) for name in zipObj.namelist() if
                     name.endswith("ERR_Y")}
            estim_X_physical_units_files = {name: gdal.Open('/vsizip/'+file_path+'/'+name) for name in zipObj.namelist() if
                     name.endswith("ESTIM_X_PHYSICAL_UNITS")}

            error_first_dataset = next(iter(error_Y_files.values())) # select first element value in dict. E.g. files={name1: val1, name22: val2, name3: val3}; next(iter(files.values())) returns val1
            # nb_scenes = error_first_dataset.RasterYSize
            nb_scenes = 10
            output = {'errorY': {},
                      'spectra': {}}
            for scene in range(nb_scenes):
                output['errorY']['scene_'+scene.__str__()] = {}
                output['spectra']['scene_'+scene.__str__()] = {}

            # Fill errorY data
            for key in error_Y_files.keys():
                dataset = error_Y_files[key]
                ds_raster = dataset.ReadAsArray() # (bands, ysize/lines, xsize/samples) => (nb_wave, n_scenes, 1)
                for scene in range(nb_scenes):
                    pred_type = key.split('_ERR_Y')[0]
                    pred_error = np.concatenate(ds_raster[:, scene, :]).tolist()
                    output['errorY']['scene_'+scene.__str__()][pred_type] = pred_error

            # Fill spectra data
            for key in estim_X_physical_units_files.keys():
                dataset = estim_X_physical_units_files[key]
                ds_raster = dataset.ReadAsArray() # (bands, ysize/lines, xsize/samples) => (nb_wave, n_scenes, L)
                for scene in range(nb_scenes):
                    pred_type = key.split('_ESTIM_X_PHYSICAL_UNITS')[0]
                    output['spectra']['scene_'+scene.__str__()][pred_type] = {}
                    for param_nb in range(dataset.RasterXSize):
                        output['spectra']['scene_'+scene.__str__()][pred_type][param_nb] = ds_raster[:, scene, param_nb].tolist()
            # Fill wavelength
            wavelengths = list(error_first_dataset.GetMetadata_Dict().values()) # mixed up wavelengths
            wavelengths.sort()
            output["wavelengths"] = wavelengths
            return simplejson.dumps(output, indent=4, ignore_nan=True)


class IntermediatesResultsManager:
    def __init__(self, physical_model_id, data_gen_id, gllim_model_id, prediction_id, importance_sampling_id):#, imis_sampling_id):
        self._physical_model_id = physical_model_id
        self._data_gen_id = data_gen_id
        self._gllim_model_id = gllim_model_id
        self._prediction_id = prediction_id
        self._importance_sampling_id = importance_sampling_id
        # self._imis_sampling_id = imis_sampling_id

        cwd = os.getcwd()
        temporary_folder_path = os.path.join(cwd, 'media', 'temp')
        if not os.path.exists(temporary_folder_path):
            os.makedirs(temporary_folder_path)
        try :
            rootgrp = Dataset(os.path.join(temporary_folder_path, 'intermediate_results.nc'), "a", format="NETCDF4")
            rootgrp.createGroup("/" + physical_model_id +
                                "/" + data_gen_id +
                                "/" + gllim_model_id +
                                "/" + prediction_id +
                                "/" + importance_sampling_id)
                                # "/" + imis_sampling_id)
            rootgrp.close()
        except FileNotFoundError:
            try:
                rootgrp = Dataset(os.path.join(temporary_folder_path, 'intermediate_results.nc'), "w", format="NETCDF4")
                rootgrp.createGroup("/" + physical_model_id +
                                    "/" + data_gen_id +
                                    "/" + gllim_model_id +
                                    "/" + prediction_id +
                                    "/" + importance_sampling_id)
                                    # "/" + imis_sampling_id)
                rootgrp.close()
            except OSError:
                os.remove(os.path.join(temporary_folder_path, 'intermediate_results.nc'))
                rootgrp = Dataset(os.path.join(temporary_folder_path, 'intermediate_results.nc'), "w", format="NETCDF4")
                rootgrp.createGroup("/" + physical_model_id +
                                    "/" + data_gen_id +
                                    "/" + gllim_model_id +
                                    "/" + prediction_id +
                                    "/" + importance_sampling_id)
                                    # "/" + imis_sampling_id)
                rootgrp.close()


    def write_gllim(self, gllim):
        cwd = os.getcwd()
        temporary_folder_path = os.path.join(cwd, 'media', 'temp', 'intermediate_results.nc')
        rootgrp = Dataset(temporary_folder_path, "a", format="NETCDF4")

        gllim_group_path = "/" + self._physical_model_id + "/" + self._data_gen_id + "/" + self._gllim_model_id

        rootgrp[gllim_group_path].createDimension("D", gllim.Sigma.shape[1])
        rootgrp[gllim_group_path].createDimension("L", gllim.Gamma.shape[1])
        rootgrp[gllim_group_path].createDimension("K", gllim.Pi.shape[0])

        gllim_A = rootgrp.createVariable(gllim_group_path + "/A", "f8", ("K", "D", "L"))
        gllim_B = rootgrp.createVariable(gllim_group_path + "/B", "f8", ("D", "K"))
        gllim_C = rootgrp.createVariable(gllim_group_path + "/C", "f8", ("L", "K"))
        gllim_Gamma = rootgrp.createVariable(gllim_group_path + "/Gamma", "f8", ("K", "L", "L"))
        gllim_Sigma = rootgrp.createVariable(gllim_group_path + "/Sigma", "f8", ("K", "D", "D"))
        gllim_Pi = rootgrp.createVariable(gllim_group_path + "/Pi", "f8", ("K"))

        gllim_Pi[:] = gllim.Pi
        gllim_A[:] = gllim.A
        gllim_B[:] = gllim.B
        gllim_C[:] = gllim.C
        gllim_Gamma[:] = gllim.Gamma
        gllim_Sigma[:] = gllim.Sigma

        rootgrp.close()

    def read_gllim(self):
        cwd = os.getcwd()
        temporary_folder_path = os.path.join(cwd, 'media', 'temp', 'intermediate_results.nc')
        rootgrp = Dataset(temporary_folder_path, "r", format="NETCDF4")

        gllim_group_path = "/" + self._physical_model_id + "/" + self._data_gen_id + "/" + self._gllim_model_id
        try:
            gllim_model = ker.GLLiMParameters()
            gllim_model.Pi = rootgrp[gllim_group_path + "/Pi"][:]
            gllim_model.A = rootgrp[gllim_group_path + "/A"][:]
            gllim_model.B = rootgrp[gllim_group_path + "/B"][:]
            gllim_model.C = rootgrp[gllim_group_path + "/C"][:]
            gllim_model.Gamma = rootgrp[gllim_group_path + "/Gamma"][:]
            gllim_model.Sigma = rootgrp[gllim_group_path + "/Sigma"][:]
            rootgrp.close()
            return gllim_model

        except Exception:
            rootgrp.close()
            return None

    def write_predictions(self, predictions):
        cwd = os.getcwd()
        temporary_folder_path = os.path.join(cwd, 'media', 'temp', 'intermediate_results.nc')
        rootgrp = Dataset(temporary_folder_path, "a", format="NETCDF4")

        predictions_group_path = "/" + self._physical_model_id + "/" + self._data_gen_id + "/" + self._gllim_model_id + "/" + self._prediction_id

        L = predictions[0, 0].meansPred.mean.shape[0]
        K_merged = predictions[0, 0].centersPred.weights.shape[0]
        K_pred_mean = predictions[0, 0].meansPred.gmm_weights.shape[0]

        rootgrp[predictions_group_path].createDimension("L", L)
        rootgrp[predictions_group_path].createDimension("wavelengths", predictions.shape[1])
        rootgrp[predictions_group_path].createDimension("points", predictions.shape[0])
        rootgrp[predictions_group_path].createDimension("K_merged", K_merged)
        rootgrp[predictions_group_path].createDimension("K_pred_mean", K_pred_mean)

        pred_mean_dtype = np.dtype([('mean', np.float64, (L,)),
                                    ('variance', np.float64, (L,)),
                                    ('gmm_weights', np.float64, (K_pred_mean, )),
                                    ('gmm_means', np.float64, (L, K_pred_mean)),
                                    ('gmm_covs', np.float64, (K_pred_mean, L, L))])

        pred_centers_dtype = np.dtype([('weights', np.float64, (K_merged, )),
                                       ('means', np.float64, (L, K_merged)),
                                       ('covs', np.float64, (K_merged, L, L))])

        prediction_result_dtype = np.dtype([('pred_mean', pred_mean_dtype), ('pred_center', pred_centers_dtype)])
        rootgrp[predictions_group_path].createCompoundType(pred_mean_dtype, 'pred_mean')
        rootgrp[predictions_group_path].createCompoundType(pred_centers_dtype, 'pred_center')
        prediction_result_type = rootgrp[predictions_group_path].createCompoundType(prediction_result_dtype, 'pred_type')
        prediction_var = rootgrp.createVariable(predictions_group_path + "/result", prediction_result_type, ("points", "wavelengths"))

        data = np.empty(shape=(predictions.shape[0], predictions.shape[1]), dtype=prediction_result_dtype)
        for point in range(predictions.shape[0]):
            for wavelength in range(predictions.shape[1]):
                data[point, wavelength]['pred_mean']['mean'] = predictions[point, wavelength].meansPred.mean
                data[point, wavelength]['pred_mean']['variance'] = predictions[point, wavelength].meansPred.variance
                data[point, wavelength]['pred_mean']['gmm_weights'] = predictions[point, wavelength].meansPred.gmm_weights
                data[point, wavelength]['pred_mean']['gmm_means'] = predictions[point, wavelength].meansPred.gmm_means
                data[point, wavelength]['pred_mean']['gmm_covs'] = predictions[point, wavelength].meansPred.gmm_covs

                data[point, wavelength]['pred_center']['weights'] = predictions[point, wavelength].centersPred.weights
                data[point, wavelength]['pred_center']['means'] = predictions[point, wavelength].centersPred.means
                data[point, wavelength]['pred_center']['covs'] = predictions[point, wavelength].centersPred.covs

        prediction_var[:] = data

        rootgrp.close()

    def read_predictions(self):
        cwd = os.getcwd()
        temporary_folder_path = os.path.join(cwd, 'media', 'temp', 'intermediate_results.nc')
        rootgrp = Dataset(temporary_folder_path, "r", format="NETCDF4")

        predictions_group_path = "/" + self._physical_model_id + "/" + self._data_gen_id + "/" + self._gllim_model_id + "/" + self._prediction_id

        try:
            data = rootgrp[predictions_group_path + "/result"][:]
            predictions = np.empty(dtype=ker.PredictionResultExport, shape=(data.shape[0], data.shape[1]))
            for point in range(data.shape[0]):
                for wavelength in range(data.shape[1]):
                    predictions[point, wavelength] = ker.PredictionResultExport()

                    predictions[point, wavelength].meansPred.mean = data[point, wavelength]['pred_mean']['mean']
                    predictions[point, wavelength].meansPred.variance = data[point, wavelength]['pred_mean']['variance']
                    predictions[point, wavelength].meansPred.gmm_weights = data[point, wavelength]['pred_mean']['gmm_weights']
                    predictions[point, wavelength].meansPred.gmm_means = data[point, wavelength]['pred_mean']['gmm_means']
                    predictions[point, wavelength].meansPred.gmm_covs = data[point, wavelength]['pred_mean']['gmm_covs']

                    predictions[point, wavelength].centersPred.weights = data[point, wavelength]['pred_center']['weights']
                    predictions[point, wavelength].centersPred.means = data[point, wavelength]['pred_center']['means']
                    predictions[point, wavelength].centersPred.covs = data[point, wavelength]['pred_center']['covs']

            rootgrp.close()
            return predictions
        except Exception:
            rootgrp.close()
            return None

    def write_is_results(self, results):
        cwd = os.getcwd()
        temporary_folder_path = os.path.join(cwd, 'media', 'temp', 'intermediate_results.nc')
        rootgrp = Dataset(temporary_folder_path, "a", format="NETCDF4")

        is_group_path = "/" + self._physical_model_id + "/" + self._data_gen_id + "/" + self._gllim_model_id + "/" + self._prediction_id + "/" + self._importance_sampling_id

        L = results[0, 0, 0].mean.shape[0]
        rootgrp[is_group_path].createDimension("points", results.shape[0])
        rootgrp[is_group_path].createDimension("wavelengths", results.shape[1])
        rootgrp[is_group_path].createDimension("nb_solutions", results.shape[2])

        is_result_dtype = np.dtype([('mean', np.float64, (L, )),
                                    ('variance', np.float64, (L, )),
                                    ('nb_effective_sample', np.int),
                                    ('effective_sample_size', np.float64),
                                    ('qn', np.float64)])

        is_results_type = rootgrp[is_group_path].createCompoundType(is_result_dtype, 'is_result_type')
        is_result_var = rootgrp.createVariable(is_group_path + "/result", is_results_type, ("points", "wavelengths", "nb_solutions"))

        data = np.empty(shape=(results.shape[0], results.shape[1], results.shape[2]), dtype=is_result_dtype)
        for solution in range(results.shape[2]):
            for point in range(results.shape[0]):
                for wavelength in range(results.shape[1]):
                    data[point, wavelength, solution]['mean'] = results[point, wavelength, solution].mean
                    data[point, wavelength, solution]['variance'] = results[point, wavelength, solution].covariance
                    data[point, wavelength, solution]['nb_effective_sample'] = results[point, wavelength, solution].diagnostic.nb_effective_sample
                    data[point, wavelength, solution]['effective_sample_size'] = results[point, wavelength, solution].diagnostic.effective_sample_size
                    data[point, wavelength, solution]['qn'] = results[point, wavelength, solution].diagnostic.qn

        is_result_var[:] = data

        rootgrp.close()

    def read_is_results(self):
        cwd = os.getcwd()
        temporary_folder_path = os.path.join(cwd, 'media', 'temp', 'intermediate_results.nc')
        rootgrp = Dataset(temporary_folder_path, "a", format="NETCDF4")

        is_group_path = "/" + self._physical_model_id + "/" + self._data_gen_id + "/" + self._gllim_model_id + "/" + self._prediction_id + "/" + self._importance_sampling_id

        try:
            data = rootgrp[is_group_path + "/result"][:]
            is_results = np.empty(dtype=ker.ImportanceSamplingResult, shape=(data.shape[0], data.shape[1], data.shape[2]))
            for solution in range(data.shape[2]):
                for point in range(data.shape[0]):
                    for wavelength in range(data.shape[1]):
                        is_results[point, wavelength, solution] = ker.ImportanceSamplingResult()
                        is_results[point, wavelength, solution].mean = data[point, wavelength, solution]['mean']
                        is_results[point, wavelength, solution].covariance = data[point, wavelength, solution]['variance']
                        is_results[point, wavelength, solution].diagnostic.nb_effective_sample =  data[point, wavelength, solution]['nb_effective_sample']
                        is_results[point, wavelength, solution].diagnostic.effective_sample_size = data[point, wavelength, solution]['effective_sample_size']
                        is_results[point, wavelength, solution].diagnostic.qn = data[point, wavelength, solution]['qn']

            rootgrp.close()
            return is_results
        except Exception:
            rootgrp.close()
            return None

