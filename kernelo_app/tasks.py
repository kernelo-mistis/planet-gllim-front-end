from __future__ import absolute_import
from celery import shared_task

from kernelo_app.models import TaskModel
from celery import uuid
from kernelo_django_vue.celery import app
from kernelo_app.io.data_mangers import IntermediatesResultsManager, LogManager
from kernelo_app.execution.simulation_executor import RemoteSensingSimulationExecutor, LaboratorySimulationExecutor

import time


@shared_task
def execute_simulation(data):
    task_model = TaskModel(task_id=execute_simulation.request.id,
                           simulation_id=data.get('simulation_id'),
                           stopeable=True,
                           step=0)

    task_model.save()
    interm_result_manager = IntermediatesResultsManager(data.get('physical_model'),
                                                        data.get('data_generation'),
                                                        data.get('gllim'),
                                                        data.get('prediction'),
                                                        data.get('importance_sampling'))
                                                        # data.get('imis_sampling'))
    LogManager.set_logger(task_model.simulation_id)
    if data.get('data_source') == 'remoteSensing':
        RemoteSensingSimulationExecutor(interm_result_manager).execute_simulation(task_model)
    elif data.get('data_source') == 'laboratory':
        LaboratorySimulationExecutor(interm_result_manager).execute_simulation(task_model)


def stop_simulation(task_id):
    task_model = TaskModel.objects.get(pk=task_id)
    if task_model.stopeable:
        task_model.step = 7
        task_model.save()
        app.control.revoke(task_id, terminate=True, signal='SIGKILL')
    else:
        stopped = False
        while not stopped:
            time.sleep(2)
            task_model = TaskModel.objects.get(pk=task_id)
            if task_model.stopeable:
                app.control.revoke(task_id, terminate=True, signal='SIGKILL')
                task_model.step = 7
                task_model.save()
                stopped = True

    return True


def get_status(task_id):
    try:
        task_model = TaskModel.objects.get(pk=task_id)
        return task_model.step
    except TaskModel.DoesNotExist:
        return 9
