import numpy as np
import json
import os
import logging
import sys
import importlib
from inspect import isclass
import billiard as mp
try:
    from osgeo import gdal
except ImportError:
    import gdal
try:
    from zipfile import ZipFile
except ImportError:
    from zipfile36 import ZipFile
try:
    from kernelo_app.kernelo_library import kernelo as ker
except ImportError as e:
    try:
        import kernelo as ker
    except ImportError:
        raise ImportError("Unable to import kernelo")

logging.getLogger().setLevel(logging.INFO)


# ------------------------ Read input files -------------------------

def get_wavelengths_laboratory(data):
    return data['wavelengths']

def get_wavelengths_remote_sensing(data_rho):
    header = data_rho.GetMetadata_Dict() # {'Band_1': '0.1', 'Band_10': '0.5', 'Band_11': '0.6' ...}. Bands are not sorted here.
    wavelengths = list(header.values())
    wavelengths.sort()
    return wavelengths

def get_reflectances_laboratory(data, relative_uncertainty):
    reflectances = []
    keys = []
    for key in data.keys():
        if key.startswith('reflectance'):
            keys.append(key)
            observations = []
            for wavelength in data[key]:
                if len(wavelength) == 2:
                    observations.append({
                        'reflectances': wavelength[0],
                        'incertitudes': wavelength[1]
                    })
                else:
                    observations.append({
                        'reflectances': wavelength[0],
                        'incertitudes': [reflectance * relative_uncertainty for reflectance in wavelength[0]]
                    })
            reflectances.append(observations)
    return reflectances, keys

def get_reflectances_remote_sensing(data_rho, data_drho):

    nb_geometries = data_rho.RasterXSize    # samples
    nb_scenes = data_rho.RasterYSize        # lines
    nb_wavelengths = data_rho.RasterCount   # bands

    reflectances_raster = data_rho.ReadAsArray().reshape(nb_wavelengths, nb_scenes, nb_geometries)
    incertitude_raster = data_drho.ReadAsArray().reshape(nb_wavelengths, nb_scenes, nb_geometries)

    reflectances = []
    for scene in range(nb_scenes):
        observations = []
        for wavelength in range(nb_wavelengths):
            observations.append({
                'reflectances': np.array(reflectances_raster[wavelength, scene, :], dtype=np.double),
                'incertitudes': np.array(incertitude_raster[wavelength, scene, :], dtype=np.double)
            })
        reflectances.append(observations)

    return reflectances

def read_inputs(config_file_path, geometries_file_path, external_model_file_path, observations_file_path):    
    cwd = os.getcwd()

    # get configuration data
    if config_file_path:
        with open(config_file_path) as data:
            config = json.load(data)
    else:
        raise FileNotFoundError("Configuration file not found")

    # get external model file data
    if external_model_file_path:
        filename = os.path.basename(external_model_file_path)
        filedir = os.path.dirname(external_model_file_path)
        if filename.endswith('.py'):
            filename = os.path.splitext(filename)[0]
        spec = importlib.util.spec_from_file_location(filename, external_model_file_path)
        foo = importlib.util.module_from_spec(spec)
        sys.modules[filename] = foo
        spec.loader.exec_module(foo)
        class_name = [x for x in dir(foo) if isclass(getattr(foo, x))][0]
        config['physical_model']['config'] = {"file_name": filename, "file_path": filedir, "class_name": class_name}
        logging.info("External model module is loaded")
        geometries = None
    
    # get geometries data
    else:
        if geometries_file_path:
            with open(geometries_file_path) as data:
                geometries = json.load(data)
            geometries = np.array([geometries['sza'], geometries['vza'], geometries['phi']], dtype=np.double)
            geometries = geometries.transpose() # geometries = np.array([[sza0, vza0, phi0], [sza1, vza1, phi1], ....]) ; size = (nb_geometries, 3)
        else:
            raise FileNotFoundError("Geometries file not found")

    # get observations data
    if observations_file_path:
        observation_type = config.get('entry_data').get('data_source')
        if observation_type == 'remoteSensing':
            with ZipFile(observations_file_path, 'r') as zip_ref:
                zip_ref.extractall(cwd)
            data_rho = gdal.Open(os.path.splitext(observations_file_path)[0] + '_rho_mod', gdal.GA_ReadOnly)
            data_drho = gdal.Open(os.path.splitext(observations_file_path)[0] + '_drho', gdal.GA_ReadOnly)
            wavelengths = get_wavelengths_remote_sensing(data_rho)
            observations = get_reflectances_remote_sensing(data_rho, data_drho)
            observations = np.array(observations)
            keys = None
            # _mask = np.ones(shape=(observations.shape[0], observations.shape[1]), dtype=bool)
            # for scene in range(observations.shape[0]):
            #     for wavelength in range(observations.shape[1]):
            #         if np.isnan(observations[scene, wavelength]['reflectances']).any() or\
            #                 np.isnan(observations[scene, wavelength]['incertitudes']).any():
            #             _mask[scene, wavelength] = False
            #             print("There are NaNs in data")
        elif observation_type == 'laboratory':
            with open(observations_file_path) as data:
                observations_data = json.load(data)
            observations_title = list(observations_data.keys())[0]
            observations_content = observations_data[observations_title]
            data_global_relative_uncertainty = config.get('entry_data').get('data_global_relative_uncertainty', 0.1)
            wavelengths = get_wavelengths_laboratory(observations_content)
            observations, keys = get_reflectances_laboratory(observations_content, data_global_relative_uncertainty)
            observations = np.array(observations)
    else:
        raise FileNotFoundError("Configuration file not found")
    
    # if geometries != None and observations != None:
    #     if len(observations[0,0]['reflectances']) != geometries.shape[0] or len(observations[0,0]['incertitudes']) != geometries.shape[0]:
    #         raise ValueError("Geometries dimensions does not correspond to the observations")
    
    return config, geometries, observations, wavelengths, keys, os.path.splitext(os.path.basename(observations_file_path))[0]


# --------------------------- Prediction ----------------------------

def return_null_pred(null_pred_dim):
    # NOTE: faire ça dans le "cinit" de la lib Kernelo, 
    # de sorte de ker.PredictionResultExport(L,k_pred_mean,kmerged) renvoie des matrices de zeros.
    L, k_pred_mean, k_merged = null_pred_dim
    null_pred = ker.PredictionResultExport()
    null_pred.meansPred.mean = np.zeros(L)
    null_pred.meansPred.variance = np.zeros(shape=(L))
    null_pred.meansPred.gmm_weights = np.zeros(shape=(k_pred_mean))
    null_pred.meansPred.gmm_means = np.zeros(shape=(L, k_pred_mean))
    null_pred.meansPred.gmm_covs = np.zeros(shape=(k_pred_mean, L, L))
    null_pred.centersPred.weights = np.zeros(shape=(k_merged))
    null_pred.centersPred.means = np.zeros(shape=(L, k_merged))
    null_pred.centersPred.covs = np.zeros(shape=(k_merged, L, L))
    return null_pred

def predict_mp(predictor, queue: mp.Manager().Queue(), shared_list: mp.Manager().list()):
    while not queue.empty():
        reflectance, incertitude, id_wavelength, id_sample, nb_pred, nb_wavelengths, null_pred_dim = queue.get()
        # Check if all instance is (float, int). This check shoulb be done at the pre-processing step
        # Note that NaN is float object.
        if not ( all(isinstance(x, (int,float)) for x in reflectance) and\
                all(isinstance(x, (int,float)) for x in incertitude) ):
            print("!!! Some observation data is not a number")
            shared_list.append((id_sample, id_wavelength, return_null_pred(null_pred_dim)))
        else:
            if np.any(np.isnan(reflectance)) or np.any(np.isnan(incertitude)):
                print("!!! Some observation data is NaN")
                shared_list.append((id_sample, id_wavelength, return_null_pred(null_pred_dim)))
            else:
                shared_list.append((id_sample, id_wavelength, predictor.predict(reflectance, incertitude)))
        
        nb_pred_processed = (id_wavelength + 1) + (id_sample * nb_wavelengths)
        if nb_pred_processed % (nb_pred // 10) == 0:
            logging.info("      [Prediction] Processed : {}/{}".format(nb_pred_processed, nb_pred))

def predict(observations, predictor, null_pred_dim):

    nb_samples = observations.shape[0]
    nb_wavelengths = observations.shape[1]
    nb_pred = nb_samples * nb_wavelengths
    predictions = np.empty((nb_samples, nb_wavelengths), dtype=object)
    logging.info("      [Prediction] Starting estimation of {} predictions composed of {} samples/scenes and {} wavelengths".format(nb_pred, nb_samples, nb_wavelengths))
    
    manager = mp.Manager()
    shared_list = manager.list()
    queue = manager.Queue()
    logging.info("      [Prediction] Multiprocessing with {} CPU cores".format(mp.cpu_count()))

    # Create arguments queue for process function
    for id_sample, sample in enumerate(observations):
        for id_wavelength, wavelength in enumerate(sample):
            queue.put((wavelength['reflectances'], wavelength['incertitudes'], id_wavelength, id_sample, nb_pred, nb_wavelengths, null_pred_dim))

    # Create Processes
    # os.sched_getaffinity(0) # Return the set of CPUs the process with PID pid (or the current process if zero) is restricted to.
    nb_processes = mp.cpu_count() * 2 # number of CPUs in the system. We use twice more processes than CPUs because experiences show that it is often the faster option
    processes = [mp.Process(target=predict_mp, args=(predictor, queue, shared_list)) for _ in range(nb_processes)]

    # Start all processes
    for process in processes:
        process.start()

    # Wait for processes to end and close them properly. No need to add process.join()
    mp.connection.wait(p.sentinel for p in processes) # You can use this value if you want to wait on several events at once using multiprocessing.connection.wait(). Otherwise calling join() is simpler => but process.join() lead to issues (simulation stucks)

    for id_sample, id_wavelength, result in shared_list:
        predictions[id_sample, id_wavelength] = result
    logging.info("      [Prediction] Processed : {}/{}".format(nb_pred, nb_pred))

    return predictions


# ----------------------- Importance Sampling -----------------------

def return_null_is(null_pred_dim):
    # NOTE: faire ça dans le "cinit" de la lib Kernelo, 
    # de sorte de ker.PredictionResultExport(L,k_pred_mean,kmerged) renvoie des matrices de zeros.
    L, k_pred_mean, k_merged = null_pred_dim
    null_pred_is = ker.ImportanceSamplingResult()
    null_pred_is.mean = np.zeros(L)
    null_pred_is.covariance = np.zeros(L)
    null_pred_is.diagnostic.nb_effective_sample = 0
    null_pred_is.diagnostic.effective_sample_size = 0
    null_pred_is.diagnostic.qn = 0
    return null_pred_is

def importance_sampling_mp(imis_sampler, mean_prop_law_type, center_prop_law_type, queue: mp.Manager().Queue(), shared_list: mp.Manager().list()):
    while not queue.empty():
        prediction, observation, id_sample, id_wavelength, nb_pred, nb_wavelengths, nb_centers, null_pred_dim = queue.get()

        # Check if all instance is (float, int). This check shoulb be done at the pre-processing step
        # Note that NaN is float object.
        if not ( all(isinstance(x, (int,float)) for x in observation['reflectances']) and\
                all(isinstance(x, (int,float)) for x in observation['incertitudes']) ):
            print("!!! Some observation data is not a number")
            null_is = [return_null_is(null_pred_dim)] * (nb_centers + 1)
            shared_list.append((id_sample, id_wavelength, null_is))
        else:
            if np.any(np.isnan(observation['reflectances'])) or np.any(np.isnan(observation['incertitudes'])):
                print("!!! Some observation data is NaN")
                null_is = [return_null_is(null_pred_dim)] * (nb_centers + 1)
                shared_list.append((id_sample, id_wavelength, null_is))
            else:
                # Only IMIS algo is applied.
                imis_result = []
                mean_prop_law = mean_prop_law_type.create(prediction.meansPred.gmm_weights, prediction.meansPred.gmm_means, prediction.meansPred.gmm_covs)
                result_imis_mean = imis_sampler.execute(mean_prop_law, observation['reflectances'], observation['incertitudes'])
                imis_result.append(result_imis_mean if result_imis_mean is None else return_null_is(null_pred_dim))

                for center in range(nb_centers):
                    center_prop_law = center_prop_law_type.create(prediction.centersPred.means[:, center], prediction.centersPred.covs[center, :, :])
                    result_imis_center = imis_sampler.execute(center_prop_law, observation['reflectances'], observation['incertitudes'])
                    imis_result.append(result_imis_center if result_imis_center is None else return_null_is(null_pred_dim))

                shared_list.append((id_sample, id_wavelength, imis_result))
        
        nb_pred_processed = (id_wavelength + 1) + (id_sample * nb_wavelengths)
        if nb_pred_processed % (nb_pred // 10) == 0:
            logging.info("      [IS] Processed : {}/{}".format(nb_pred_processed, nb_pred))

def importance_sampling(predictions, observations, imis_sampler, mean_prop_law_type, center_prop_law_type, null_pred_dim):

    nb_pred = observations.shape[0] * observations.shape[1]
    nb_centers = predictions[0, 0].centersPred.weights.shape[0]
    nb_samples = predictions.shape[0]
    nb_wavelengths = predictions.shape[1]
    is_results = np.empty((nb_centers + 1, nb_samples, nb_wavelengths), dtype=object) # list[0] is the list for mean ; list[1] is the list for center1 ; list[2] is the list for center2
    logging.info("      [IS] Starting Incremental Mixture Importance Sampling for the {} predictions by the mean and by the {} centers".format(nb_pred, nb_centers))
    
    manager = mp.Manager()
    shared_list = manager.list()
    queue = manager.Queue()
    logging.info("      [IS] Multiprocessing with {} CPU cores".format(mp.cpu_count()))
    
    # Create queue of task (one task per wavelength)
    for id_sample in range(nb_samples):
        for id_wavelength in range(nb_wavelengths):
            queue.put((predictions[id_sample, id_wavelength], observations[id_sample, id_wavelength], 
                id_sample, id_wavelength, nb_pred, nb_wavelengths, nb_centers, null_pred_dim))

    # Create Processes
    nb_processes = mp.cpu_count() * 2
    processes = [mp.Process(target=importance_sampling_mp, args=(imis_sampler, mean_prop_law_type, center_prop_law_type, queue, shared_list)) for _ in range(nb_processes)]

    # Start all processes
    for process in processes:
        process.start()

    # Wait for processes to terminate and close them properly
    mp.connection.wait(p.sentinel for p in processes)

    for id_sample, id_wavelength, result in shared_list:
        is_results[:,id_sample,id_wavelength] = np.array(result)
    logging.info("      [IS] Processed : {}/{}".format(nb_pred, nb_pred))
    
    return is_results


# ----------------------- Formatting results ------------------------

def write_synthetic_dataset(x_train, y_train):
    synthetic_dataset = {}
    synthetic_dataset['X'] = x_train.tolist()
    synthetic_dataset['Y'] = y_train.tolist()
    with open('synthetic_dataset.json', 'w') as f:
        json.dump(synthetic_dataset, f, indent=4)
    f.close()

def compute_reconstruction_error(reconstruction, observation):
    return np.linalg.norm(observation - reconstruction) / (1e-10 + np.linalg.norm(observation))

def compute_multiplicity_index(predictions, observation, reconstructions):
    M = predictions.shape[0]
    L = predictions.shape[1]

    # Compute p_tilda
    p_tilda = np.empty(shape=M)
    for i in range(M):
        p_tilda[i] = np.exp(-0.5 * np.sum(np.square(reconstructions[i] - observation)))

    # Compute p
    p = np.empty(shape=M)
    sum_p_tilda = np.sum(p_tilda)
    for i in range(M):
        p[i] = p_tilda[i]/sum_p_tilda

    # Compute x
    x_mean = np.zeros(shape=L)
    for i in range(M):
        x_mean = x_mean + p[i] * predictions[i]

    # Compute V
    V = 0
    for i in range(M):
        V += p[i] * np.sum(np.square(predictions[i] - x_mean))

    # Compute V_tilda
    V_tilda = V / (0.25 - np.sqrt(np.sum(np.square(x_mean - 0.5)))/ L)

    return V_tilda

def write_output_indicators(N,M,L):
    return {
        'ESTIM_X': np.empty((N,M,L)),
        'ESTIM_X_PHYSICAL_UNITS': np.empty((N,M,L)),
        'STD_X': np.empty((N,M,L)),
        'STD_X_PHYSICAL_UNITS': np.empty((N,M,L)),
        'ERR_Y': np.empty((N,M))
    }

def write_results(predictions, imis_results, observations, physical_model, predictor):
    nb_centers = predictions[0, 0].centersPred.weights.shape[0]
    nb_samples = predictions.shape[0]
    nb_wavelengths = predictions.shape[1]
    L = physical_model.get_L_dimension()

    results = {
        'pred_mean': write_output_indicators(nb_samples, nb_wavelengths, L),
        # 'is_mean': write_output_indicators(nb_samples, nb_wavelengths, L),
        'imis_mean': write_output_indicators(nb_samples, nb_wavelengths, L),
        'x_best': write_output_indicators(nb_samples, nb_wavelengths, L),
        'MULT': np.empty((nb_samples,nb_wavelengths)),
        'REGU_CENTERS': np.empty((nb_samples,nb_wavelengths,L)),
        'REGU_CENTERS_IS': np.empty((nb_samples,nb_wavelengths,L)),
        'REGU_CENTERS_IMIS': np.empty((nb_samples,nb_wavelengths,L))
    }
    for i in range(nb_centers):
        results['pred_center_' + (i+1).__str__()] = write_output_indicators(nb_samples, nb_wavelengths, L)
        # results['is_center_' + (i+1).__str__()] = write_output_indicators(nb_samples, nb_wavelengths, L)
        results['imis_center_' + (i+1).__str__()] = write_output_indicators(nb_samples, nb_wavelengths, L)
    
    for sample in range(nb_samples):

        series = np.empty((nb_wavelengths,L,nb_centers))
        # series_is = np.empty(shape=(nb_wavelengths, L, nb_centers))
        series_imis = np.empty((nb_wavelengths,L,nb_centers))

        for wavelength in range(nb_wavelengths):
            predictions_post = []
            reconstructions = []
            reconstruction_error_min = np.inf # for X best estimation
            prediction_type = 'pred_mean' # for X best estimation

            ### Predictions
            reconstruction = physical_model.F(predictions[sample, wavelength].meansPred.mean)
            reconstruction_error = compute_reconstruction_error(reconstruction, observations[sample, wavelength]['reflectances'])
            if reconstruction_error < reconstruction_error_min:
                reconstruction_error_min = reconstruction_error
                prediction_type = 'pred_mean'
            reconstructions.append(reconstruction)
            predictions_post.append(predictions[sample, wavelength].meansPred.mean)
            results['pred_mean']['ESTIM_X'][sample,wavelength,:] = predictions[sample, wavelength].meansPred.mean
            results['pred_mean']['STD_X'][sample,wavelength,:] = np.sqrt(predictions[sample, wavelength].meansPred.variance)
            results['pred_mean']['ERR_Y'][sample,wavelength] = reconstruction_error
            results['pred_mean']['ESTIM_X_PHYSICAL_UNITS'][sample,wavelength,:] = \
                physical_model.to_physic(predictions[sample, wavelength].meansPred.mean)
            results['pred_mean']['STD_X_PHYSICAL_UNITS'][sample,wavelength,:] = physical_model.to_physic(results['pred_mean']['STD_X'][sample,wavelength,:])

            ### IMIS
            reconstruction = physical_model.F(imis_results[0, sample, wavelength].mean)
            reconstruction_error = compute_reconstruction_error(reconstruction, observations[sample, wavelength]['reflectances'])
            if reconstruction_error < reconstruction_error_min:
                reconstruction_error_min = reconstruction_error
                prediction_type = 'imis_mean'
            reconstructions.append(reconstruction)
            predictions_post.append(imis_results[0, sample, wavelength].mean)
            results['imis_mean']['ESTIM_X'][sample,wavelength,:] = imis_results[0, sample, wavelength].mean
            results['imis_mean']['STD_X'][sample,wavelength,:] = np.sqrt(imis_results[0, sample, wavelength].covariance)
            results['imis_mean']['ERR_Y'][sample,wavelength] = reconstruction_error
            results['imis_mean']['ESTIM_X_PHYSICAL_UNITS'][sample,wavelength,:] =\
                physical_model.to_physic(imis_results[0, sample, wavelength].mean)
            results['imis_mean']['STD_X_PHYSICAL_UNITS'][sample,wavelength,:] = physical_model.to_physic(results['imis_mean']['STD_X'][sample,wavelength,:])

            for i in range(nb_centers):
                
                ### Prediction
                reconstruction = physical_model.F(predictions[sample, wavelength].centersPred.means[:, i])
                reconstruction_error = compute_reconstruction_error(reconstruction, observations[sample, wavelength]['reflectances'])
                if reconstruction_error < reconstruction_error_min:
                    reconstruction_error_min = reconstruction_error
                    prediction_type = 'pred_center_' + (i+1).__str__()
                reconstructions.append(reconstruction)
                predictions_post.append(predictions[sample, wavelength].centersPred.means[:, i])
                series[wavelength,:,i] = predictions[sample, wavelength].centersPred.means[:, i]

                results['pred_center_' + (i+1).__str__()]['ESTIM_X'][sample,wavelength,:] = predictions[sample, wavelength].centersPred.means[:, i]
                results['pred_center_' + (i+1).__str__()]['STD_X'][sample,wavelength,:] = \
                    np.sqrt(predictions[sample, wavelength].centersPred.covs[i, :, :].diagonal())
                results['pred_center_' + (i+1).__str__()]['ERR_Y'][sample,wavelength] = reconstruction_error
                results['pred_center_' + (i + 1).__str__()]['ESTIM_X_PHYSICAL_UNITS'][sample,wavelength,:] =\
                    physical_model.to_physic(predictions[sample, wavelength].centersPred.means[:, i])
                results['pred_center_' + (i+1).__str__()]['STD_X_PHYSICAL_UNITS'][sample,wavelength,:] = physical_model.to_physic(results['pred_center_' + (i+1).__str__()]['STD_X'][sample,wavelength,:])

                ### IMIS
                reconstruction = physical_model.F(imis_results[i + 1, sample, wavelength].mean)
                reconstruction_error = compute_reconstruction_error(reconstruction, observations[sample, wavelength]['reflectances'])
                if reconstruction_error < reconstruction_error_min:
                    reconstruction_error_min = reconstruction_error
                    prediction_type = 'imis_center_' + (i+1).__str__()
                reconstructions.append(reconstruction)
                predictions_post.append(imis_results[i + 1, sample, wavelength].mean)

                series_imis[wavelength,:,i] = imis_results[i + 1, sample, wavelength].mean

                results['imis_center_' + (i+1).__str__()]['ESTIM_X'][sample,wavelength,:] = imis_results[i + 1, sample, wavelength].mean
                results['imis_center_' + (i+1).__str__()]['STD_X'][sample,wavelength,:] = np.sqrt(imis_results[i + 1, sample, wavelength].mean)
                results['imis_center_' + (i+1).__str__()]['ERR_Y'][sample,wavelength] = reconstruction_error
                results['imis_center_' + (i + 1).__str__()]['ESTIM_X_PHYSICAL_UNITS'][sample,wavelength,:] =\
                    physical_model.to_physic(imis_results[i + 1, sample, wavelength].mean)
                results['imis_center_' + (i+1).__str__()]['STD_X_PHYSICAL_UNITS'][sample,wavelength,:] = physical_model.to_physic(results['imis_center_' + (i+1).__str__()]['STD_X'][sample,wavelength,:])
            
            ### X best
            results['x_best']['ESTIM_X'][sample,wavelength,:] = results[prediction_type]['ESTIM_X'][sample,wavelength,:]
            results['x_best']['STD_X'][sample,wavelength,:] = results[prediction_type]['STD_X'][sample,wavelength,:]
            results['x_best']['ERR_Y'][sample,wavelength] = results[prediction_type]['ERR_Y'][sample,wavelength]
            results['x_best']['ESTIM_X_PHYSICAL_UNITS'][sample,wavelength,:] = results[prediction_type]['ESTIM_X_PHYSICAL_UNITS'][sample,wavelength,:]
            results['x_best']['STD_X_PHYSICAL_UNITS'][sample,wavelength,:] = results[prediction_type]['STD_X_PHYSICAL_UNITS'][sample,wavelength,:]

            results['MULT'][sample,wavelength] = compute_multiplicity_index(np.array(predictions_post), observations[sample, wavelength]['reflectances'],
                                        reconstructions)

        permutations = predictor.regularize(np.array(series, dtype=np.double))
        # permutations_is = predictor.regularize(series_is)
        permutations_imis = predictor.regularize(np.array(series_imis, dtype=np.double))

        regu = np.empty((nb_wavelengths,L))
        # regu_is = np.empty((nb_wavelengths,L))
        regu_imis = np.empty((nb_wavelengths,L))
        for i in range(nb_wavelengths):
            regu[i,:] = series[i,:,int(permutations[0, i])]
            # regu_is[i,:] = series_is[i,:,int(permutations_is[0, i])]
            regu_imis[i,:] = series_imis[i,:,int(permutations_imis[0, i])]

        results['REGU_CENTERS'][sample,:,:] = regu
        # results['REGU_CENTERS_IS'][sample,:,:] = regu_is
        results['REGU_CENTERS_IMIS'][sample,:,:] = regu_imis

    ### Save results
    # with open('RESULTS-' + observations_file_name, 'w') as f:
    #     json.dump(results, f, indent=4)
    return results


# ----------------------- Write output files ------------------------

def zip_file(folder_path, file_names):
    zipObj = ZipFile(os.path.join(folder_path, 'results.zip'), 'w')
    for filename in file_names:
        zipObj.write(os.path.join(folder_path, filename), filename)
        os.remove(os.path.join(folder_path, filename))
    zipObj.close()

    return os.path.join(folder_path, 'results.zip')

def write_json_file(results, folder_path, wavelengths, keys, title):
    file_names = []
    for element in results.keys():  # loop over prediction type
        to_write = {title: {
            "wavelengths": wavelengths
        }}
        sample = 0
        for key in keys:  # loop over samples
            if key.startswith('reflectance'):
                to_write[title][key] = {}
                if element in ['MULT', 'REGU_CENTERS', 'REGU_CENTERS_IS', 'REGU_CENTERS_IMIS']:
                    to_write[title][key][element] = results[element][sample].tolist()
                else:
                    for cube in results[element].keys():  # loop over result type
                        to_write[title][key][cube] = results[element][cube][sample].tolist()
                sample += 1

        file_names.append(title + '_' + element + ".json")
        with open(os.path.join(folder_path, title + '_' + element + ".json"), 'w') as results_file:
            json.dump(to_write, results_file, indent=4)
    
    return zip_file(folder_path, file_names) 

def write_envi_file(array, element, folder_path, wavelengths):
    envi_driver = gdal.GetDriverByName("ENVI")
    dim_size = len(array.shape)
    dst_ds = envi_driver.Create(os.path.join(folder_path, element),
                                xsize=(array.shape[2] if dim_size==3 else 1), # samples (envi) / L (planetgllim) or 1
                                ysize=array.shape[0], # lines (envi) / nb_scenes (planetgllim)
                                bands=array.shape[1], # bands (envi) / nb_wave (planetgllim)
                                eType=gdal.GDT_Float32)
    for k in range(array.shape[1]): # k is wavelength index
        band = dst_ds.GetRasterBand(k + 1)
        if dim_size == 3:
            band.WriteArray(array[:, k, :])
        else:
            band.WriteArray(np.array(array[:, k]).reshape(array.shape[0], 1))
    dst_ds.FlushCache()
    envi_label = open(os.path.join(folder_path, element + ".hdr"), "a")
    envi_label.write('wavelength = { ')
    envi_label.write(', '.join(map(str, wavelengths)))
    envi_label.write('}\n')
    envi_label.close()

def write_remote_sensing_file(results, folder_path, wavelengths):
    file_names = []
    for element in results.keys():
        if element in ['MULT', 'REGU_CENTERS', 'REGU_CENTERS_IS', 'REGU_CENTERS_IMIS']:
            file_names.append(element)
            write_envi_file(results[element], element, folder_path, wavelengths)
            file_names.append(element + ".hdr")
        else:
            for cube in results[element].keys():
                file_names.append(element + '_' + cube)
                write_envi_file(results[element][cube], element + '_' + cube, folder_path, wavelengths)
                file_names.append(element + '_' + cube + ".hdr")
    return zip_file(folder_path, file_names)




# [IDEA]: create a Simulation class like this
# class SimulationPipeline :
#    def __init__(self, config, geometries, observations):
#       MainParser()
#       read_observations: 
#       self.wavelength = obs["wavelength"]
#       self.geometries = get_geom
#       ...