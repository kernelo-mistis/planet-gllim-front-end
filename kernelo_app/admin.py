from django.contrib import admin
from .models import Simulation
from .models import LaboratoryData
from .models import RemoteSensingData
from .models import TaskModel
from .models import CustomModel

# Register your models here.

admin.site.register(Simulation)
admin.site.register(LaboratoryData)
admin.site.register(RemoteSensingData)
admin.site.register(TaskModel)
admin.site.register(CustomModel)