from django.db import models
import os


# Create your models here.
def upload_simulation_file(instance, filename):
    cwd = os.getcwd()
    filepath = os.path.join(cwd, 'media')
    filepath = os.path.join(filepath, 'simulation')
    filepath = os.path.join(filepath, str(instance.simulation_id))
    if not os.path.exists(filepath):
        os.makedirs(filepath)
    filepath = os.path.join(filepath, filename)
    return filepath


def upload_data_file(instance, filename):
    cwd = os.getcwd()
    filepath = os.path.join(cwd, 'media')
    filepath = os.path.join(filepath, 'data')
    filepath = os.path.join(filepath, str(instance.entry_id))
    if not os.path.exists(filepath):
        os.makedirs(filepath)
    filepath = os.path.join(filepath, filename)
    return filepath


def upload_custom_model_file(instance, filename):
    cwd = os.getcwd()
    filepath = os.path.join(cwd, 'media')
    filepath = os.path.join(filepath, 'external_model')
    filepath = os.path.join(filepath, str(instance.external_model_id))
    if not os.path.exists(filepath):
        os.makedirs(filepath)
    filepath = os.path.join(filepath, filename)
    return filepath


class EntryData(models.Model):
    class Meta:
        abstract = True


class Simulation(models.Model):
    simulation_id = models.CharField(max_length=250, primary_key=True)
    simulation_title = models.CharField(max_length=250)
    simulation_config = models.TextField(null=True)
    data_id = models.CharField(max_length=250)
    data_source = models.CharField(max_length=15)
    physical_model = models.CharField(max_length=250)
    data_generation = models.CharField(max_length=250)
    gllim = models.CharField(max_length=250)
    prediction = models.CharField(max_length=250)
    importance_sampling = models.CharField(max_length=250)
    # imis_sampling = models.CharField(max_length=250)


class LaboratoryData(EntryData):
    entry_id = models.CharField(primary_key=True, max_length=250)
    entry_file = models.FileField(upload_to=upload_data_file, max_length=500)


class RemoteSensingData(EntryData):
    entry_id = models.CharField(primary_key=True, max_length=250)
    reflectance_file = models.FileField(upload_to=upload_data_file, max_length=500)
    reflectance_header_file = models.FileField(upload_to=upload_data_file, max_length=500)
    incertitude_file = models.FileField(upload_to=upload_data_file, max_length=500)
    incertitude_header_file = models.FileField(upload_to=upload_data_file, max_length=500)


class TaskModel(models.Model):
    task_id = models.CharField(max_length=250, primary_key=True)
    simulation_id = models.CharField(max_length=250)
    stopeable = models.BooleanField()
    step = models.IntegerField()

class CustomModel(EntryData):
    external_model_id = models.CharField(primary_key=True, max_length=250)
    external_model_file = models.FileField(upload_to=upload_custom_model_file, max_length=500)
